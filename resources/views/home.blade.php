@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header" align="center">Menu Cepat</div>

                <div class="card-body">
                    @if(\Auth::user()->level=='1')
            			@include('layouts.dashboard')
                    @elseif(\Auth::user()->level=='5')
                        @include('layouts.dashboard5')
            		@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
