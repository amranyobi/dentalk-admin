@extends('layouts.master')
@section('css')
  <link href="{{asset('plugins/wizard/steps.css')}}" rel="stylesheet">
  <!--alerts CSS -->
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#dataTables').DataTable();
      var table = $('#dataTables').DataTable();
      table.columns.adjust().draw();
    } );
  </script>
@endsection
@section('content')
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Plot Mahasiswa Ujian : {{$ujian->ujian}}  ({{date("d-m-Y",strtotime($ujian->tgl))}})</h4>
                                <h5 class="card-subtitle"></h5>
                                <form action="{{route('SimpanMahasiswa')}}" method="POST" id="form-soap" class="validation-wizard wizard-circle">
                                    <!-- Step 1 -->
                                    @csrf
                                    <section>
                                          <div class="row">
                                              <div class="col-md-12">
                                                <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0">
                                                  <thead>
                                                    <th>
                                                      No
                                                    </th>
                                                    <th>
                                                      NIM
                                                    </th>
                                                    <th>
                                                      Nama
                                                    </th>
                                                    <th>
                                                      Aksi
                                                    </th>
                                                  </thead>
                                                  <tbody>
                                                    @foreach($mahasiswa as $mhs)
                                                      <tr>
                                                        <td>
                                                          {{$no++}}
                                                        </td>
                                                        <td>
                                                          {{$mhs->nim}}
                                                        </td>
                                                        <td>
                                                          {{$mhs->nama}}
                                                        </td>
                                                        <td>
                                                          <div class="btn-group">
                                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Aksi
                                                              </button>
                                                              <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="{{route('HapusRubrik',['id'=>$mhs->nim])}}" onclick="return confirm('Anda yakin akan menghapus ?'); if (ok) return true; else return false">Hapus</a>
                                                              </div>
                                                          </div>
                                                        </td>
                                                      </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                              </div>
                                              <div class="col-md-12">
                                                <h4>Data Angkatan</h4>
                                                Pilih Angkatan : 
                                                <select onchange="Angkatan()" name="angkatan" id="angkatan">
                                                  <option value="">--Pilih--</option>
                                                  <?php
                                                  $masadepan = date("Y") + 1;
                                                  for ($i=2017; $i < $masadepan ; $i++) { 
                                                    ?>
                                                    <option value="<?php echo $i?>"><?php echo $i?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select><br><br>
                                              </div>
                                              <input name="id_ujian" type="hidden" value="{{$id}}">
                                              <div class="col-md-12">
                                                <strong>Data Angkatan</strong>
                                                <table name="peserta" class="table" id="data_angkatan">

                                                </table>
                                                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                                              </div>
                                        </div>
                                    </section>
                                </form>
                                <a href="{{route('DaftarUjian')}}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
@section('script')
  <script src="{{asset('plugins/wizard/jquery.steps.min.js')}}"></script>
  <script src="{{asset('plugins/wizard/jquery.validate.min.js')}}"></script>
  <!-- Sweet-Alert  -->
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
    $(".select2").select2({
    maximumSelectionLength: 3
    });
  </script>

  <script type="text/javascript">
    $(function () {
      $(".select2").select2();
      Angkatan()
    });

    function Angkatan()
    {
      var angkatan = $("#angkatan").val();
      var url = '/ujian/ajaxangkatan/'+angkatan;
        
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>NIM</th><th>Nama</th><th>Pilih</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.nim+"</td><td>"+element.nama+"</td><td><input name=nim[] id=flexCheckChecked["+element.nim+"] class=form-check-input type=checkbox value="+element.nim+"><label class=form-check-label for=flexCheckChecked["+element.nim+"]></label></td></tr>");
            no++;
        });
        $("#data_angkatan").html(items);
      });
    }
  </script>




<script>
  $(document).ready(function(){
    $('#data_angkatan').DataTable();
});
</script>
@endsection
