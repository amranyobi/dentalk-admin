@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Tambah Rubrik</h4>
        @if(Session::has('simpan_error'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-danger">
            <i class="fa fa-exclamation-triangle"></i> GAGAL</h3> {{session('simpan_error')}}
        </div>
        @endif
            <form class="form" method="POST" action="{{route('SimpanTambahRubrik')}}">
              @csrf <!-- @csrf harus selalu ada di setiap form, token csrf-->
              <div class="form-group m-t-10 row {{ $errors->has('materi') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Nama Rubrik</label>
                  <div class="col-md-4">
                    <input class="form-control" type="text" name="materi" value="{{old('materi')}}">
                    @if($errors->has('nama'))
                    <div class="form-control-feedback">{{ $errors->first('materi') }}</div>
                    @endif
                  </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('tipe') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">Jenis Rubrik</label>
                <div class="col-md-2">
                  <select name="tipe" class="form-control">
                      <option value="1">OSCE</option>
                      <option value="2">Komprehensif</option>
                  </select>
                </div>
              </div>
              <div class="form-group m-t-40 row" style="margin-left: 5px">
                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                <div style="margin-left: 10px"><a href="{{route('DaftarRubrik')}}"><input type="button" value="Batal" class="btn btn-danger" /></a></div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

@endsection
