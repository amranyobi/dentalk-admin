@extends('layouts.master')
@section('css')
  <link href="{{asset('plugins/wizard/steps.css')}}" rel="stylesheet">
  <!--alerts CSS -->
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#dataTables').DataTable();
      var table = $('#dataTables').DataTable();
      table.columns.adjust().draw();
    } );
  </script>
@endsection
@section('content')
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Data Gejala Diagnosa : {{$diagnosa->diagnosa}}</h4>
                                <h5 class="card-subtitle"></h5>
                                <form action="{{route('SimpanSoap')}}" method="POST" id="form-soap" class="validation-wizard wizard-circle">
                                    <!-- Step 1 -->
                                    @csrf
                                    <input type="hidden" name="id_rubrik" value="{{$id}}" />
                                    <section>
                                          <div class="row">
                                              <div class="col-md-12">
                                                <table class="display table table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                    <tr>
                                                    <th>
                                                      Gejala
                                                    </th>
                                                    <th>
                                                      Aksi
                                                    </th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr>
                                                        <td>
                                                          <select id="id_gejala" name="id_gejala" class="form-control select2">
                                                            <?php
                                                            foreach ($gejala as $gej) {
                                                              ?>
                                                              <option value="{{$gej->id}}">{{$gej->gejala}}</option>
                                                              <?php
                                                            }
                                                            ?>
                                                          </select>
                                                        </td>
                                                        <td>
                                                           <button type="button" class='btn btn-sm btn-primary' onclick="tambah_gejala({{$id}},2)">Tambah</button>
                                                        </td>
                                                      </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                              <div class="col-md-12">
                                                <strong>Data Gejala Diagnosa</strong>
                                                <table class="table" id="data_gejala">

                                                </table>
                                              </div>
                                        </div>
                                    </section>
                                </form>
                                <a href="{{route('DaftarDiagnosa')}}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
@section('script')
  <script src="{{asset('plugins/wizard/jquery.steps.min.js')}}"></script>
  <script src="{{asset('plugins/wizard/jquery.validate.min.js')}}"></script>
  <!-- Sweet-Alert  -->
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
    $(".select2").select2({
    maximumSelectionLength: 3
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      tambah_gejala({{$id}})
    });
    
  </script>

  <script>
  $(".tab-wizard").steps({
      headerTag: "h6",
      bodyTag: "section",
      transitionEffect: "fade",
      titleTemplate: '<span class="step">#index#</span> #title#',
      labels: {
          finish: "Submit"
      },
      onFinished: function (event, currentIndex) {
          var form = $(this);
          // Submit form input
          form.submit();
      }
  });


  var form = $(".validation-wizard").show();

  $(".validation-wizard").steps({
      headerTag: "h6"
      , bodyTag: "section"
      , transitionEffect: "fade"
      , titleTemplate: '<span class="step">#index#</span> #title#'
      , labels: {
          finish: "Simpan",
          next: "Selanjutnya",
          previous: "Sebelumnya",
      }
      , onStepChanging: function (event, currentIndex, newIndex) {
          //Untuk Simpan Bisa Disini alert(currentIndex)
          return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
      }
      , onFinishing: function (event, currentIndex) {
          return form.validate().settings.ignore = ":disabled", form.valid()
      }
      , onFinished: function (event, currentIndex) {
        //swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
        $(this).submit();
      }
  }), $(".validation-wizard").validate({
      ignore: "input[type=hidden]"
      , errorClass: "text-danger"
      , successClass: "text-success"
      , highlight: function (element, errorClass) {
          $(element).removeClass(errorClass)
      }
      , unhighlight: function (element, errorClass) {
          $(element).removeClass(errorClass)
      }
      , errorPlacement: function (error, element) {
          error.insertAfter(element)
      }
      , rules: {
          email: {
              email: !0
          }
      }
  })

    $(document).ready(function() {
    $('#daftarobat').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true,
      'scrollX'      : true,
      });
     $('#daftartindakan').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true,
      'scrollX'     : true,
      });
    });

   

    function tambah_gejala(id_diagnosa,id_gejala)
    {
      if(id_gejala==null)
      {
        var url = '/diagnosa/ajaxgejala/'+id_diagnosa;
      }else{
        var id_gejala = $("#id_gejala").val();
        var url = '/diagnosa/ajaxgejala/'+id_diagnosa+'/'+id_gejala;
      }
        
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>Gejala</th><th>Aksi</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.gejala+"</td><td><button type='button' onclick='hapusstase({{$id}},"+element.id+")' class='btn btn-sm btn-danger'>Hapus</button></td></tr>");
            no++;
        });
        $("#data_gejala").html(items);
      });
    }

    function hapusstase(id_diagnosa,id_gejala)
    {
      if(id_gejala == null)
      {
        var url = '/diagnosa/ajaxhapusgejala/'+id_diagnosa;
      }
      else {
      	var url = '/diagnosa/ajaxhapusgejala/'+id_diagnosa+'/'+id_gejala;
      }
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>Gejala</th><th>Aksi</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.gejala+"</td><td><button type='button' onclick='hapusstase({{$id}},"+element.id+")' class='btn btn-sm btn-danger'>Hapus</button></td></tr>");
            no++;
        });
        $("#data_gejala").html(items);
      });
    }

    function hapustambahan(id_periksa,id_tambahan)
    {
      if(id_tambahan == null)
      {
        var url = '/dokter/ajaxhapustambahan/'+id_periksa;
      }
      else {
        var url = '/dokter/ajaxhapustambahan/'+id_periksa+'/'+id_tambahan;
      }
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>Komponen</th><th>Biaya</th><th>Qty</th><th>Jumlah</th><th>Aksi</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.komponen+"</td><td>"+element.biaya.toLocaleString('en-US')+"</td><td>"+element.qty.toLocaleString('en-US')+"</td><td>"+element.jumlah_kali.toLocaleString('en-US')+"</td><td><button type='button' onclick='hapustambahan({{$id}},"+element.id+")' class='btn btn-sm btn-danger'>Hapus</button></td></tr>");
            no++;
        });
        $("#biaya_tambahan").html(items);
      });
      // $("#form-soap")[0].reset();
    }
</script>
<script>
  $(document).ready(function(){
    $('#tabel-data').DataTable();
});
</script>
@endsection
