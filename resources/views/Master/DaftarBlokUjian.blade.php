@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div align="right" style="margin-right:25px">
          <a href="{{route('TambahBlokUjian')}}"><input type="button" value="Tambah Blok Ujian" class="btn btn-primary" /></a>
        </div>
        <br>
        <h4 class="card-title">Data Blok Ujian</h4>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>
                No
              </th>
              <th>
                Nama Blok
              </th>
              <th>
                TA
              </th>
              <th>
                Aksi
              </th>
            </thead>
            <tbody>
              @foreach($blokujian as $blok)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{$blok->blok}} - {{$blok->nama_mk1}}
                  </td>
                  <td>
                    {{$blok->ta}}
                  </td>
                  <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Aksi
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('HapusBlokUjian',['id'=>$blok->id])}}" onclick="return confirm('Anda yakin akan menghapus ?'); if (ok) return true; else return false">Hapus</a>
                          <a class="dropdown-item" href="{{route('RekapNilaiBlokUjian',['id'=>$blok->id])}}">Rekapitulasi</a>
                        </div>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
     $('#daftarpoli').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection