<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Rekapitulasi Nilai {{$data['ujian']->ujian}} ({{date("d-m-Y",strtotime($data['ujian']->tgl))}}) </h4>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table border="1" width="100%">
            <thead>
              <tr>
              <th>
                No
              </th>
              <th>
                NIM
              </th>
              <th>
                Nama
              </th>
              <?php
              foreach ($data['stase'] as $st2) {
                ?>
                <th align="center"><div align="center">
                  <?php echo $st2->stase;?><br>
                  <?php echo $st2->rubrik;?></div>
                </th>
                <?php
              }
              ?>
            </tr>
                            
            </thead>
            <tbody>
              @foreach($data['mahasiswa'] as $mhs)
                <tr>
                  <td>
                    {{$data['no']++}}
                  </td>
                  <td>
                    {{$mhs->nim}}
                  </td>
                  <td>
                    {{$mhs->nama}}
                  </td>
                  <?php
                  foreach ($data['stase'] as $st) {
                    ?>
                    <td>
                      <?php
                      $coba = 1;
                      $nilai_mahasiswa = App\Http\Controllers\Master\MasterController::NilaiSatu($data['id'],$mhs->nim,$st->stase);
                      $hasil = number_format($nilai_mahasiswa,2);
                      echo $hasil + 0;
                      ?>
                    </td>
                    <?php
                  }
                  ?>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>