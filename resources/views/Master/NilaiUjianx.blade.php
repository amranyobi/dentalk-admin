@extends('layouts.master')
@section('css')
  <link href="{{asset('plugins/wizard/steps.css')}}" rel="stylesheet">
  <!--alerts CSS -->
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#dataTables').DataTable();
      var table = $('#dataTables').DataTable();
      table.columns.adjust().draw();
    } );
  </script>
@endsection
@section('content')
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Input Nilai Ujian : {{$ujian->ujian}}  (Stase {{$ujian->stase}})</h4>
                                <h4 class="card-title">Tanggal : {{date("d-m-Y",strtotime($ujian->tgl))}}</h4>
                                <h5 class="card-subtitle"></h5>
                                
                                    <!-- Step 1 -->
                                    <section>
                                          <div class="row">
                                              <div class="col-md-12">
                                                <form action="{{route('UbahMahasiswa')}}" method="POST" id="form-soap" class="validation-wizard wizard-circle">
                                                @csrf
                                                <input name="id_ujian" type="hidden" value="{{$id}}">
                                                <input name="stase" type="hidden" value="{{$stase}}">
                                                Pilih Mahasiswa : 
                                                <select class="select2 required" autocomplete="off" name="nim" id="nim">
                                                  <option value="">--Pilih--</option>
                                                  <?php
                                                  $masadepan = date("Y") + 1;
                                                  foreach ($mahasiswa as $mhs) {
                                                    ?>
                                                    <option value="<?php echo $mhs->nim?>" <?php
                                                    if($nim==$mhs->nim)
                                                      echo "selected";
                                                    ?>><?php echo $mhs->nim?> - <?php echo $mhs->nama?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select>&nbsp;&nbsp;<input type="SUBMIT" value="Lihat" class="btn btn-success"/><br><br>
                                                </form>
                                              </div>
                                              <div class="col-md-12">
                                                <form action="{{route('SimpanNilai')}}" method="POST" id="form-soap" class="validation-wizard wizard-circle">
                                                @csrf
                                                <input name="id_ujian" type="hidden" value="{{$id}}">
                                                <input name="stase" type="hidden" value="{{$stase}}">
                                                <input name="nim" type="hidden" value="{{$nim}}">
                                                <table class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                    <th>
                                                      No
                                                    </th>
                                                    <th>
                                                      Materi
                                                    </th>
                                                    <th>
                                                      Keterangan Skor
                                                    </th>
                                                    <th>
                                                      Nilai
                                                    </th>
                                                  </thead>
                                                  <tbody>
                                                    @php
                                                      $total_nmax = 0;
                                                      $total_nilai = 0;
                                                    @endphp
                                                    @foreach($get_materi as $mat)
                                                    <input type="hidden" name="materi[{{$mat['id_materi']}}]" value="{{$mat['id_materi']}}">
                                                      <tr>
                                                        <td>
                                                          {{$no++}}
                                                        </td>
                                                        <td>
                                                          <?php
                                                          echo $mat['materi'];
                                                          ?>
                                                        </td>
                                                        <td>
                                                          @if($mat['ks0']!='')
                                                            0 : {{$mat['ks0']}}<br>
                                                          @endif
                                                          @if($mat['ks1']!='')
                                                            1 : {{$mat['ks1']}}<br>
                                                          @endif
                                                          @if($mat['ks2']!='')
                                                            2 : {{$mat['ks2']}}
                                                          @endif
                                                        </td>
                                                        <td>
                                                          <select autocomplete="off" name="isi_nilai[{{$mat['id_materi']}}]">
                                                            @if($mat['ks0']!='')
                                                              <option value="0" <?php
                                                              if($mat['nilai']=='0')
                                                                echo "selected";
                                                              ?>>0</option>
                                                            @endif
                                                            @if($mat['ks1']!='')
                                                              <option value="1" <?php
                                                              if($mat['nilai']=='1')
                                                                echo "selected";
                                                              ?>>1</option>
                                                            @endif
                                                            @if($mat['ks2']!='')
                                                              <option value="2" <?php
                                                              if($mat['nilai']=='2')
                                                                echo "selected";
                                                              ?>>2</option>
                                                            @endif
                                                          </select>
                                                          <?php
                                                          if($mat['ks2']!='')
                                                            $nmax = 2;
                                                          else
                                                            $nmax = 1;

                                                          $total_nmax = $total_nmax + $nmax;
                                                          if($mat['nilai']!='')
                                                            $total_nilai = $total_nilai + $mat['nilai'];
                                                          else
                                                            $total_nilai = $total_nilai + 0;
                                                          ?>
                                                        </td>
                                                      </tr>
                                                    @endforeach
                                                      <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td><div align="right">Nilai</td>
                                                        <td><div align="left">
                                                        <?php
                                                        if($total_nilai!='0'){
                                                          $nilai = $total_nilai/$total_nmax * 100;
                                                          echo number_format($nilai,2);
                                                        }else{
                                                          echo "<font color=red>Belum Dinilai</font>";
                                                        }
                                                        
                                                        ?>  
                                                        </div>
                                                        </td>
                                                      </tr>
                                                  </tbody>
                                                </table>
                                                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                                                </form>
                                              </div>
                                        </div>
                                    </section>
                                <a href="{{route('DaftarUjianDosen')}}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
@section('script')
  <script src="{{asset('plugins/wizard/jquery.steps.min.js')}}"></script>
  <script src="{{asset('plugins/wizard/jquery.validate.min.js')}}"></script>
  <!-- Sweet-Alert  -->
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
    $(".select2").select2({
    maximumSelectionLength: 3
    });
  </script>

  <script type="text/javascript">
    $(function () {
      $(".select2").select2();
      Mahasiswa()
    });

    function Mahasiswa()
    {
      var nim = $("#nim").val();
      var url = '/ujian/ajaxnilai/'+{{$id}}+'/'+<?php echo $stase?>+'/'+nim;
        
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>NIM</th><th>Nama</th><th>Pilih</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.nim+"</td><td>"+element.nama+"</td><td><input name=nim[] id=flexCheckChecked["+element.nim+"] class=form-check-input type=checkbox value="+element.nim+"><label class=form-check-label for=flexCheckChecked["+element.nim+"]></label></td></tr>");
            no++;
        });
        $("#data_angkatan").html(items);
      });
    }
  </script>




<script>
  $(document).ready(function(){
    $('#data_angkatan').DataTable();
});
</script>
@endsection
