@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <br>
        <h4 class="card-title">Data Ujian</h4>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>
                No
              </th>
              <th>
                Nama Ujian
              </th>
              <th>
                Tanggal
              </th>
              <th>
                Stase
              </th>
              <th>
                Tipe
              </th>
              <th>
                Aksi
              </th>
            </thead>
            <tbody>
              @foreach($ujian as $uji)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{$uji->ujian}}
                  </td>
                  <td>
                    {{date("d-m-Y",strtotime($uji->tgl))}}
                  </td>
                  <td>
                    {{$uji->stase}}
                  </td>
                  <td>
                    @if($uji->tipe=='1')
                      OSCE
                    @elseif($uji->tipe=='2')
                      Komprehensif
                    @endif
                  </td>
                  <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Aksi
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('NilaiUjian',['id'=>$uji->id])}}">Nilai Mahasiswa</a>
                          <a class="dropdown-item" href="{{route('RekapNilaiDosen',['id'=>$uji->id])}}">Rekapitulasi</a>
                        </div>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
     $('#daftarpoli').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection