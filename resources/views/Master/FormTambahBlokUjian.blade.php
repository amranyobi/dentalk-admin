@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Tambah Ujian</h4>
        @if(Session::has('simpan_error'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-danger">
            <i class="fa fa-exclamation-triangle"></i> GAGAL</h3> {{session('simpan_error')}}
        </div>
        @endif
            <form class="form" method="POST" action="{{route('SimpanTambahBlokUjian')}}">
              @csrf <!-- @csrf harus selalu ada di setiap form, token csrf-->
              <div class="form-group m-t-10 row {{ $errors->has('blok') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">Blok</label>
                <div class="col-md-6">
                  <select name="blok" class="form-control">
                      <?php
                      foreach ($blok as $blk) {
                        ?>
                        <option value="{{$blk->kdmk}}">{{$blk->kdmk}} - {{$blk->nama_mk1}}</option>
                        <?php
                      }
                      ?>
                  </select>
                </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('ta') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">TA</label>
                <div class="col-md-2">
                  <select name="ta" class="form-control">
                      <?php
                      foreach ($ta as $t) {
                        ?>
                        <option value="{{$t->tahun}}">{{$t->tahun}}</option>
                        <?php
                      }
                      ?>
                  </select>
                </div>
              </div>
              <div class="form-group m-t-40 row" style="margin-left: 5px">
                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                <div style="margin-left: 10px"><a href="{{route('DaftarBlokUjian')}}"><input type="button" value="Batal" class="btn btn-danger" /></a></div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

@endsection
