@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <!-- <div align="right" style="margin-right:25px">
          <a href="{{route('TambahPoli')}}"><input type="button" value="Tambah Poliklinik" class="btn btn-primary" /></a>
        </div> -->
        <br>
        <h4 class="card-title">Data Penguji</h4>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>
                No
              </th>
              <th>
                ID User
              </th>
              <th>
                Nama
              </th>
            </thead>
            <tbody>
              @foreach($penguji as $peng)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{$peng->id}}
                  </td>
                  <td>
                    {{$peng->nama}}, {{$peng->gelar}}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
     $('#daftarpoli').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection