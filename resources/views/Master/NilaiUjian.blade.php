@extends('layouts.master')
@section('css')
  <link href="{{asset('plugins/wizard/steps.css')}}" rel="stylesheet">
  <!--alerts CSS -->
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#dataTables').DataTable();
      var table = $('#dataTables').DataTable();
      table.columns.adjust().draw();
    } );
  </script>
@endsection
@section('content')
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Input Nilai Ujian : {{$ujian->ujian}}  (Stase {{$ujian->stase}})</h4>
                                <h4 class="card-title">Tanggal : {{date("d-m-Y",strtotime($ujian->tgl))}}</h4>
                                <h5 class="card-subtitle"></h5>
                                
                                    <!-- Step 1 -->
                                    
                                    <section>
                                          <div class="row">
                                              <div class="col-md-12">
                                                <form action="{{route('UbahMahasiswa')}}" method="POST" id="form-soap" class="validation-wizard wizard-circle">
                                                @csrf
                                                <input name="id_ujian" type="hidden" value="{{$id}}">
                                                <input name="stase" type="hidden" value="{{$stase}}">
                                                Pilih Mahasiswa : 
                                                <select class="select2 required" name="nim" id="nim">
                                                  <option value="">--Pilih--</option>
                                                  <?php
                                                  $masadepan = date("Y") + 1;
                                                  foreach ($mahasiswa as $mhs) {
                                                    ?>
                                                    <option value="<?php echo $mhs->nim?>"><?php echo $mhs->nim?> - <?php echo $mhs->nama?></option>
                                                    <?php
                                                  }
                                                  ?>
                                                </select>&nbsp;&nbsp;<input type="SUBMIT" value="Lihat" class="btn btn-success"/><br><br>
                                                </form>
                                              </div>
                                        </div>
                                    </section>
                                <a href="{{route('DaftarUjianDosen')}}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
@section('script')
  <script src="{{asset('plugins/wizard/jquery.steps.min.js')}}"></script>
  <script src="{{asset('plugins/wizard/jquery.validate.min.js')}}"></script>
  <!-- Sweet-Alert  -->
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
    $(".select2").select2({
    maximumSelectionLength: 3
    });
  </script>

  <script type="text/javascript">
    $(function () {
      $(".select2").select2();
      Mahasiswa()
    });

    function Mahasiswa()
    {
      var nim = $("#nim").val();
      var url = '/ujian/ajaxnilai/'+{{$id}}+'/'+<?php echo $stase?>+'/'+nim;
        
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>NIM</th><th>Nama</th><th>Pilih</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.nim+"</td><td>"+element.nama+"</td><td><input name=nim[] id=flexCheckChecked["+element.nim+"] class=form-check-input type=checkbox value="+element.nim+"><label class=form-check-label for=flexCheckChecked["+element.nim+"]></label></td></tr>");
            no++;
        });
        $("#data_angkatan").html(items);
      });
    }
  </script>




<script>
  $(document).ready(function(){
    $('#data_angkatan').DataTable();
});
</script>
@endsection
