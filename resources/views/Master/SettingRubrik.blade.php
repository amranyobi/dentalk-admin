@extends('layouts.master')
@section('css')
  <link href="{{asset('plugins/wizard/steps.css')}}" rel="stylesheet">
  <!--alerts CSS -->
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#dataTables').DataTable();
      var table = $('#dataTables').DataTable();
      table.columns.adjust().draw();
    } );
  </script>
  <script src="https://cdn.tiny.cloud/1/xdpr56qzimhnfuxl32i2zquqdmwjenxlxbn3wpn8qfu5li7b/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
      tinymce.init({
      selector: 'textarea',
      height: 300,
      forced_root_block : "",
      force_br_newlines : true,
      force_p_newlines : false,
      plugins: [
        'autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect table | bold italic | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media ',
      templates: [
        { title: 'Test template 1', content: '' },
        { title: 'Test template 2', content: '' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ],
    });
</script>
@endsection
@section('content')
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <h4 class="card-title">Data Materi RUbrik : {{$rubrik->rubrik}}</h4>
                                <h5 class="card-subtitle"></h5>
                                <form action="{{route('SimpanMateri')}}" method="POST" id="form-soap" class="validation-wizard wizard-circle">
                                    <!-- Step 1 -->
                                    @csrf
                                    <input type="hidden" name="id_rubrik" value="{{$id}}" />
                                    <section>
                                          <div class="row">
                                              <div class="col-md-12">
                                                <table class="display table table-bordered" cellspacing="0" width="100%">
                                                  <thead>
                                                    <tr>
                                                    <th>
                                                      Materi
                                                    </th>
                                                    <th>
                                                      Keterangan Skor
                                                    </th>
                                                    <th>
                                                      Aksi
                                                    </th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                      <tr>
                                                        <td>
                                                          <textarea class="form-control" id="materi" name="materi"></textarea>
                                                        </td>
                                                        <td>
                                                          <input class="form-control" name="ks0" id="ks0" placeholder="Keterangan Skor 0"><br>
                                                          <input class="form-control" name="ks1" id="ks1" placeholder="Keterangan Skor 1"><br>
                                                          <input class="form-control" name="ks2" id="ks2" placeholder="Keterangan Skor 2">
                                                        </td>
                                                        <td>
                                                           <!-- <button type="button" class='btn btn-sm btn-primary' onclick="tambah_materi({{$id}},2)">Tambah</button> -->
                                                           <button type="submit" class='btn btn-sm btn-primary'>Tambah</button>
                                                        </td>
                                                      </tr>
                                                  </tbody>
                                                </table>
                                              </div>
                                              <div class="col-md-12">
                                                <strong>Data Materi</strong>
                                                <table class="table" id="data_materi">

                                                </table>
                                              </div>
                                        </div>
                                    </section>
                                </form>
                                <a href="{{route('DaftarRubrik')}}" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
@section('script')
  <script src="{{asset('plugins/wizard/jquery.steps.min.js')}}"></script>
  <script src="{{asset('plugins/wizard/jquery.validate.min.js')}}"></script>
  <!-- Sweet-Alert  -->
  <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      tambah_materi({{$id}})
    });
    
  </script>

  <script>
  $(".tab-wizard").steps({
      headerTag: "h6",
      bodyTag: "section",
      transitionEffect: "fade",
      titleTemplate: '<span class="step">#index#</span> #title#',
      labels: {
          finish: "Submit"
      },
      onFinished: function (event, currentIndex) {
          var form = $(this);
          // Submit form input
          form.submit();
      }
  });


  var form = $(".validation-wizard").show();

  $(".validation-wizard").steps({
      headerTag: "h6"
      , bodyTag: "section"
      , transitionEffect: "fade"
      , titleTemplate: '<span class="step">#index#</span> #title#'
      , labels: {
          finish: "Simpan",
          next: "Selanjutnya",
          previous: "Sebelumnya",
      }
      , onStepChanging: function (event, currentIndex, newIndex) {
          //Untuk Simpan Bisa Disini alert(currentIndex)
          return currentIndex > newIndex || !(3 === newIndex && Number($("#age-2").val()) < 18) && (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
      }
      , onFinishing: function (event, currentIndex) {
          return form.validate().settings.ignore = ":disabled", form.valid()
      }
      , onFinished: function (event, currentIndex) {
        //swal("Form Submitted!", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat eleifend ex semper, lobortis purus sed.");
        $(this).submit();
      }
  }), $(".validation-wizard").validate({
      ignore: "input[type=hidden]"
      , errorClass: "text-danger"
      , successClass: "text-success"
      , highlight: function (element, errorClass) {
          $(element).removeClass(errorClass)
      }
      , unhighlight: function (element, errorClass) {
          $(element).removeClass(errorClass)
      }
      , errorPlacement: function (error, element) {
          error.insertAfter(element)
      }
      , rules: {
          email: {
              email: !0
          }
      }
  })

    $(document).ready(function() {
    $(".select2").select2({
    maximumSelectionLength: 3
    });
    $('#daftarobat').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true,
      'scrollX'      : true,
      });
     $('#daftartindakan').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true,
      'scrollX'     : true,
      });
    });

   

    function tambah_materi(id_rubrik,materi,ks0,ks1,ks2)
    {
      if(materi==null && ks0==null && ks1==null && ks2==null)
      {
        var url = '/rubrik/ajaxmateri/'+id_rubrik;
      }else{
        var materi = $("#materi").val();
        var ks0 = $("#ks0").val();
        if(ks0=='')
          ks0 = "-";
        var ks1 = $("#ks1").val();
        if(ks1=='')
          ks1 = "-";
        var ks2 = $("#ks2").val();
        if(ks2=='')
          ks2 = "-";
        var url = '/rubrik/ajaxmateri/'+id_rubrik+'/'+materi+'/'+ks0+'/'+ks1+'/'+ks2;
      }
        
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>Materi</th><th>Keterangan Skor</th><th>Aksi</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.materi+"</td><td>1. "+element.ks0+"<br>2. "+element.ks1+"<br>3. "+element.ks2+"</td><td><button type='button' onclick='hapusmateri({{$id}},"+element.id+")' class='btn btn-sm btn-danger'>Hapus</button></td></tr>");
            no++;
        });
        $("#data_materi").html(items);
      });
    }

    function hapusmateri(id_rubrik,id_materi)
    {
      if(id_materi == null)
      {
        var url = '/rubrik/ajaxhapusmateri/'+id_rubrik;
      }
      else {
      	var url = '/rubrik/ajaxhapusmateri/'+id_rubrik+'/'+id_materi;
      }
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>Materi</th><th>Keterangan Skor</th><th>Aksi</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.materi+"</td><td>"+element.ks0+"<br>"+element.ks1+"<br>"+element.ks2+"</td><td><button type='button' onclick='hapusmateri({{$id}},"+element.id+")' class='btn btn-sm btn-danger'>Hapus</button></td></tr>");
            no++;
        });
        $("#data_materi").html(items);
      });
    }

    function hapustambahan(id_periksa,id_tambahan)
    {
      if(id_tambahan == null)
      {
        var url = '/dokter/ajaxhapustambahan/'+id_periksa;
      }
      else {
        var url = '/dokter/ajaxhapustambahan/'+id_periksa+'/'+id_tambahan;
      }
      $.getJSON(url,function(data) {
        var items=[];
          items.push("<tr><th>No</th><th>Komponen</th><th>Biaya</th><th>Qty</th><th>Jumlah</th><th>Aksi</th></tr>");
          var no =1;
        $.each(data, function(index, element) {
            items.push("<tr><td>"+no+"</td><td>"+element.komponen+"</td><td>"+element.biaya.toLocaleString('en-US')+"</td><td>"+element.qty.toLocaleString('en-US')+"</td><td>"+element.jumlah_kali.toLocaleString('en-US')+"</td><td><button type='button' onclick='hapustambahan({{$id}},"+element.id+")' class='btn btn-sm btn-danger'>Hapus</button></td></tr>");
            no++;
        });
        $("#biaya_tambahan").html(items);
      });
      // $("#form-soap")[0].reset();
    }
</script>
<script>
  $(document).ready(function(){
    $('#tabel-data').DataTable();
});
</script>
@endsection
