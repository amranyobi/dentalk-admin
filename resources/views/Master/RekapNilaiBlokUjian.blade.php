@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Rekapitulasi Nilai {{$blokujian->nama_mk1}} ({{$blokujian->ta}})</h4>
        <br>
        <div align="right" style="margin-right:25px">
          <a target="_blank" href="{{route('ExportRekapBlokUjian',['id'=>$id])}}"><input type="button" value="Unduh Excel" class="btn btn-primary" /></a>
        </div>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
              <td rowspan="2">No.</td>
              <td rowspan="2">NIM</td>
              <td rowspan="2">Nama</td>
              <?php
              foreach ($stase as $st) {
                  ?>
                  <td colspan="2" align="center">Stase {{$st->stase}}</td>
                  <?php
              }
              ?>
              
            </tr>
            <tr>
              <?php
              foreach ($stase as $st) {
                ?>
                <td align="center">Penguji</td>
                <td align="center">Nilai</td>
                <?php
              }
              ?>
            </tr>
            </thead>
            <tbody>
            @foreach($mahasiswa as $mhs)
            <tr>
              <td>{{$no++}}</td>
              <td>{{$mhs->nim}}</td>
              <td>{{$mhs->nama}}</td>
              <?php
              foreach ($stase as $st) {
                ?>
                <td><?php
                  $peng = App\Http\Controllers\Master\MasterController::CekPembimbing($id,$mhs->nim,$st->stase);
                  echo $peng;
                  ?></td>
                <td>
                  <?php
                  $nilai_mahasiswa = App\Http\Controllers\Master\MasterController::NilaiSatuBlokUjian($id,$mhs->nim,$st->stase);
                  $hasil = number_format($nilai_mahasiswa,2);
                  echo $hasil + 0;
                  ?>
                </td>
                <?php
              }
              ?>
            </tr>
            @endforeach
            </tbody>
          </table>
        <a href="{{route('DaftarBlokUjian')}}" class="btn btn-danger">Kembali</a> </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
     $('#daftarpoli').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection