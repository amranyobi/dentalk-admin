@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div align="right" style="margin-right:25px">
          <a href="{{route('TambahUjian')}}"><input type="button" value="Tambah Ujian" class="btn btn-primary" /></a>
        </div>
        <br>
        <h4 class="card-title">Data Ujian</h4>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>
                No
              </th>
              <th>
                Ujian
              </th>
              <th>
                Blok Ujian
              </th>
              <th>
                Tanggal
              </th>
              <th>
                Status
              </th>
              <th>
                Tipe
              </th>
              <th>
                Aksi
              </th>
            </thead>
            <tbody>
              @foreach($ujian as $uji)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{$uji->ujian}}
                  </td>
                  <td>
                    {{$uji->blok}} - {{$uji->nama_mk1}} ({{$uji->ta}})
                  </td>
                  <td>
                    {{date("d-m-Y",strtotime($uji->tgl))}}
                  </td>
                  <td>
                    @if($uji->status=='0')
                      <font color="red">Tidak Aktif</font>
                    @elseif($uji->status=='1')
                      <font color="green">Aktif</font>
                    @endif
                  </td>
                  <td>
                    @if($uji->tipe=='1')
                      OSCE
                    @elseif($uji->tipe=='2')
                      Komprehensif
                    @endif
                  </td>
                  <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Aksi
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('HapusUjian',['id'=>$uji->id])}}" onclick="return confirm('Anda yakin akan menghapus ?'); if (ok) return true; else return false">Hapus</a>
                          <a class="dropdown-item" href="{{route('StaseUjian',['id'=>$uji->id])}}">Stase</a>
                          <a class="dropdown-item" href="{{route('PlotMahasiswa',['id'=>$uji->id])}}">Plot Mahasiswa</a>
                          <a class="dropdown-item" href="{{route('RekapNilai',['id'=>$uji->id])}}">Rekapitulasi</a>
                        </div>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
     $('#daftarpoli').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection