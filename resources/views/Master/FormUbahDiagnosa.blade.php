@extends('layouts.master')
@section('css')
  <link href="{{asset('plugins/wizard/steps.css')}}" rel="stylesheet">
  <!--alerts CSS -->
  <link href="{{asset('plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" type="text/css" href="{{asset('plugins/datatables/jquery.dataTables.min.css')}}">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
      $('#dataTables').DataTable();
      var table = $('#dataTables').DataTable();
      table.columns.adjust().draw();
    } );
  </script>
  <script src="https://cdn.tiny.cloud/1/xdpr56qzimhnfuxl32i2zquqdmwjenxlxbn3wpn8qfu5li7b/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
      tinymce.init({
      selector: 'textarea',
      height: 300,
      forced_root_block : "",
      force_br_newlines : true,
      force_p_newlines : false,
      plugins: [
        'autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
      ],
      toolbar1: 'undo redo | insert | styleselect table | bold italic | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media ',
      templates: [
        { title: 'Test template 1', content: '' },
        { title: 'Test template 2', content: '' }
      ],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ],
    });
  </script>
@endsection
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Ubah Diagnosa</h4>
        @if(Session::has('simpan_error'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-danger">
            <i class="fa fa-exclamation-triangle"></i> GAGAL</h3> {{session('simpan_error')}}
        </div>
        @endif
            <form class="form" method="POST" action="{{route('SimpanUbahDiagnosa')}}">
              @csrf <!-- @csrf harus selalu ada di setiap form, token csrf-->
              <input name="id" value="{{$id}}" type="hidden">
              <div class="form-group m-t-10 row {{ $errors->has('materi') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Diagnosa</label>
                  <div class="col-md-4">
                    <input class="form-control" type="text" name="diagnosa" value="{{$data_diagnosa->diagnosa}}">
                    @if($errors->has('diagnosa'))
                    <div class="form-control-feedback">{{ $errors->first('diagnosa') }}</div>
                    @endif
                  </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('materi') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Detail</label>
                  <div class="col-md-10">
                    <textarea class="form-control" id="detail" name="detail">{{$data_diagnosa->detail}}</textarea>
                  </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('penanganan') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Penanganan</label>
                  <div class="col-md-10">
                    <textarea class="form-control" id="penanganan" name="penanganan">{{$data_diagnosa->penanganan}}</textarea>
                  </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('sumber') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Sumber</label>
                  <div class="col-md-10">
                    <textarea class="form-control" id="sumber" name="sumber">{{$data_diagnosa->sumber}}</textarea>
                  </div>
              </div>
              <div class="form-group m-t-40 row" style="margin-left: 5px">
                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                <div style="margin-left: 10px"><a href="{{route('DaftarDiagnosa')}}"><input type="button" value="Batal" class="btn btn-danger" /></a></div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

@endsection
