@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Tambah Ujian</h4>
        @if(Session::has('simpan_error'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-danger">
            <i class="fa fa-exclamation-triangle"></i> GAGAL</h3> {{session('simpan_error')}}
        </div>
        @endif
            <form class="form" method="POST" action="{{route('SimpanTambahUjian')}}">
              @csrf <!-- @csrf harus selalu ada di setiap form, token csrf-->
              <div class="form-group m-t-10 row {{ $errors->has('ujian') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Nama Ujian</label>
                  <div class="col-md-6">
                    <input class="form-control" type="text" name="ujian" value="{{old('ujian')}}">
                    @if($errors->has('nama'))
                    <div class="form-control-feedback">{{ $errors->first('ujian') }}</div>
                    @endif
                  </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('tipe') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">Jenis Ujian</label>
                <div class="col-md-2">
                  <select name="tipe" class="form-control">
                      <option value="1">OSCE</option>
                      <option value="2">Komprehensif</option>
                  </select>
                </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('tgl') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">Tanggal Ujian</label>
                <div class="col-md-2">
                  <input name="tgl" type="date" class="form-control">
                </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('blok') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">Blok Ujian</label>
                <div class="col-md-6">
                  <select name="blokujian" class="form-control">
                      <?php
                      foreach ($blokujian as $blk) {
                        ?>
                        <option value="{{$blk->id}}">{{$blk->blok}} - {{$blk->nama_mk1}} ({{$blk->ta}})</option>
                        <?php
                      }
                      ?>
                  </select>
                </div>
              </div>
              <div class="form-group m-t-40 row" style="margin-left: 5px">
                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                <div style="margin-left: 10px"><a href="{{route('DaftarRubrik')}}"><input type="button" value="Batal" class="btn btn-danger" /></a></div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

@endsection
