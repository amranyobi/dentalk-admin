@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div align="right" style="margin-right:25px">
          <a href="{{route('TambahDiagnosa')}}"><input type="button" value="Tambah Diagnosa" class="btn btn-sm btn-primary" /></a>
        </div>
        <br>
        <h4 class="card-title">Data Diagnosa</h4>
        @if(Session::has('simpan_sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('simpan_sukses')}}
        </div>
        @endif
        <div class="table-responsive-mt-40">
          <table id="daftarpoli" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>
                No
              </th>
              <th>
                Diagnosa
              </th>
              <th>
                Aksi
              </th>
            </thead>
            <tbody>
              @foreach($diagnosa as $diag)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{$diag->diagnosa}}
                  </td>
                  <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Aksi
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('HapusDiagnosa',['id'=>$diag->id])}}" onclick="return confirm('Anda yakin akan menghapus ?'); if (ok) return true; else return false">Hapus</a>
                          <a class="dropdown-item" href="{{route('DetailDiagnosa',['id'=>$diag->id])}}">Detail</a>
                          <a class="dropdown-item" href="{{route('UbahDiagnosa',['id'=>$diag->id])}}">Ubah</a>
                          <a class="dropdown-item" href="{{route('DataGejalaDiag',['id'=>$diag->id])}}">Data Gejala</a>
                        </div>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
     $('#daftarpoli').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection