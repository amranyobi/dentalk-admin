@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Ubah Data User</h4>
        @if(Session::has('gagal'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-danger">
            <i class="fa fa-exclamation-triangle"></i> GAGAL</h3> {{session('gagal')}}
        </div>
        @endif
            <form class="form" method="POST" action="{{route('SimpanUbahUser',['id'=>$getuser->id])}}">
				@csrf <!-- @csrf harus selalu ada di setiap form, token csrf-->
              <div class="form-group m-t-40 row {{ $errors->has('username') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Username</label>
                  <div class="col-md-2">
                    <!-- nama form di sesuaikan dengan nama tabel supaya mudah saat simpan-->
                    <input class="form-control" type="text" name="username" value="{{$getuser->username}}" readonly="readonly">
                    @if($errors->has('username'))
                    <div class="form-control-feedback">{{ $errors->first('username') }}</div>
                    @endif
                  </div>
              </div>
              <div class="form-group m-t-10 row {{ $errors->has('password') ? 'has-danger' : '' }}">
                <label for="example-text-input" class="col-md-2 col-form-label">Password</label>
                  <div class="col-md-4">
                    <input class="form-control" type="password" name="password">
                    @if($errors->has('password'))
                    <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                    @endif
                  </div>
              </div>
			  <div class="form-group m-t-10 row {{ $errors->has('level') ? 'has-danger' : '' }}">
                <label class="col-md-2 col-form-label">Level</label>
                <div class="col-md-4">
                  <select name="level" class="form-control">
                    @foreach($level as $getlevel)
						@if($getuser->level==$getlevel->id)
                      		<option value="{{$getlevel->id}}" selected="selected">{{$getlevel->keterangan}}</option>
						@else
							<option value="{{$getlevel->id}}">{{$getlevel->keterangan}}</option>
						@endif
                    @endforeach

                  </select>
                </div>
              </div>
              <div class="form-group m-t-40 row" style="margin-left: 5px">
                <input type="SUBMIT" value="Simpan" class="btn btn-success pull-right"/>
                <div style="margin-left: 10px"><a href="{{route('DaftarUser')}}"><input type="button" value="Batal" class="btn btn-danger" /></a></div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

@endsection
