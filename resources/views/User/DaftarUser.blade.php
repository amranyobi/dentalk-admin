@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div align="right" style="margin-right:25px">
          <a href="{{route('TambahUser')}}"><input type="button" value="Tambah User" class="btn btn-primary" /></a>
        </div>
        <br>
        <h4 class="card-title">Data User</h4>
        @if(Session::has('sukses'))
        <div class="alert alert-success">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-success">
            <i class="fa fa-check"></i> Sukses</h3> {{session('sukses')}}
        </div>
        @elseif(Session::has('gagal'))
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
          <h3 class="text-danger">
            <i class="fa fa-exclamation-triangle"></i> GAGAL</h3> {{session('gagal')}}
        </div>
        @endif

        <div class="table-responsive-mt-40">
          <table id="daftaruser" class="display table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <th>
                No
              </th>
              <th>
                Nama
              </th>
              <th>
                Username
              </th>
              <th>
                Level
              </th>
              <th>
                Aksi
              </th>
            </thead>
            <tbody>
              @foreach($user as $us)
                <tr>
                  <td>
                    {{$no++}}
                  </td>
                  <td>
                    {{$us->nama}}
                  </td>
                  <td>
                    {{$us->username}}
                  </td>
                  <td>
                    {{$us->keterangan}}
                  </td>
				  <td>
                  <div class="btn-group">
                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Aksi
                        </button>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="{{route('HapusUser',['id'=>$us->id])}}" onclick="return confirm('Anda yakin akan menghapus ?'); if (ok) return true; else return false">Hapus</a>
                          <a class="dropdown-item" href="{{route('UbahUser',['id'=>$us->id])}}">Ubah</a>
                        </div>
                    </div>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- This is data table -->

  <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script>
    $(document).ready(function() {
    $('#daftaruser').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
      });
    });
  </script>
@endsection