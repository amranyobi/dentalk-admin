        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="{{asset('images/dentalkadmin.png')}}" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                        </b>
                        <!--End Logo icon -->
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"></li>
                        <li class="nav-item m-l-30"><div style="color: white"><b>Dentalk Administrator</b></div></li>
                        <li class="nav-item m-l-10">
                            @if(\Auth::Check())
                                @if(App\UserLevel::AmbilBagian()!=null)
                                    @if(App\UserLevel::AmbilBagian()->level=='1')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Administrator</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='2')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Dosen</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='3')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Dokter</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='4')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Apoteker</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='5')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Front Office Pasien</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='6')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Kasir</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='7')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Laborat Pasien</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='8')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Dokter Radiografi</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='9')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Keuangan</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='10')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Laborat Gigi</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='11')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">FO Klinik Integrasi</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='12')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Mahasiswa</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='13')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Perawat Klinik Integrasi</button>

                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='14')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Kasir Klinik Integrasi</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='15')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Administrasi Profesi</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='16')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Admin Radiografi</button>
                                    @endif
                                    @if(App\UserLevel::AmbilBagian()->level=='17')
                                    <button class="pull-right btn btn-sm btn-rounded btn-warning">Admin Rekam Medis</button>
                                    @endif

                                @endif
                            @endif
                        </li>
                    </ul>

                    <!-- ============================================================== -->
                    <!-- User profile -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="{{asset('images/users/avatar.png')}}" alt="user" class="profile-pic" />
                                @if(\Auth::Check())
                                <span class="hidden-xs">{{\App\UserLevel::AmbilNama()}}</span>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li><a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                        <i class="fa fa-power-off"></i> Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
