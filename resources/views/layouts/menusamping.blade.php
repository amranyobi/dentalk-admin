        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li> <a class="waves-effect waves-light" href="{{route('Home')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Master</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <!--li><a href="">Rekam Medik </a></li-->
                                <!-- <li><a href="{{route('DaftarRubrik')}}">Rubrik Penilaian</a></li> -->
                                <li><a href="{{route('DaftarGejala')}}">Gejala</a></li>
                                <li><a href="{{route('DaftarDiagnosa')}}">Diagnosa</a></li>
                            </ul>
                        </li>
                        
                       <!--  <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Pengaturan Ujian</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('DaftarBlokUjian')}}">Blok Ujian</a></li>
                                <li><a href="{{route('DaftarUjian')}}">Ujian</a></li>
                                <li><a href="{{route('AdminListSPTM')}}">Station</a></li>
                                <li><a href="{{route('AdminListSPTM')}}">Plot</a></li>
                            </ul>
                        </li> -->

                        <!-- <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-file-document"></i><span class="hide-menu">Rekapitulasi</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('AdminListSPTM')}}">Rekap Nilai</a></li> -->
                                <!-- <li><a href="{{route('AdminListSPTM')}}">Station</a></li>
                                <li><a href="{{route('AdminListSPTM')}}">Plot</a></li> -->
                            <!-- </ul>
                        </li> -->
                    </ul>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
