<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/',['as'=>'Home','uses'=>'HomeController@index']);

//Dokter
Route::get('/dokter',['as'=>'DaftarDokter','uses'=>'Dokter\DokterController@DaftarDokter']);
Route::get('/dokter/tambah',['as'=>'TambahDaftarDokter','uses'=>'Dokter\DokterController@FormTambahDokter']);
Route::post('/dokter/simpan',['as'=>'SimpanDaftarDokter','uses'=>'Dokter\DokterController@SimpanTambahDokter']);
Route::get('/dokter/ubah/{id}',['as'=>'UbahDokter','uses'=>'Dokter\DokterController@FormUbahDokter']);
Route::post('/dokter/ubah/{id}/simpan',['as'=>'SimpanUbahDokter','uses'=>'Dokter\DokterController@SimpanUbahDokter']);
Route::get('/dokter/hapus/{id}',['as'=>'HapusDokter','uses'=>'Dokter\DokterController@HapusDokter']);
Route::get('/dokter/tindakan',['as'=>'DaftarTindakan','uses'=>'Dokter\DokterController@DaftarTindakan']);
Route::get('/dokter/tindakan/tambah',['as'=>'TambahTindakan','uses'=>'Dokter\DokterController@TambahTindakan']);
Route::post('/dokter/tindakan/simpan',['as'=>'SimpanTambahTindakan','uses'=>'Dokter\DokterController@SimpanTambahTindakan']);
Route::get('/dokter/tindakan/ubah/{id}',['as'=>'UbahTindakan','uses'=>'Dokter\DokterController@FormUbahTindakan']);
Route::post('/dokter/tindakan/ubah/{id}/simpan',['as'=>'SimpanUbahTindakan','uses'=>'Dokter\DokterController@SimpanUbahTindakan']);
Route::get('/dokter/tindakan/hapus/{id}',['as'=>'HapusTindakan','uses'=>'Dokter\DokterController@HapusTindakan']);
//Spesialis
Route::get('/dokter/spesialis',['as'=>'DaftarSpesialis','uses'=>'Dokter\SpesialisController@DaftarSpesialis']);
Route::get('/dokter/spesialis/tambah',['as'=>'TambahSpesialis','uses'=>'Dokter\SpesialisController@TambahSpesialis']);
Route::post('/dokter/spesialis/simpan',['as'=>'SimpanTambahSpesialis','uses'=>'Dokter\SpesialisController@SimpanTambahSpesialis']);
Route::get('/dokter/spesialis/hapus/{id}',['as'=>'HapusSpesialis','uses'=>'Dokter\SpesialisController@HapusSpesialis']);
Route::get('/dokter/spesialis/ubah/{id}',['as'=>'UbahSpesialis','uses'=>'Dokter\SpesialisController@UbahSpesialis']);
Route::post('/dokter/spesialis/ubah/{id}/simpan',['as'=>'SimpanUbahSpesialis','uses'=>'Dokter\SpesialisController@SimpanUbahSpesialis']);
//Jadwal Dokter
Route::get('/dokter/jadwal',['as'=>'DaftarJadwal','uses'=>'Dokter\JadwalController@DaftarJadwal']);
Route::get('/dokter/jadwal/tambah',['as'=>'TambahJadwal','uses'=>'Dokter\JadwalController@TambahJadwal']);
Route::post('/dokter/jadwal/simpan',['as'=>'SimpanTambahJadwal','uses'=>'Dokter\JadwalController@SimpanTambahJadwal']);
Route::post('/dokter/jadwal/hapus/{id}',['as'=>'HapusJadwal','uses'=>'Dokter\JadwalController@HapusJadwal']);
Route::get('/dokter/jadwal/ubah/{id}',['as'=>'UbahJadwal','uses'=>'Dokter\JadwalController@UbahJadwal']);
Route::post('/dokter/jadwal/ubah/{id}/simpan',['as'=>'SimpanUbahJadwal','uses'=>'Dokter\JadwalController@SimpanUbahJadwal']);

//Obat
Route::get('/obat',['as'=>'DaftarObat','uses'=>'Obat\ObatController@DaftarObat']);
Route::get('/obat/tambah',['as'=>'TambahObat','uses'=>'Obat\ObatController@TambahObat']);
Route::post('/obat/simpan',['as'=>'SimpanTambahObat','uses'=>'Obat\ObatController@SimpanTambahObat']);
Route::get('/obat/hapus/{id}',['as'=>'MasterHapusObat','uses'=>'Obat\ObatController@HapusObat']);
Route::get('/obat/ubah/{id}',['as'=>'UbahObat','uses'=>'Obat\ObatController@UbahObat']);
Route::post('/obat/ubah/{id}/simpan',['as'=>'SimpanUbahObat','uses'=>'Obat\ObatController@SimpanUbahObat']);

//Pembelian Obat
Route::get('/obat/pembelian',['as'=>'PembelianObat','uses'=>'Obat\ObatController@PembelianObat']);
Route::get('/obat/pembelian/tambah',['as'=>'TambahPembelianObat','uses'=>'Obat\ObatController@TambahPembelianObat']);
Route::post('/obat/pembelian/simpan',['as'=>'SimpanBeliObat','uses'=>'Obat\ObatController@SimpanBeliObat']);
Route::post('/obat/hapus',['as'=>'HapusObat','uses'=>'Obat\ObatController@HapusObat']);
Route::get('/obat/ubah/{id}',['as'=>'UbahObat','uses'=>'Obat\ObatController@UbahObat']);
Route::post('/obat/ubah/{id}/simpan',['as'=>'SimpanUbahObat','uses'=>'Obat\ObatController@SimpanUbahObat']);

//Poliklinik
Route::get('/poli',['as'=>'DaftarPoli','uses'=>'Poli\PoliController@DaftarPoli']);
Route::get('/poli/tambah',['as'=>'TambahPoli','uses'=>'Poli\PoliController@TambahPoli']);
Route::post('/poli/simpan',['as'=>'SimpanTambahPoli','uses'=>'Poli\PoliController@SimpanTambahPoli']);
Route::get('/poli/hapus/{id}',['as'=>'HapusPoli','uses'=>'Poli\PoliController@HapusPoli']);
Route::get('/poli/ubah/{id}',['as'=>'UbahPoli','uses'=>'Poli\PoliController@UbahPoli']);
Route::post('/poli/ubah/{id}/simpan',['as'=>'SimpanUbahPoli','uses'=>'Poli\PoliController@SimpanUbahPoli']);

//Data Gejala
Route::get('/gejala',['as'=>'DaftarGejala','uses'=>'Master\MasterController@DaftarGejala']);
Route::get('/gejala/tambah',['as'=>'TambahGejala','uses'=>'Master\MasterController@TambahGejala']);
Route::post('/gejala/simpan',['as'=>'SimpanTambahGejala','uses'=>'Master\MasterController@SimpanTambahGejala']);
Route::get('/gejala/hapus/{id}',['as'=>'HapusGejala','uses'=>'Master\MasterController@HapusGejala']);

//Data Diagnosa
Route::get('/diagnosa',['as'=>'DaftarDiagnosa','uses'=>'Master\MasterController@DaftarDiagnosa']);
Route::get('/diagnosa/tambah',['as'=>'TambahDiagnosa','uses'=>'Master\MasterController@TambahDiagnosa']);
Route::post('/diagnosa/simpan',['as'=>'SimpanTambahDiagnosa','uses'=>'Master\MasterController@SimpanTambahDiagnosa']);
Route::get('/diagnosa/hapus/{id}',['as'=>'HapusDiagnosa','uses'=>'Master\MasterController@HapusDiagnosa']);
Route::get('/diagnosa/datagejaladiag/{id}',['as'=>'DataGejalaDiag','uses'=>'Master\MasterController@DataGejalaDiag']);
Route::get('/diagnosa/ajaxgejala/{id_diagnosa}/{id_gejala?}',['as'=>'AjaxGejalaDiag','uses'=>'Master\MasterController@AjaxGejalaDiag']);
Route::get('/diagnosa/ajaxhapusgejala/{id_diagnosa}/{id_gejala?}/',['as'=>'AjaxHapusGejala','uses'=>'Master\MasterController@AjaxHapusGejala']);
Route::get('/diagnosa/detail/{id}',['as'=>'DetailDiagnosa','uses'=>'Master\MasterController@DetailDiagnosa']);
Route::get('/diagnosa/ubah/{id}',['as'=>'UbahDiagnosa','uses'=>'Master\MasterController@UbahDiagnosa']);
Route::post('/diagnosa/simpanubah',['as'=>'SimpanUbahDiagnosa','uses'=>'Master\MasterController@SimpanUbahDiagnosa']);

//Data Master
Route::get('/penguji',['as'=>'DaftarPenguji','uses'=>'Master\MasterController@DaftarPenguji']);
Route::get('/mahasiswa',['as'=>'DaftarMahasiswa','uses'=>'Master\MasterController@DaftarMahasiswa']);
Route::get('/rubrik',['as'=>'DaftarRubrik','uses'=>'Master\MasterController@DaftarRubrik']);
Route::get('/rubrik/tambah',['as'=>'TambahRubrik','uses'=>'Master\MasterController@TambahRubrik']);
Route::post('/rubrik/simpan',['as'=>'SimpanTambahRubrik','uses'=>'Master\MasterController@SimpanTambahRubrik']);
Route::post('/rubrik/simpanmateri',['as'=>'SimpanMateri','uses'=>'Master\MasterController@SimpanMateri']);
Route::get('/rubrik/setting/{id}/{id_materi?}',['as'=>'SettingRubrik','uses'=>'Master\MasterController@SettingRubrik']);
Route::get('/rubrik/ajaxmateri/{id_rubrik}/{materi?}/{ks0?}/{ks1?}/{ks2?}',['as'=>'AjaxMateri','uses'=>'Master\MasterController@AjaxMateri']);
Route::get('/rubrik/ajaxhapusmateri/{id_rubrik}/{id_materi?}/',['as'=>'AjaxHapusMateri','uses'=>'Master\MasterController@AjaxHapusMateri']);
Route::get('/rubrik/hapus/{id}',['as'=>'HapusRubrik','uses'=>'Master\MasterController@HapusRubrik']);
Route::get('/ujian',['as'=>'DaftarUjian','uses'=>'Master\MasterController@DaftarUjian']);
Route::get('/ujian/hapus/{id}',['as'=>'HapusUjian','uses'=>'Master\MasterController@HapusUjian']);
Route::get('/ujian/stase/{id}/{id_stase?}',['as'=>'StaseUjian','uses'=>'Master\MasterController@StaseUjian']);
Route::get('/ujian/ajaxstase/{id_ujian}/{stase?}/{penguji?}/{rubrik?}',['as'=>'AjaxStase','uses'=>'Master\MasterController@AjaxStase']);
Route::get('/ujian/tambah',['as'=>'TambahUjian','uses'=>'Master\MasterController@TambahUjian']);
Route::post('/ujian/simpan',['as'=>'SimpanTambahUjian','uses'=>'Master\MasterController@SimpanTambahUjian']);
Route::get('/rubrik/ajaxhapusstase/{id_ujian}/{id_stase?}/',['as'=>'AjaxHapusStase','uses'=>'Master\MasterController@AjaxHapusStase']);
Route::get('/ujian/plotmhs/{id}/{id_stase?}',['as'=>'PlotMahasiswa','uses'=>'Master\MasterController@PlotMahasiswa']);
Route::get('/ujian/ajaxangkatan/{angkatan}',['as'=>'AjaxAngkatan','uses'=>'Master\MasterController@AjaxAngkatan']);
Route::post('/ujian/simpan_mahasiswa',['as'=>'SimpanMahasiswa','uses'=>'Master\MasterController@SimpanMahasiswa']);
Route::get('/ujian/rekapnilai/{id}',['as'=>'RekapNilai','uses'=>'Master\MasterController@RekapNilai']);
Route::get('/ujian/nilaisatu/{id}',['as'=>'NilaiSatu','uses'=>'Master\MasterController@NilaiSatu']);
Route::get('/ujian/exportrekap/{id}',['as'=>'ExportRekap','uses'=>'Master\MasterController@ExportRekap']);

Route::get('/ujiandos',['as'=>'DaftarUjianDosen','uses'=>'Master\UjianController@DaftarUjianDosen']);
Route::get('/ujian/nilaiujian/{id}',['as'=>'NilaiUjian','uses'=>'Master\UjianController@NilaiUjian']);
Route::post('/ujian/ubahmahasiswa',['as'=>'UbahMahasiswa','uses'=>'Master\UjianController@UbahMahasiswa']);
Route::get('/ujian/nilaiujianx/{id}/{stase}/{nim}',['as'=>'NilaiUjianx','uses'=>'Master\UjianController@NilaiUjianx']);
Route::post('/ujian/simpan_nilai',['as'=>'SimpanNilai','uses'=>'Master\UjianController@SimpanNilai']);
Route::get('/ujian/rekapnilaidosen/{id}',['as'=>'RekapNilaiDosen','uses'=>'Master\UjianController@RekapNilai']);
Route::get('/ujian/nilaisatudosen/{id}',['as'=>'NilaiSatuDosen','uses'=>'Master\UjianController@NilaiSatu']);
Route::get('/ujian/exportrekapdosen/{id}',['as'=>'ExportRekapDosen','uses'=>'Master\UjianController@ExportRekap']);
//blokujian
Route::get('/blokujian',['as'=>'DaftarBlokUjian','uses'=>'Master\MasterController@DaftarBlokUjian']);
Route::get('/blokujian/tambah',['as'=>'TambahBlokUjian','uses'=>'Master\MasterController@TambahBlokUjian']);
Route::post('/blokujian/simpan',['as'=>'SimpanTambahBlokUjian','uses'=>'Master\MasterController@SimpanTambahBlokUjian']);
Route::get('/blokujian/hapus/{id}',['as'=>'HapusBlokUjian','uses'=>'Master\MasterController@HapusBlokUjian']);
Route::get('/blokujian/rekapnilai/{id}',['as'=>'RekapNilaiBlokUjian','uses'=>'Master\MasterController@RekapNilaiBlokUjian']);
Route::get('/blokujian/exportrekap/{id}',['as'=>'ExportRekapBlokUjian','uses'=>'Master\MasterController@ExportRekapBlokUjian']);

//Front Office
Route::get('/pasien/baru',['as'=>'PasienBaru','uses'=>'Front\PasienController@PasienBaru']);
Route::get('/pasien/baru_asuransi',['as'=>'PasienBaruAsuransi','uses'=>'Front\PasienController@PasienBaruAsuransi']);
Route::post('/pasien/simpan',['as'=>'SimpanTambahPasienBaru','uses'=>'Front\PasienController@SimpanTambahPasienBaru']);
Route::get('/pasien/cekdataasuransi/{no_peserta}',['as'=>'CekDataAsuransi','uses'=>'Front\PasienController@CekDataAsuransi']);
Route::get('/pasien/daftar',['as'=>'FODaftarPasien','uses'=>'Front\PasienController@DaftarPasien']);
Route::get('/pasien/ubah/{id}',['as'=>'FOUbahPasien','uses'=>'Front\PasienController@UbahPasien']);
Route::post('/pasien/ubah/{id}/simpan',['as'=>'SimpanUbahPasien','uses'=>'Front\PasienController@SimpanUbahPasien']);
Route::get('/pasien/hapus/{id}',['as'=>'FOHapusPasien','uses'=>'Front\PasienController@HapusPasien']);
Route::get('/pemeriksaan/tambahpasien',['as'=>'TambahPraktik','uses'=>'Front\JadwalPraktikController@FormJadwalHarian']);
Route::get('/pemeriksaan/tambahpasienpantom',['as'=>'TambahPraktikPantom','uses'=>'Front\JadwalPraktikController@FormJadwalHarianPantom']);
Route::get('/pemeriksaan/ajaxjadwal/{dokter?}/{poli?}/{tgl?}',['as'=>'AjaxAmbilJadwal','uses'=>'Front\JadwalPraktikController@AjaxAmbilJadwal']);
Route::get('/pemeriksaan/ajaxjadwalharian/{dokter?}/{poli?}/{tgl?}',['as'=>'AjaxAmbilJadwalHarian','uses'=>'Front\JadwalPraktikController@AjaxAmbilHarian']);
Route::get('/pemeriksaan/ajaxambilonline/{dokter?}/{poli?}/{tgl?}',['as'=>'AjaxAmbilOnline','uses'=>'Front\JadwalPraktikController@AjaxAmbilOnline']);
Route::get('/pemeriksaan/ajaxpasien/{rekam_medis?}',['as'=>'AjaxAmbilPasien','uses'=>'Front\JadwalPraktikController@AjaxAmbilPasien']);
Route::post('/pemeriksaan/tambahpasien/simpan',['as'=>'SimpanTambahPraktik','uses'=>'Front\JadwalPraktikController@SimpanJadwalHarian']);
Route::post('/pemeriksaan/tambahpasien/simpanpantom',['as'=>'SimpanTambahPraktikPantom','uses'=>'Front\JadwalPraktikController@SimpanJadwalHarianPantom']);

Route::get('/pemeriksaan/ubahonline/{id}',['as'=>'UbahOnline','uses'=>'Front\JadwalPraktikController@FormUbahOnline']);
Route::get('/pemeriksaan/ubahoterkonfirmasi/{id}',['as'=>'UbahTerkonfirmasi','uses'=>'Front\JadwalPraktikController@FormUbahTerkonfirmasi']);
Route::post('/pemeriksaan/ubahonline/simpan',['as'=>'SimpanUbahOnline','uses'=>'Front\JadwalPraktikController@SimpanUbahOnline']);
Route::post('/pemeriksaan/ubahterkonfirmasi/simpan',['as'=>'SimpanUbahTerkonfirmasi','uses'=>'Front\JadwalPraktikController@SimpanUbahTerkonfirmasi']);
Route::get('/pemeriksaan/batalkanpasien/{id}/{id_jadwal}',['as'=>'BatalkanPasien','uses'=>'Front\JadwalPraktikController@BatalkanPasien']);

Route::get('/pemeriksaan/cari',['as'=>'CariDaftarPemeriksaan','uses'=>'Front\JadwalPraktikController@CariDaftarPemeriksaan']);
Route::get('/pemeriksaan/online',['as'=>'DaftarPemeriksaanOnline','uses'=>'Front\JadwalPraktikController@DaftarPemeriksaanOnline']);
Route::get('/pemeriksaan/cari/{id}',['as'=>'DetilCariDaftarPemeriksaan','uses'=>'Front\JadwalPraktikController@DetilCariDaftarPemeriksaan']);
Route::get('/pemeriksaan/konfirmasionline/{id_calon}',['as'=>'KonfirmasiOnline','uses'=>'Front\JadwalPraktikController@KonfirmasiOnline']);
Route::post('/pemeriksaan/cari/konfirmasi',['as'=>'KonfirmasiKehadiranPasien','uses'=>'Front\JadwalPraktikController@KonfirmasiKehadiranPasien']);
Route::get('/pemeriksaan/konfirmasi',['as'=>'FormKonfirmasiPasien','uses'=>'Front\JadwalPraktikController@FormKonfirmasiPasien']);
Route::get('/pemeriksaan/konfirmasi/{rm}',['as'=>'FormKonfirmasiPasienRM','uses'=>'Front\JadwalPraktikController@FormKonfirmasiPasienRM']);
Route::get('/fo/dokter',['as'=>'FormKonfirmasiDokter','uses'=>'Front\KonfirmasiDokter@FormKonfirmasi']);
Route::get('/fo/dokter/{tgl}',['as'=>'DaftarKonfirmasiDokter','uses'=>'Front\KonfirmasiDokter@DaftarKonfirmasi']);
Route::match(['get','post'],'/fo/dokter/{tgl}/konfirmasi',['as'=>'SimpanKonfirmasiDokter','uses'=>'Front\KonfirmasiDokter@SimpanKonfirmasi']);
Route::get('/pemeriksaan/tambah/radiografi',['as'=>'TambahPemeriksaanJadwalRadiografi','uses'=>'Front\RadiografiController@PendaftaranJadwalRadiografi']);
Route::post('/pemeriksaan/tambah/radiografi',['as'=>'TambahPemeriksaanRadiografi','uses'=>'Front\RadiografiController@PendaftaranRadiografi']);
Route::get('/pemeriksaan/tambah/radiografi/ajaxradiologi/{id_periksa}/{nogigi?}/{tindakan?}/{baru?}',['as'=>'FOAjaxRadiologi','uses'=>'Front\RadiografiController@AjaxRadiologi']);
Route::get('/pemeriksaan/tambah/radiografi/hapusajaxradiologi/{id_periksa}/{tindakan?}/{grup?}',['as'=>'FOHapusAjaxRadiologi','uses'=>'Front\RadiografiController@HapusAjaxRadiologi']);
Route::post('/pemeriksaan/tambah/radiografi/simpan',['as'=>'SimpanTambahPemeriksaanRadiografi','uses'=>'Front\RadiografiController@SimpanPendaftaranRadiografi']);
Route::get('/pemeriksaan/tambah/lab',['as'=>'TambahPemeriksaanJadwalLab','uses'=>'Front\LabController@PendaftaranJadwalLab']);
Route::post('/pemeriksaan/tambah/labsimpan',['as'=>'TambahPemeriksaanLab','uses'=>'Front\LabController@PendaftaranLab']);
Route::post('/pemeriksaan/tambah/labsimpantindakan',['as'=>'SimpanPendaftaranLab','uses'=>'Front\LabController@SimpanPendaftaranLab']);


//Apoteker
Route::get('/apoteker/obat',['as'=>'ApotekerDaftarObat','uses'=>'Apoteker\ObatController@DaftarObat']);
Route::get('/apoteker/obat/tambah',['as'=>'ApotekerTambahObat','uses'=>'Apoteker\ObatController@TambahObat']);
Route::post('/apoteker/obat/simpan',['as'=>'ApotekerSimpanTambahObat','uses'=>'Apoteker\ObatController@SimpanTambahObat']);
Route::get('/apoteker/obat/hapus/{id}',['as'=>'ApotekerHapusObat','uses'=>'Apoteker\ObatController@HapusObat']);
Route::get('/apoteker/obat/ubah/{id}',['as'=>'ApotekerUbahObat','uses'=>'Apoteker\ObatController@UbahObat']);
Route::post('/apoteker/obat/ubah/{id}/simpan',['as'=>'ApotekerSimpanUbahObat','uses'=>'Apoteker\ObatController@SimpanUbahObat']);
Route::get('/apoteker/antrian',['as'=>'ApotekerDaftarAntrian','uses'=>'Apoteker\ObatController@DaftarAntrian']);
Route::get('/apoteker/antrian/{id}',['as'=>'ApotekerDetilAntrian','uses'=>'Apoteker\ObatController@DetilAntrian']);
Route::post('/apoteker/antrian/simpansiap',['as'=>'ApotekerObatSiap','uses'=>'Apoteker\ObatController@SimpanObatSiap']);
Route::get('/apoteker/antriansiap',['as'=>'ApotekerDaftarAntrianSiap','uses'=>'Apoteker\ObatController@DaftarAntrianSiap']);
Route::get('/apoteker/antriansiap/{id}',['as'=>'ApotekerDetilAntrianSiap','uses'=>'Apoteker\ObatController@DetilAntrianSiap']);
Route::post('/apoteker/antriansiap/simpanditerima',['as'=>'ApotekerObatDiterima','uses'=>'Apoteker\ObatController@SimpanObatDiterima']);
Route::get('/apoteker/antrianselesai',['as'=>'ApotekerDaftarSelesai','uses'=>'Apoteker\ObatController@DaftarSelesai']);
Route::get('/apoteker/antrianselesai/{id}',['as'=>'ApotekerDetilSelesai','uses'=>'Apoteker\ObatController@DetilSelesai']);
Route::post('/apoteker/antrianselesaiganti',['as'=>'ApotekerDaftarSelesaiGanti','uses'=>'Apoteker\ObatController@DaftarSelesaiGanti']);
Route::get('/apoteker/historiobat',['as'=>'HistoriObat','uses'=>'Apoteker\ObatController@HistoriObat']);
Route::post('/apoteker/historiobattanggal',['as'=>'HistoriObatTanggal','uses'=>'Apoteker\ObatController@HistoriObatTanggal']);
Route::get('/apoteker/historiobatintegrasi',['as'=>'HistoriObatIntegrasi','uses'=>'Apoteker\ObatController@HistoriObatIntegrasi']);
Route::post('/apoteker/historiobatintegrasitanggal',['as'=>'HistoriObatIntegrasiTanggal','uses'=>'Apoteker\ObatController@HistoriObatIntegrasiTanggal']);
Route::get('/apoteker/export/{dari}/{sampai}',['as'=>'ExportHistoriObat','uses'=>'Apoteker\ObatController@ExportHistoriObat']);
Route::get('/apoteker/exportintegrasi/{dari}/{sampai}',['as'=>'ExportHistoriObatIntegrasi','uses'=>'Apoteker\ObatController@ExportHistoriObatIntegrasi']);
Route::get('/apoteker/buktilistobat/{rm?}',['as'=>'BuktiListObat','uses'=>'Apoteker\ObatController@BuktiListObat']);
Route::get('/apoteker/buktilistobatdetail/{rm}/{id}',['as'=>'BuktiListObatDetail','uses'=>'Apoteker\ObatController@BuktiListObatDetail']);
Route::get('/apoteker/cetakbuktilistobat/{rm}/{id}',['as'=>'CetakBuktiListObat','uses'=>'Apoteker\ObatController@CetakBuktiListObat']);
Route::get('/apoteker/obat/pembelian',['as'=>'PembelianObat','uses'=>'Apoteker\ObatController@PembelianObat']);
Route::post('/apoteker/obat/simpanpembelian',['as'=>'SimpanPembelianObat','uses'=>'Apoteker\ObatController@SimpanPembelianObat']);
Route::get('/apoteker/obat/pembelianlist/{id}',['as'=>'PembelianList','uses'=>'Apoteker\ObatController@PembelianList']);
Route::post('/apoteker/obat/simpanpembelianlist',['as'=>'SimpanPembelianList','uses'=>'Apoteker\ObatController@SimpanPembelianList']);
Route::get('/apoteker/obat/datapembelian',['as'=>'DataPembelianObat','uses'=>'Apoteker\ObatController@DataPembelianObat']);
Route::get('/apoteker/obat/detailpembelianobat/{id}',['as'=>'DetailPembelianObat','uses'=>'Apoteker\ObatController@DetailPembelianObat']);


//Karyawan
Route::get('/karyawan',['as'=>'DaftarKaryawan','uses'=>'Karyawan\KaryawanController@DaftarKaryawan']);
Route::get('/karyawan/tambah',['as'=>'TambahKaryawan','uses'=>'Karyawan\KaryawanController@TambahKaryawan']);
Route::post('/karyawan/simpan',['as'=>'SimpanTambahKaryawan','uses'=>'Karyawan\KaryawanController@SimpanTambahKaryawan']);
Route::get('/karyawan/hapus/{id}',['as'=>'HapusKaryawan','uses'=>'Karyawan\KaryawanController@HapusKaryawan']);
Route::get('/karyawan/ubah/{id}',['as'=>'UbahKaryawan','uses'=>'Karyawan\KaryawanController@UbahKaryawan']);
Route::post('/karyawan/{id}/simpan',['as'=>'SimpanUbahKaryawan','uses'=>'Karyawan\KaryawanController@SimpanUbahKaryawan']);
//User
Route::get('/user',['as'=>'DaftarUser','uses'=>'User\UserController@DaftarUser']);
Route::get('/user/tambah',['as'=>'TambahUser','uses'=>'User\UserController@TambahUser']);
Route::post('/user/simpan',['as'=>'SimpanTambahUser','uses'=>'User\UserController@SimpanTambahUser']);
Route::get('/user/hapus/{id}',['as'=>'HapusUser','uses'=>'User\UserController@HapusUser']);
Route::get('/user/ubah/{id}',['as'=>'UbahUser','uses'=>'User\UserController@UbahUser']);
Route::post('/user/{id}/simpan',['as'=>'SimpanUbahUser','uses'=>'User\UserController@SimpanUbahUser']);


//Pemeriksaan
Route::get('/dokter/daftar_pasien',['as'=>'DaftarPasien','uses'=>'Dokter\DokterPeriksaController@DaftarPasien']);
Route::get('/dokter/pemeriksaan/{id}',['as'=>'PemeriksaanPasien','uses'=>'Dokter\DokterPeriksaController@PemeriksaanPasien']);
Route::get('/dokter/pemeriksaanwz/{id}',['as'=>'PemeriksaanPasienWizard','uses'=>'Dokter\DokterPeriksaController@PemeriksaanPasienWizard']);
Route::post('/dokter/pemeriksaanwz/{id}/soapdoang',['as'=>'SimpanSOAPDoang','uses'=>'Dokter\DokterPeriksaController@SimpanSOAPDoang']);
Route::post('/dokter/pemeriksaanwz/{id}/awaldoang',['as'=>'SimpanAwalDoang','uses'=>'Dokter\DokterPeriksaController@SimpanAwalDoang']);
Route::get('/dokter/odontogram/{rm}',['as'=>'DaftarOdontogramPasien','uses'=>'Dokter\DokterPeriksaController@DaftarOdontogramPasien']);
Route::get('/dokter/odontogram/{rm}/{id}',['as'=>'OdontogramDBPasien','uses'=>'Dokter\DokterPeriksaController@OdontogramDB']);
Route::post('/dokter/simpanperiksa',['as'=>'SimpanSoap','uses'=>'Dokter\DokterPeriksaController@SimpanSoap']);
Route::get('/dokter/ajaxtindakan/{id_periksa}/{tindakan?}/{persen?}',['as'=>'AjaxTindakan','uses'=>'Dokter\DokterPeriksaController@AjaxTindakan']);
Route::get('/dokter/ajaxhapustindakan/{id_periksa}/{id_pemeriksaan?}/',['as'=>'AjaxHapusTindakan','uses'=>'Dokter\DokterPeriksaController@AjaxHapusTindakan']);
Route::get('/dokter/ajaxobat/{id_periksa}/{obat?}/{qty?}',['as'=>'AjaxObat','uses'=>'Dokter\DokterPeriksaController@AjaxObat']);
Route::get('/dokter/ajaxhapusobat/{id_periksa}/{id_obat?}',['as'=>'AjaxHapusObat','uses'=>'Dokter\DokterPeriksaController@AjaxHapusObat']);
Route::get('/dokter/ajaxradiologi/{id_periksa}/{nogigi?}/{tindakan?}/{baru?}',['as'=>'AjaxRadiologi','uses'=>'Dokter\DokterPeriksaController@AjaxRadiologi']);
Route::get('/dokter/hapusajaxradiologi/{id_periksa}/{tindakan?}/{grup?}',['as'=>'HapusAjaxRadiologi','uses'=>'Dokter\DokterPeriksaController@HapusAjaxRadiologi']);
Route::get('/dokter/daftar_mahasiswa_bim',['as'=>'DaftarMhsBim','uses'=>'Dokter\DokterPeriksaController@DaftarMhsBim']);
Route::get('/dokter/daftar_pasien_mhs/{nim}',['as'=>'DaftarPasienMhs','uses'=>'Dokter\DokterPeriksaController@DaftarPasienMhs']);
Route::get('/dokter/daftar_pasien_mhs2/{nim}',['as'=>'DaftarPasienMhs2','uses'=>'Dokter\DokterPeriksaController@DaftarPasienMhs2']);
Route::get('/dokter/detail_mahasiswa_bim/{id_histori}/{nim}/',['as'=>'DetailMhsBim','uses'=>'Dokter\DokterPeriksaController@DetailMhsBim']);
Route::get('/dokter/penilaian_tindakan/{nim}/{id?}',['as'=>'PenilaianTindakan','uses'=>'Dokter\DokterPeriksaController@PenilaianTindakan']);
Route::get('/dokter/nilai_mahasiswa_bim/{id_histori}/{nim}',['as'=>'NilaiMhsBim','uses'=>'Dokter\DokterPeriksaController@NilaiMhsBim']);
Route::get('/dokter/input_nilai_mahasiswa_bim/{id_histori}/{id_tindakan}',['as'=>'InputNilaiMhsBim','uses'=>'Dokter\DokterPeriksaController@InputNilaiMhsBim']);
Route::get('/dokter/input_nilai_mahasiswa_bim2/{id_histori}/{id_tindakan}',['as'=>'InputNilaiMhsBim2','uses'=>'Dokter\DokterPeriksaController@InputNilaiMhsBim2']);
Route::post('/dokter/SimpanKonfirmasiMhs',['as'=>'SimpanKonfirmasiMhs','uses'=>'Dokter\DokterPeriksaController@SimpanKonfirmasiMhs']);
Route::post('/dokter/SimpanNilaiMhs',['as'=>'SimpanNilaiMhs','uses'=>'Dokter\DokterPeriksaController@SimpanNilaiMhs']);
Route::get('/dokter/ajaxodontogram/{rm}/{id}/{gigi}/{keluhan}',['as'=>'AjaxOdontogram','uses'=>'Dokter\DokterPeriksaController@AjaxOdontogram']);
Route::get('/dokter/daftar_pasien_selesai/{rm?}',['as'=>'DaftarPasienSelesai','uses'=>'Dokter\DokterPeriksaController@DaftarPasienSelesai']);
Route::get('/dokter/detail_pemeriksaan/{id}',['as'=>'DetailPemeriksaan','uses'=>'Dokter\DokterPeriksaController@DetailPemeriksaan']);
Route::get('/dokter/riwayatpasien/{rm}',['as'=>'DokterRiwayatPasien','uses'=>'Dokter\DokterPeriksaController@RiwayatPasien']);
Route::get('/dokter/doktindakan',['as'=>'DokDaftarTindakan','uses'=>'Dokter\DokterPeriksaController@DokDaftarTindakan']);
Route::get('/dokter/dokpresensi',['as'=>'DokPresensi','uses'=>'Dokter\DokterPeriksaController@DokPresensi']);
Route::post('/dokter/simpantambahpresensi',['as'=>'SimpanTambahPresensi','uses'=>'Dokter\DokterPeriksaController@SimpanTambahPresensi']);
Route::get('/dokter/rekappresensi/{dari}/{sampai}',['as'=>'RekapPresensi','uses'=>'Dokter\DokterPeriksaController@RekapPresensi']);
Route::post('/dokter/rekappresensitanggal',['as'=>'RekapPresensiTanggal','uses'=>'Dokter\DokterPeriksaController@RekapPresensiTanggal']);
Route::get('/dokter/mulairujukan',['as'=>'MulaiRujukan','uses'=>'Dokter\DokterPeriksaController@MulaiRujukan']);
Route::get('/dokter/tambahrujukan/{id}',['as'=>'TambahRujukan','uses'=>'Dokter\DokterPeriksaController@TambahRujukan']);
Route::get('/dokter/ajaxjadwalrujukan/{dokter?}/{poli?}/{tgl?}',['as'=>'AjaxAmbilJadwalRujukan','uses'=>'Dokter\DokterPeriksaController@AjaxAmbilJadwalRujukan']);
Route::post('/dokter/tambahpasienrujukan/simpan',['as'=>'SimpanTambahPraktikRujukan','uses'=>'Dokter\DokterPeriksaController@SimpanJadwalHarianRujukan']);


//Kasir
Route::get('/kasir',['as'=>'PembayaranPasien','uses'=>'Kasir\KasirController@PembayaranPasien']);
Route::get('/kasirrad',['as'=>'PembayaranPasienRadiografi','uses'=>'Kasir\KasirController@PembayaranPasienRadiografi']);
Route::get('/kasirlab',['as'=>'PembayaranPasienLab','uses'=>'Kasir\KasirController@PembayaranPasienLab']);
Route::get('/kasir/detail/{id}',['as'=>'PembayaranDetail','uses'=>'Kasir\KasirController@PembayaranDetail']);
Route::get('/kasir/detailrad/{id}',['as'=>'PembayaranDetailRadiografi','uses'=>'Kasir\KasirController@PembayaranDetailRadiografi']);
Route::get('/kasir/detaillab/{id}',['as'=>'PembayaranDetailLab','uses'=>'Kasir\KasirController@PembayaranDetailLab']);
Route::post('/kasir/detail/konfirmasi',['as'=>'PembayaranKonfirmasi','uses'=>'Kasir\KasirController@PembayaranKonfirmasi']);
Route::post('/kasir/detail/konfirmasirad',['as'=>'PembayaranKonfirmasiRadiografi','uses'=>'Kasir\KasirController@PembayaranKonfirmasiRadiografi']);
Route::post('/kasir/detail/konfirmasilab',['as'=>'PembayaranKonfirmasiLab','uses'=>'Kasir\KasirController@PembayaranKonfirmasiLab']);
Route::post('/kasir/detail/simpankonfirmasi',['as'=>'SimpanKonfirmasi','uses'=>'Kasir\KasirController@SimpanKonfirmasi']);
Route::post('/kasir/detail/simpankonfirmasirad',['as'=>'SimpanKonfirmasiRadiografi','uses'=>'Kasir\KasirController@SimpanKonfirmasiRadiografi']);
Route::post('/kasir/detail/simpankonfirmasilab',['as'=>'SimpanKonfirmasiLab','uses'=>'Kasir\KasirController@SimpanKonfirmasiLab']);
Route::get('/kasir/detail/suksesbayar/{id}',['as'=>'SuksesBayar','uses'=>'Kasir\KasirController@SuksesBayar']);
Route::get('/kasir/detail/suksesbayarrad/{id}',['as'=>'SuksesBayarRadiografi','uses'=>'Kasir\KasirController@SuksesBayarRadiografi']);
Route::get('/kasir/detail/suksesbayarlab/{id}',['as'=>'SuksesBayarLab','uses'=>'Kasir\KasirController@SuksesBayarLab']);
Route::get('/kasir/detail/cetak/{id}',['as'=>'PembayaranCetakStruk','uses'=>'Kasir\KasirController@CetakStruk']);
Route::get('/kasir/detail/cetakrad/{id}',['as'=>'PembayaranCetakStrukRadiografi','uses'=>'Kasir\KasirController@CetakStrukRadiografi']);
Route::get('/kasir/detail/cetaklab/{id}',['as'=>'PembayaranCetakStrukLab','uses'=>'Kasir\KasirController@CetakStrukLab']);
Route::get('/kasir/historipembayaran/{dari}/{sampai}',['as'=>'HistoriPembayaran','uses'=>'Kasir\KasirController@HistoriPembayaran']);
Route::post('/kasir/historipembayarantanggal',['as'=>'HistoriPembayaranTanggal','uses'=>'Kasir\KasirController@HistoriPembayaranTanggal']);
Route::get('/kasir/historipembayarandetail/{id}/{dari}/{sampai}',['as'=>'HistoriPembayaranDetail','uses'=>'Kasir\KasirController@HistoriPembayaranDetail']);
Route::get('/kasir/cetakpembayarandetail/{id}',['as'=>'CetakPembayaranDetail','uses'=>'Kasir\KasirController@CetakPembayaranDetail']);
Route::get('/kasir/export/{dari}/{sampai}',['as'=>'ExportHistoriPembayaran','uses'=>'Kasir\KasirController@ExportHistoriPembayaran']);

//Laboratorium
Route::get('/laborat',['as'=>'LaboratPasien','uses'=>'Lab\LabController@LaboratPasien']);
Route::get('/laboratlab',['as'=>'LaboratPasienLab','uses'=>'Lab\LabController@LaboratPasienLab']);
Route::get('/laborat/detail/{id}',['as'=>'LaboratPasienDetail','uses'=>'Lab\LabController@LaboratPasienDetail']);
Route::get('/laborat/detaillab/{id}',['as'=>'LaboratPasienDetailLab','uses'=>'Lab\LabController@LaboratPasienDetailLab']);
Route::get('/laborat/konfirmasi/{id}',['as'=>'KonfirmasiLaborat','uses'=>'Lab\LabController@KonfirmasiLaborat']);
Route::get('/laborat/konfirmasilab/{id}',['as'=>'KonfirmasiLaboratLab','uses'=>'Lab\LabController@KonfirmasiLaboratLab']);
Route::get('/laborat/suratpengantar/{id}',['as'=>'SuratPengantar','uses'=>'Lab\LabController@SuratPengantar']);
Route::get('/laborat/suratpengantarlab/{id}',['as'=>'SuratPengantarLab','uses'=>'Lab\LabController@SuratPengantarLab']);
Route::get('/hasil_lab',['as'=>'HasilLaborat','uses'=>'Lab\LabController@HasilLaborat']);

//Perawat
Route::get('/perawat/datapasien',['as'=>'PerawatDaftarPasien','uses'=>'Perawat\PerawatController@PerawatDaftarPasien']);
Route::get('/perawat/pemeriksaanawal/{id}',['as'=>'PemeriksaanAwal','uses'=>'Perawat\PerawatController@PemeriksaanAwal']);
Route::post('/perawat/simpanperiksaawal',['as'=>'SimpanPemeriksaanAwal','uses'=>'Perawat\PerawatController@SimpanPemeriksaanAwal']);
Route::get('/perawat/ubahpemeriksaan/{id}',['as'=>'UbahPemeriksaanAwal','uses'=>'Perawat\PerawatController@UbahPemeriksaanAwal']);
Route::post('/perawat/simpanubahpemeriksaan',['as'=>'SimpanUbahPemeriksaanAwal','uses'=>'Perawat\PerawatController@SimpanUbahPemeriksaanAwal']);
Route::get('/perawat/riwayatperiksa/{rm?}',['as'=>'PerawatRiwayatPeriksa','uses'=>'Perawat\PerawatController@RiwayatPeriksa']);
Route::get('/perawat/riwayatperiksa/{rm}/{id}',['as'=>'PerawatDetilRiwayatPeriksa','uses'=>'Perawat\PerawatController@DetilRiwayatPeriksa']);

//Laboratorium Gigi
Route::get('/labgigi',['as'=>'PemeriksaanLabGigi','uses'=>'LabGigi\LabGigiController@PemeriksaanLabGigi']);
Route::get('/labgigi/detail/{id}',['as'=>'PemeriksaanLabGigiDetail','uses'=>'LabGigi\LabGigiController@PemeriksaanLabGigiDetail']);
Route::get('/labgigi/konfirmasi/{id}',['as'=>'PemeriksaanLabGigiKonfirmasi','uses'=>'LabGigi\LabGigiController@PemeriksaanLabGigiKonfirmasi']);
Route::get('/labgigi/cetak/{id}',['as'=>'CetakLabGigi','uses'=>'LabGigi\LabGigiController@CetakLabGigi']);

//Klinik Integrasi
Route::get('/integrasi/plotdosen/{tanggal}',['as'=>'PlotDosen','uses'=>'Integrasi\IntegrasiController@PlotDosen']);
Route::post('/integrasi/gantitanggal',['as'=>'PlotGantiTanggal','uses'=>'Integrasi\IntegrasiController@PlotGantiTanggal']);
Route::get('/integrasi/tambahplot/{tanggal}',['as'=>'TambahPlot','uses'=>'Integrasi\IntegrasiController@TambahPlot']);
Route::post('/integrasi/simpantambahplot',['as'=>'SimpanTambahPlot','uses'=>'Integrasi\IntegrasiController@SimpanTambahPlot']);
Route::get('/integrasi/ubahstatus/{id}/{tanggal}',['as'=>'UbahStatus','uses'=>'Integrasi\IntegrasiController@UbahStatus']);
Route::get('/integrasi/integrasidaftarPasien',['as'=>'IntegrasiDaftarPasien','uses'=>'Integrasi\IntegrasiController@IntegrasiDaftarPasien']);
Route::get('/integrasi/pilihDokter/{id}',['as'=>'PilihDokter','uses'=>'Integrasi\IntegrasiController@PilihDokter']);
Route::post('/integrasi/simpanpilihdokter',['as'=>'SimpanPilihDokter','uses'=>'Integrasi\IntegrasiController@SimpanPilihDokter']);
Route::get('/integrasi/plotdatamhs/{id}',['as'=>'PlotDataMhs','uses'=>'Integrasi\IntegrasiController@PlotDataMhs']);
Route::get('/integrasi/hapus/{id}/{idplot}',['as'=>'HapusPlotMhs','uses'=>'Integrasi\IntegrasiController@HapusPlotMhs']);


//Mahasiswa
Route::get('/mahasiswa/plotdosenmhs/{tanggal}/{id}',['as'=>'PlotDosenMhs','uses'=>'Mahasiswa\MahasiswaController@PlotDosenMhs']);
Route::get('/mahasiswa/pilihdosen/{id}/{id_integrasi}',['as'=>'PilihDosen','uses'=>'Mahasiswa\MahasiswaController@PilihDosen']);
Route::get('/mahasiswa/daftarpasien',['as'=>'MhsDaftarPasien','uses'=>'Mahasiswa\MahasiswaController@DaftarPasien']);
Route::get('/mahasiswa/pemeriksaanwz/{id}/{id_plot}',['as'=>'MhsPemeriksaanPasienWizard','uses'=>'Mahasiswa\MahasiswaController@PemeriksaanPasienWizard']);
Route::post('/mahasiswa/simpanperiksa',['as'=>'MhsSimpanSoap','uses'=>'Mahasiswa\MahasiswaController@SimpanSoap']);
Route::get('/mahasiswa/ajaxdaftartindakan/{id_modul}',['as'=>'MhsAjaxDaftarTindakan','uses'=>'Mahasiswa\MahasiswaController@AjaxDaftarTindakan']);
Route::get('/mahasiswa/ajaxtindakan/{id_periksa}/{id_histori}/{tindakan?}',['as'=>'MhsAjaxTindakan','uses'=>'Mahasiswa\MahasiswaController@AjaxTindakan']);
Route::get('/mahasiswa/ajaxobat/{id_periksa}/{obat?}/{qty?}',['as'=>'MhsAjaxObat','uses'=>'Mahasiswa\MahasiswaController@AjaxObat']);
Route::get('/mahasiswa/ajaxradiologi/{id_periksa}/{id_histori}/{nogigi?}/{tindakan?}',['as'=>'MhsAjaxRadiologi','uses'=>'Mahasiswa\MahasiswaController@AjaxRadiologi']);
Route::get('/mahasiswa/historiperiksa/{id}',['as'=>'MhsDetilHistoriPeriksa','uses'=>'Mahasiswa\MahasiswaController@HistoriPeriksa']);
Route::get('/mahasiswa/daftarpasiennilai',['as'=>'MhsDaftarPasienNilai','uses'=>'Mahasiswa\MahasiswaController@DaftarPasienNilai']);
Route::get('/mahasiswa/obat/{id_periksa}/{id_histori}',['as'=>'MhsObat','uses'=>'Mahasiswa\MahasiswaController@Obat']);
Route::get('/mahasiswa/obat/{id_periksa}/{id_histori}/tambah',['as'=>'MhsTambahObat','uses'=>'Mahasiswa\MahasiswaController@TambahObat']);
Route::post('/mahasiswa/obat/{id_periksa}/{id_histori}/simpan',['as'=>'SimpanMhsTambahObat','uses'=>'Mahasiswa\MahasiswaController@SimpanTambahObat']);
Route::get('/mahasiswa/penunjang/{id_periksa}/{id_histori}',['as'=>'MhsTambahPenunjang','uses'=>'Mahasiswa\MahasiswaController@TambahPenunjang']);
Route::post('/mahasiswa/penunjang/{id_periksa}/{id_histori}/simpan',['as'=>'SimpanMhsTambahPenunjang','uses'=>'Mahasiswa\MahasiswaController@SimpanTambahPenunjang']);
Route::get('/mahasiswa/ubahtindakan/{id_periksa}/{id_histori}',['as'=>'MhsUbahTindakan','uses'=>'Mahasiswa\MahasiswaController@UbahTindakan']);
Route::get('/mahasiswa/ajaxhapustindakan/{id_periksa}/{id_histori}/{id_tindakan_pemeriksaan}',['as'=>'MhsAjaxHapusTindakan','uses'=>'Mahasiswa\MahasiswaController@AjaxHapusTindakan']);
Route::get('/mahasiswa/bahan/{id_periksa}/{id_histori}',['as'=>'MhsBahan','uses'=>'Mahasiswa\MahasiswaController@Bahan']);
Route::get('/mahasiswa/bahan/{id_periksa}/{id_histori}/tambah',['as'=>'MhsTambahBahan','uses'=>'Mahasiswa\MahasiswaController@TambahBahan']);
Route::post('/mahasiswa/bahan/{id_periksa}/{id_histori}/simpan',['as'=>'SimpanMhsTambahBahan','uses'=>'Mahasiswa\MahasiswaController@SimpanTambahBahan']);
Route::get('/mahasiswa/lihattindakan/{id_histori}',['as'=>'LihatTindakanMhs','uses'=>'Mahasiswa\MahasiswaController@LihatTindakanMhs']);
Route::get('/mahasiswa/lihat_nilai_mhs/{id_histori}/{id_tindakan}',['as'=>'LihatNilaiMhs','uses'=>'Mahasiswa\MahasiswaController@LihatNilaiMhs']);
Route::get('/mahasiswa/ubahdosen/{tanggal}/{id}',['as'=>'UbahDosenMhs','uses'=>'Mahasiswa\MahasiswaController@UbahDPJP']);
Route::post('/mahasiswa/ubahdosen/simpan',['as'=>'SimpanUbahDosenMhs','uses'=>'Mahasiswa\MahasiswaController@SimpanUbahDPJP']);

//Radiografi
Route::get('/radiografi',['as'=>'PemeriksaanRadiografi','uses'=>'Radiografi\RadiografiController@PemeriksaanRadiografi']);
Route::get('/radradiografi',['as'=>'RadPemeriksaanRadiografi','uses'=>'Radiografi\RadiografiController@RadPemeriksaanRadiografi']);
Route::get('/radiografi/detail/{id}',['as'=>'PemeriksaanRadiografiDetail','uses'=>'Radiografi\RadiografiController@PemeriksaanRadiografiDetail']);
Route::get('/radradiografi/detail/{id}',['as'=>'RadPemeriksaanRadiografiDetail','uses'=>'Radiografi\RadiografiController@RadPemeriksaanRadiografiDetail']);
Route::post('/radiografi/konfirmasi',['as'=>'KonfirmasiRadiografi','uses'=>'Radiografi\RadiografiController@KonfirmasiRadiografi']);
Route::post('/radradiografi/konfirmasi',['as'=>'RadKonfirmasiRadiografi','uses'=>'Radiografi\RadiografiController@RadKonfirmasiRadiografi']);
Route::get('/radiografi/suratpengantar/{id}',['as'=>'CetakSuratPengantar','uses'=>'Radiografi\RadiografiController@CetakSuratPengantar']);
Route::get('/hasil_radiografi',['as'=>'HasilPemeriksaanRadiografi','uses'=>'Radiografi\RadiografiController@HasilPemeriksaanRadiografi']);

//Admin Radiografi
Route::get('/admin/radiografi',['as'=>'AdminPemeriksaanRadiografi','uses'=>'AdminRadiografi\AdminRadiografiController@AdminPemeriksaanRadiografi']);
Route::get('/admin/radradiografi',['as'=>'RadAdminPemeriksaanRadiografi','uses'=>'AdminRadiografi\AdminRadiografiController@RadAdminPemeriksaanRadiografi']);
Route::get('/admin/radiografi/detail/{id}',['as'=>'AdminPemeriksaanRadiografiDetail','uses'=>'AdminRadiografi\AdminRadiografiController@AdminPemeriksaanRadiografiDetail']);
Route::get('/admin/radradiografi/detail/{id}',['as'=>'RadAdminPemeriksaanRadiografiDetail','uses'=>'AdminRadiografi\AdminRadiografiController@RadAdminPemeriksaanRadiografiDetail']);
Route::post('/admin/radiografi/konfirmasi',['as'=>'AdminKonfirmasiRadiografi','uses'=>'AdminRadiografi\AdminRadiografiController@AdminKonfirmasiRadiografi']);
Route::post('/admin/radradiografi/konfirmasi',['as'=>'RadAdminKonfirmasiRadiografi','uses'=>'AdminRadiografi\AdminRadiografiController@RadAdminKonfirmasiRadiografi']);
Route::get('/admin/radiografi/suratpengantar/{id}',['as'=>'AdminCetakSuratPengantar','uses'=>'AdminRadiografi\AdminRadiografiController@AdminCetakSuratPengantar']);
Route::get('/admin/radradiografi/suratpengantar/{id}',['as'=>'RadAdminCetakSuratPengantar','uses'=>'AdminRadiografi\AdminRadiografiController@RadAdminCetakSuratPengantar']);
Route::get('/admin/hasil_radiografi',['as'=>'AdminHasilPemeriksaanRadiografi','uses'=>'AdminRadiografi\AdminRadiografiController@AdminHasilPemeriksaanRadiografi']);
Route::get('/admin/radhasil_radiografi',['as'=>'RadAdminHasilPemeriksaanRadiografi','uses'=>'AdminRadiografi\AdminRadiografiController@RadAdminHasilPemeriksaanRadiografi']);

//Perawat Integrasi
Route::get('/perawatintegrasi/datamhs',['as'=>'DataMhs','uses'=>'PerawatIntegrasi\PerawatIntegrasiController@DataMhs']);
Route::get('/perawatintegrasi/daftar_pasien_mhs/{nim}',['as'=>'PasienMhs','uses'=>'PerawatIntegrasi\PerawatIntegrasiController@DaftarPasienMhs']);
Route::get('/perawatintegrasi/konfirmasi/{id_histori}/{nim}/',['as'=>'KonfirmasiTindakan','uses'=>'PerawatIntegrasi\PerawatIntegrasiController@KonfirmasiTindakan']);
//Route::get('/perawatintegrasi/bahan/{id_periksa}/{id_histori}/{nim}',['as'=>'PerawatTambahBahan','uses'=>'PerawatIntegrasi\PerawatIntegrasiController@TambahBahan']);

//Kasir Klinik Integrasi
Route::get('/kasirintegrasi',['as'=>'PembayaranPasienIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@PembayaranPasienIntegrasi']);
Route::get('/kasirintegrasi/detail/{id}',['as'=>'PembayaranDetailIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@PembayaranDetailIntegrasi']);
Route::post('/kasirintegrasi/detail/konfirmasi',['as'=>'PembayaranKonfirmasiIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@PembayaranKonfirmasiIntegrasi']);
Route::post('/kasirintegrasi/detail/simpankonfirmasi',['as'=>'SimpanKonfirmasiIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@SimpanKonfirmasiIntegrasi']);
Route::get('/kasirintegrasi/detail/suksesbayar/{id}',['as'=>'SuksesBayarIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@SuksesBayarIntegrasi']);
Route::get('/kasirintegrasi/detail/cetak/{id}',['as'=>'PembayaranCetakStrukIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@CetakStruk']);
Route::get('/kasirintegrasi/historipembayaranintegrasi/{dari}/{sampai}',['as'=>'HistoriPembayaranIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@HistoriPembayaranIntegrasi']);
Route::post('/kasirintegrasi/historipembayarantanggalintegrasi',['as'=>'HistoriPembayaranTanggalIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@HistoriPembayaranTanggalIntegrasi']);
Route::get('/kasirintegrasi/historipembayarandetailintegrasi/{id}/{dari}/{sampai}',['as'=>'HistoriPembayaranDetailIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@HistoriPembayaranDetailIntegrasi']);
Route::get('/kasirintegrasi/cetakpembayarandetailintegrasi/{id}',['as'=>'CetakPembayaranDetailIntegrasi','uses'=>'KasirIntegrasi\KasirIntegrasiController@CetakPembayaranDetailIntegrasi']);


#admin Profesi
Route::get('/admp/daftardu',['as'=>'AdminProfesiDaftarDU','uses'=>'AdminProfesi\AdminProfesiController@DaftarDUMahasiswa']);
Route::get('/admp/daftardu/tambah',['as'=>'AdminProfesiFormTambahPlotDUMahasiswa','uses'=>'AdminProfesi\AdminProfesiController@FormTambahPlotDUMahasiswa']);
Route::post('/admp/daftardu/simpan',['as'=>'AdminProfesiSimpanFormTambahPlotDUMahasiswa','uses'=>'AdminProfesi\AdminProfesiController@SimpanFormTambahPlotDUMahasiswa']);

Route::get('/admp/plotdosen/{tanggal}',['as'=>'AdminProfesiPlotDosen','uses'=>'AdminProfesi\AdminProfesiController@PlotDosen']);
Route::post('/admp/gantitanggal',['as'=>'AdminProfesiPlotGantiTanggal','uses'=>'AdminProfesi\AdminProfesiController@PlotGantiTanggal']);
Route::get('/admp/tambahplot/{tanggal}',['as'=>'AdminProfesiTambahPlot','uses'=>'AdminProfesi\AdminProfesiController@TambahPlot']);
Route::post('/admp/simpantambahplot',['as'=>'AdminProfesiSimpanTambahPlot','uses'=>'AdminProfesi\AdminProfesiController@SimpanTambahPlot']);
Route::get('/admp/ubahstatus/{id}/{tanggal}',['as'=>'AdminProfesiUbahStatus','uses'=>'AdminProfesi\AdminProfesiController@UbahStatus']);
Route::get('/admp/pilihDokter/{id}',['as'=>'AdminProfesiPilihDokter','uses'=>'AdminProfesi\AdminProfesiController@PilihDokter']);
Route::post('/admp/simpanpilihdokter',['as'=>'AdminProfesiSimpanPilihDokter','uses'=>'AdminProfesi\AdminProfesiController@SimpanPilihDokter']);
Route::get('/admp/plotdatamhs/{id}',['as'=>'AdminProfesiPlotDataMhs','uses'=>'AdminProfesi\AdminProfesiController@PlotDataMhs']);
Route::get('/admp/hapus/{id}/{idplot}',['as'=>'AdminProfesiHapusPlotMhs','uses'=>'AdminProfesi\AdminProfesiController@HapusPlotMhs']);
Route::get('/admp/plotdatamhs/{id}/tambah',['as'=>'AdminProfesiTambahPlotDataMhs','uses'=>'AdminProfesi\AdminProfesiController@TambahPlotDataMhs']);
Route::match(['get','post'],'/admp/plotdatamhs/{id}/simpan',['as'=>'AdminProfesiSimpanTambahPlotDataMhs','uses'=>'AdminProfesi\AdminProfesiController@SimpanTambahPlotDataMhs']);

//Keuangan
Route::get('/keuangan/rekappendapatan/{tanggal_dari}/{tanggal_sampai}',['as'=>'RekapPendapatan','uses'=>'Keuangan\KeuanganController@RekapPendapatan']);
Route::post('/keuangan/rekappendapatangt',['as'=>'RekapPendapatanGantiTanggal','uses'=>'Keuangan\KeuanganController@RekapPendapatanGantiTanggal']);
Route::get('/keuangan/cetakpendapatan/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakPendapatan','uses'=>'Keuangan\KeuanganController@CetakPendapatan']);
Route::get('/keuangan/rekapbilling/{tanggal_dari}/{tanggal_sampai}',['as'=>'RekapBilling','uses'=>'Keuangan\KeuanganController@RekapBilling']);
Route::post('/keuangan/rekapbillinggt',['as'=>'RekapBillingGantiTanggal','uses'=>'Keuangan\KeuanganController@RekapBillingGantiTanggal']);
Route::get('/keuangan/cetakbilling/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakBilling','uses'=>'Keuangan\KeuanganController@CetakBilling']);

Route::get('/keuangan/rekappendapatanintegrasi/{tanggal_dari}/{tanggal_sampai}',['as'=>'RekapPendapatanIntegrasi','uses'=>'Keuangan\KeuanganController@RekapPendapatanIntegrasi']);
Route::post('/keuangan/rekappendapatanintegrasigt',['as'=>'RekapPendapatanIntegrasiGanti','uses'=>'Keuangan\KeuanganController@RekapPendapatanIntegrasiGanti']);
Route::get('/keuangan/cetakpendapatanintegrasi/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakPendapatanIntegrasi','uses'=>'Keuangan\KeuanganController@CetakPendapatanIntegrasi']);
Route::get('/keuangan/rekapbillingintegrasi/{tanggal_dari}/{tanggal_sampai}',['as'=>'RekapBillingIntegrasi','uses'=>'Keuangan\KeuanganController@RekapBillingIntegrasi']);
Route::post('/keuangan/rekapbillingintegrasigt',['as'=>'RekapBillingIntegrasiGanti','uses'=>'Keuangan\KeuanganController@RekapBillingIntegrasiGanti']);
Route::get('/keuangan/cetakbillingintegrasi/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakBillingIntegrasi','uses'=>'Keuangan\KeuanganController@CetakBillingIntegrasi']);
Route::get('/keuangan/pendapatandokter',['as'=>'PendapatanDokter','uses'=>'Keuangan\KeuanganController@PendapatanDokter']);
Route::post('/keuangan/hasilpendapatandokter',['as'=>'HasilPendapatanDokter','uses'=>'Keuangan\KeuanganController@HasilPendapatanDokter']);
Route::get('/keuangan/cetakpendapatandokter/{kode_dokter}/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakPendapatanDokter','uses'=>'Keuangan\KeuanganController@CetakPendapatanDokter']);
Route::get('/keuangan/pendapatandokterintegrasi',['as'=>'PendapatanDokterIntegrasi','uses'=>'Keuangan\KeuanganController@PendapatanDokterIntegrasi']);
Route::post('/keuangan/hasilpendapatandokterintegrasi',['as'=>'HasilPendapatanDokterIntegrasi','uses'=>'Keuangan\KeuanganController@HasilPendapatanDokterIntegrasi']);
Route::get('/keuangan/cetakpendapatandokterintegrasi/{kode_dokter}/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakPendapatanDokterIntegrasi','uses'=>'Keuangan\KeuanganController@CetakPendapatanDokterIntegrasi']);
Route::get('/keuangan/pendapatanperawat',['as'=>'PendapatanPerawat','uses'=>'Keuangan\KeuanganController@PendapatanPerawat']);
Route::post('/keuangan/hasilpendapatanperawat',['as'=>'HasilPendapatanPerawat','uses'=>'Keuangan\KeuanganController@HasilPendapatanPerawat']);
Route::get('/keuangan/cetakpendapatanperawat/{kode_perawat}/{tanggal_dari}/{tanggal_sampai}',['as'=>'CetakPendapatanPerawat','uses'=>'Keuangan\KeuanganController@CetakPendapatanPerawat']);
Route::get('/keuangan/uraiangajidokter',['as'=>'UraianGajiDokter','uses'=>'Keuangan\KeuanganController@UraianGajiDokter']);
Route::post('/keuangan/hasilgajidokter',['as'=>'HasilGajiDokter','uses'=>'Keuangan\KeuanganController@HasilGajiDokter']);
Route::get('/keuangan/hasiluangduduk/{tanggal}/{kode_dokter}/{shift}',['as'=>'HasilUangDuduk','uses'=>'Keuangan\KeuanganController@HasilUangDuduk']);


//Rekam Medis
Route::get('/rekammedis',['as'=>'RekamDataPasien','uses'=>'RekamMedis\RekamMedisController@RekamDataPasien']);
Route::get('/rekammedisintegrasi',['as'=>'RekamDataPasienIntegrasi','uses'=>'RekamMedis\RekamMedisController@RekamDataPasienIntegrasi']);
Route::post('/rekammedis/tanggal',['as'=>'RekamDataPasienTanggal','uses'=>'RekamMedis\RekamMedisController@RekamDataPasienTanggal']);
Route::post('/rekammedisintegrasi/tanggal',['as'=>'RekamDataPasienTanggalIntegrasi','uses'=>'RekamMedis\RekamMedisController@RekamDataPasienTanggalIntegrasi']);
Route::get('/rekammedis/export/{dari}/{sampai}',['as'=>'ExportRekamDataPasien','uses'=>'RekamMedis\RekamMedisController@ExportRekamDataPasien']);
Route::get('/rekammedisintegrasi/export/{dari}/{sampai}',['as'=>'ExportRekamDataPasienIntegrasi','uses'=>'RekamMedis\RekamMedisController@ExportRekamDataPasienIntegrasi']);
Route::get('/rekammedis/datarm',['as'=>'DaftarPasienRM','uses'=>'RekamMedis\RekamMedisController@DaftarPasienRM']);
Route::get('/rekammedis/detail/{id}',['as'=>'DetailPasienRM','uses'=>'RekamMedis\RekamMedisController@DetailPasienRM']);
Route::get('/rekammedis/cetak/{id}',['as'=>'CetakPasienRM','uses'=>'RekamMedis\RekamMedisController@CetakPasienRM']);
Route::get('/rekammedis/riwayatperiksa/{rm?}',['as'=>'RekamMedisRiwayatPeriksa','uses'=>'RekamMedis\RekamMedisController@RiwayatPeriksa']);
Route::get('/rekammedis/riwayatperiksa/{rm}/{id}',['as'=>'RekamMedisDetilRiwayatPeriksa','uses'=>'RekamMedis\RekamMedisController@DetilRiwayatPeriksa']);
Route::get('/rekammedis/cetakriwayatperiksa/{rm}/{id}',['as'=>'CetakRiwayatPeriksa','uses'=>'RekamMedis\RekamMedisController@CetakRiwayatPeriksa']);

//Dokumen
Route::get('/dokter/dokumen',['as'=>'DaftarDokumen','uses'=>'Dokter\DokterDokumen@DaftarDokumen']);
Route::get('/dokter/dokumen/tambah',['as'=>'TambahDokumen','uses'=>'Dokter\DokterDokumen@TambahDokumen']);
Route::post('/dokter/dokumen/simpan',['as'=>'SimpanDokumen','uses'=>'Dokter\DokterDokumen@TambahkanDokumen']);
Route::get('/dokter/dokumen/hapus/{id}',['as'=>'HapusDokumen','uses'=>'Dokter\DokterDokumen@HapusDokumen']);

//Dokumen Karyawan
Route::get('/karyawan/dokumenkaryawan/{id}',['as'=>'DaftarDokumenKaryawan','uses'=>'Karyawan\KaryawanDokumen@DaftarDokumenKaryawan']);
Route::get('/karyawan/dokumendokter/{id}',['as'=>'DaftarDokumenDokter','uses'=>'Karyawan\KaryawanDokumen@DaftarDokumenDokter']);
Route::get('/karyawan/dokumen/tambah/{id}',['as'=>'TambahDokumenKaryawan','uses'=>'Karyawan\KaryawanDokumen@TambahDokumen']);
Route::post('/karyawan/dokumen/baru/simpan',['as'=>'SimpanDokumenBaruKaryawan','uses'=>'Karyawan\KaryawanDokumen@TambahkanDokumen']);
Route::get('/karyawan/dokumen/hapus/{id}',['as'=>'HapusDokumenKaryawan','uses'=>'Karyawan\KaryawanDokumen@HapusDokumen']);

//Dokumen
Route::get('/dokter/sptm/daftar',['as'=>'DaftarSPTM','uses'=>'Dokter\DokumenSPTM@DaftarDokumen']);
Route::get('/dokter/sptm/formulir/{id}',['as'=>'FormulirSPTM','uses'=>'Dokter\DokumenSPTM@FormulirDokumen']);
Route::post('/dokter/sptm/simpan',['as'=>'SimpanSPTM','uses'=>'Dokter\DokumenSPTM@TambahkanDokumen']);
Route::get('/dokter/sptm/dokumen/{id}',['as'=>'LihatSPTM','uses'=>'Dokter\DokumenSPTM@LihatDokumen']);

Route::get('/admin/sptm/daftar',['as'=>'AdminListSPTM','uses'=>'SPTM\ManajemenSPTM@DaftarDokumen']);
Route::get('/admin/sptm/dokumen/{id}',['as'=>'AdminViewSPTM','uses'=>'SPTM\ManajemenSPTM@LihatDokumen']);
Route::get('/admin/sptm/hapus/{id}',['as'=>'AdminHapusSPTM','uses'=>'SPTM\ManajemenSPTM@HapusDokumen']);

Route::middleware(['cors'])->group(function () {
    Route::post('/pasien/baru', 'Front\PasienController@PasienBaru');
});


Route::get('/pasien/baru',['as'=>'PasienBaru','uses'=>'Front\PasienController@PasienBaru']);