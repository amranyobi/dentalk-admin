<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use DB;

/**
 *
 */
class UserLevel
{
	public static function AmbilBagian()
	{
		if (Auth::Check()) {
			$ambilbagian = DB::table('user')
			->where('username',\Auth::user()->username);
			if ($ambilbagian->count()=='0') {
				return null;
			}
			else {
				return $ambilbagian->get()->first();
			}
		}
		else {
			return null;
		}
	}

	public static function AmbilNama()
	{
		if (Auth::user()->level==1) {
			$ambiladmin = DB::table('user')
			->where('username',\Auth::user()->username)
			->first();

			return $ambiladmin->username;
		}
		else if (Auth::user()->level==2) {
			$ambilperawat = DB::table('fk_penguji')
			->join('user','fk_penguji.id','=','user.username')
			->select('fk_penguji.nama','fk_penguji.gelar')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilperawat->nama;
		}
		else if (Auth::user()->level==3) {
			$ambildokter = DB::table('master_dokter')
			->leftjoin('user','master_dokter.kode_dokter','=','user.username')
			->select('master_dokter.nama')
			->where('username',\Auth::user()->username)
			->first();
			//dd($ambildokter);
			return $ambildokter->nama;
		}
		else if (Auth::user()->level==4) {
			$ambilapoteker = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilapoteker->nama;
		}
		else if (Auth::user()->level==5) {
			$ambilfront = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilfront->nama;
		}
		else if (Auth::user()->level==6) {
			$ambilkasir = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilkasir->nama;
		}
		else if (Auth::user()->level==7) {
			$ambillab = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambillab->nama;
		}
		else if (Auth::user()->level==8) {
			$ambildokter = DB::table('master_dokter')
			->leftjoin('user','master_dokter.kode_dokter','=','user.username')
			->select('master_dokter.nama')
			->where('username',\Auth::user()->username)
			->first();
			//dd($ambildokter);
			return $ambildokter->nama;
		}
		else if (Auth::user()->level==9) {
			$ambilkeuangan = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilkeuangan->nama;
		}
		else if (Auth::user()->level==10) {
			$ambillabgigi = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambillabgigi->nama;
		}
		else if (Auth::user()->level==11) {
			$ambilfoklinik = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilfoklinik->nama;
		}
		else if (Auth::user()->level==12) {
			$ambildokter = DB::table('msmhs')
			->leftjoin('user','msmhs.nim','=','user.username')
			->select('msmhs.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambildokter->nama;
		}
		else if (Auth::user()->level==13) {
			$ambilperawatintegrasi = DB::table('master_karyawan')
			->join('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilperawatintegrasi->nama;
		}
		else if (Auth::user()->level==14) {
			$ambilkasirintegrasi = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilkasirintegrasi->nama;
		}

		else if (Auth::user()->level==15) {
			$ambilkasirintegrasi = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilkasirintegrasi->nama;
		}


		else if (Auth::user()->level==16) {
			$ambilkasirintegrasi = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilkasirintegrasi->nama;
		}

		else if (Auth::user()->level==17) {
			$ambilkasirintegrasi = DB::table('master_karyawan')
			->leftjoin('user','master_karyawan.kode_karyawan','=','user.username')
			->select('master_karyawan.nama')
			->where('username',\Auth::user()->username)
			->first();

			return $ambilkasirintegrasi->nama;
		}
	}
}
