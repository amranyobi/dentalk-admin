<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Session;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
	public function __construct(){
		$this->middleware(['auth','level:1']);
	}
	
	public function DaftarUser(){

		$mhs = DB::table('user')
		->select('user.id', 'user.username', 'jenis_user.keterangan','msmhs.nama as nama')
		->join('jenis_user','user.level', 'jenis_user.id')
		->join('msmhs', 'msmhs.nim', 'user.username');

		$karyawan = DB::table('user')
		->select('user.id', 'user.username', 'jenis_user.keterangan','master_karyawan.nama as nama')
		->join('jenis_user','user.level', 'jenis_user.id')
		->join('master_karyawan', 'master_karyawan.kode_karyawan', 'user.username');
		
		$user = DB::table('user')
		->select('user.id', 'user.username', 'jenis_user.keterangan','master_dokter.nama as nama')
		->join('jenis_user','user.level', 'jenis_user.id')
		->join('master_dokter', 'master_dokter.kode_dokter', 'user.username')
		->unionAll($karyawan)
		->unionAll($mhs)
		->get();
      	$no = 1;
		
		//dd($user);
		
		return view('User.DaftarUser',compact('user','no'));
	}
	
	public function TambahUser(){
		$level = DB::table('jenis_user')
		->get();

		$karyawan = DB::table('master_karyawan')
		->select('master_karyawan.kode_karyawan as kode', 'master_karyawan.nama','master_karyawan.gelar');

		$mhs = DB::table('msmhs')
		->select('msmhs.nim as kode', 'msmhs.nama','msmhs.kdprodi');

		$dokter = DB::table('master_dokter')
		->select('master_dokter.kode_dokter as kode','master_dokter.nama','master_dokter.gelar')
		->unionAll($karyawan)
		->unionAll($mhs)
		->get();

		
		return view('User.FormTambahUser',compact('level','dokter'));
	}
	
	public function SimpanTambahUser(request $data){
		//Validasi inputan
		$validasi = Validator::make($data->all(),[
			'username'=>'required|max:15',
			'password'=>'required',
		],[
			'username.required'=>'Username harus diisi.',
			'username.max'=>'Username tidak boleh lebih dari 15 karakter.',
			'password.required'=>'Password harus diisi.',
		]);
		
		/* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
		if($validasi->fails()){
			return redirect(route('TambahUser'))
        	->withErrors($validasi)
        	->withInput();
		}

		$cariuser = DB::table('user')
		->where('username',$data->username);
		
		if($cariuser->count()>0)
	    {
	       	Session::flash('gagal','User Login sudah ada.');
				return redirect(route('DaftarUser'));
	    }
      	
		
		DB::BeginTransaction();
		try{
			/* Insert ke tabel master_User, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
			$username  = $data->username;
			$password_enkrip = Hash::make($data->password);
			$level = $data->level;
			DB::table('user')->insert(
			['username' => $username, 'password' => $password_enkrip, 'level' => $level]
			);
			DB::Commit();
			/* Bikin Alert Sukses */
			Session::flash('sukses','Tambah User Sukses');
			return redirect(route('DaftarUser'));
      	}
      	catch(\Exception $e)
      	{
			/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
			DB::rollBack();
			/*nulis exception di log*/
			dd($e);
			//report($e);
			/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
			Session::flash('gagal','Tambah User Gagal, Silahkan Hubungi TIK');
			return redirect(route('TambahUser'))->withInput();
      	}
	}
	
	public function HapusUser(request $data){
		DB::table('user')
		->where('id',$data->id)->delete();
		Session::flash('sukses','Data User berhasil dihapus.');
		return redirect(route('DaftarUser'));
	}
	
	public function UbahUser($id){
		$level = DB::table('jenis_user')
		->get();
		
		$cariUser = DB::table('user')
		->where('id',$id);
		
		if($cariUser->count()==0)
      	{
        	return redirect(route('DaftarUser'));
      	}
		$getuser=$cariUser->first();
		return view('User.FormUbahUser',compact('getuser', 'level'));
	}
	
	public function SimpanUbahUser(request $data){
		//Validasi inputan
		$validasi = Validator::make($data->all(),[
			'username'=>'required',
			'username'=>'required|max:15',
		],[
			'username.required'=>'Username harus diisi.',
			'username.max'=>'Nama tidak boleh lebih dari 15 karakter.',
		]);
		
		/* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
		if($validasi->fails()){
			return redirect(route('UbahUser',['id'=>$data->id]))
        	->withErrors($validasi)
        	->withInput();
		}
		
		DB::BeginTransaction();
		try{
			/* Insert ke tabel master_User, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
			$username  = $data->username;
			$password_enkrip = Hash::make($data->password);
			$level = $data->level;
			if(!empty($data->password)){
			DB::table('user')->where('id',$data->id)
			->update(
			['username' => $data->username, 'password' => $password_enkrip, 'level' => $level]
			);
			}else{
			DB::table('user')->where('id',$data->id)
			->update(
			['username' => $data->username, 'level' => $level]
			);
			}
			DB::Commit();
			/* Bikin Alert Sukses */
			Session::flash('sukses','Ubah Data User Sukses');
			return redirect(route('DaftarUser'));
      	}
      	catch(\Exception $e)
      	{
			/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
			DB::rollBack();
			/*nulis exception di log*/
			dd($e);
			//report($e);
			/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
			Session::flash('gagal','Ubah Data User Gagal, Silahkan Hubungi TIK');
			return redirect(route('UbahUser'))->withInput();
      	}
	}
}
