/*
Navicat MySQL Data Transfer

Source Server         : Server RSGM
Source Server Version : 50560
Source Host           : localhost:3306
Source Database       : simrsgm

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-02-09 07:24:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `detil_odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `detil_odontogram`;
CREATE TABLE `detil_odontogram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_odontogram` int(11) NOT NULL,
  `id_gigi` int(11) NOT NULL,
  `id_daftar_odontogram` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of detil_odontogram
-- ----------------------------
