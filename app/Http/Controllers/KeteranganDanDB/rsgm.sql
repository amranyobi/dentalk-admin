/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-21 13:26:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `antrian_apotek`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_apotek`;
CREATE TABLE `antrian_apotek` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_apotek
-- ----------------------------
INSERT INTO `antrian_apotek` VALUES ('4', '1', '2018-12-19', '1', '2018-12-19 23:42:12', '0');
INSERT INTO `antrian_apotek` VALUES ('5', '4', '2019-01-04', '1', '2019-01-04 07:00:06', '0');
INSERT INTO `antrian_apotek` VALUES ('6', '5', '2019-01-04', '2', '2019-01-04 07:35:45', '2');

-- ----------------------------
-- Table structure for `antrian_kasir`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_kasir`;
CREATE TABLE `antrian_kasir` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_kasir
-- ----------------------------
INSERT INTO `antrian_kasir` VALUES ('1', '4', '2019-01-04', '1', '2019-01-04 06:58:27', '0');
INSERT INTO `antrian_kasir` VALUES ('2', '5', '2019-01-04', '2', '2019-01-04 07:34:52', '0');

-- ----------------------------
-- Table structure for `antrian_lab`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_lab`;
CREATE TABLE `antrian_lab` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_lab
-- ----------------------------
INSERT INTO `antrian_lab` VALUES ('1', '3', '2019-01-03', '1', '2019-01-03 11:31:34', '0');

-- ----------------------------
-- Table structure for `antrian_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_lab_gigi`;
CREATE TABLE `antrian_lab_gigi` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_lab_gigi
-- ----------------------------
INSERT INTO `antrian_lab_gigi` VALUES ('1', '5', '2019-01-03', '1', '2019-01-03 11:31:34', '1');

-- ----------------------------
-- Table structure for `antrian_radiografi`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_radiografi`;
CREATE TABLE `antrian_radiografi` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_radiografi
-- ----------------------------

-- ----------------------------
-- Table structure for `datamedis_umum`
-- ----------------------------
DROP TABLE IF EXISTS `datamedis_umum`;
CREATE TABLE `datamedis_umum` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rekam_medis` varchar(15) NOT NULL,
  `tekanan_darah` int(1) NOT NULL,
  `penyakit_jantung` int(1) NOT NULL,
  `diabetes_melitus` int(1) NOT NULL,
  `hemofilia` int(1) NOT NULL,
  `riwayat_asma` int(1) NOT NULL,
  `hepatitis` int(1) NOT NULL,
  `epilepsy` int(1) NOT NULL,
  `gastritis` int(1) NOT NULL,
  `asma` int(1) NOT NULL,
  `tbc` int(1) NOT NULL,
  `penyakit_lain` int(1) NOT NULL,
  `obatan` int(1) NOT NULL,
  `alergi_makanan` int(1) NOT NULL,
  PRIMARY KEY (`id`,`rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of datamedis_umum
-- ----------------------------

-- ----------------------------
-- Table structure for `data_pasien`
-- ----------------------------
DROP TABLE IF EXISTS `data_pasien`;
CREATE TABLE `data_pasien` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_jadwal` int(10) NOT NULL,
  `rekam_medis` varchar(15) NOT NULL,
  `no_antri` int(3) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_pasien
-- ----------------------------
INSERT INTO `data_pasien` VALUES ('1', '1', '000002', '1', '4');
INSERT INTO `data_pasien` VALUES ('2', '3', '000001', '1', '2');
INSERT INTO `data_pasien` VALUES ('3', '4', '000002', '1', '2');
INSERT INTO `data_pasien` VALUES ('4', '5', '000004', '1', '4');
INSERT INTO `data_pasien` VALUES ('5', '5', '000002', '2', '4');

-- ----------------------------
-- Table structure for `detail_beli`
-- ----------------------------
DROP TABLE IF EXISTS `detail_beli`;
CREATE TABLE `detail_beli` (
  `id` int(15) NOT NULL,
  `id_beli` varchar(25) NOT NULL,
  `faktur_beli` varchar(25) NOT NULL,
  `kode_obat` varchar(10) NOT NULL,
  `harga_beli` int(15) NOT NULL,
  `qty` int(10) NOT NULL,
  `total` int(25) NOT NULL,
  `expired` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of detail_beli
-- ----------------------------

-- ----------------------------
-- Table structure for `diagnosa_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `diagnosa_pemeriksaan`;
CREATE TABLE `diagnosa_pemeriksaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `id_diagnosa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of diagnosa_pemeriksaan
-- ----------------------------
INSERT INTO `diagnosa_pemeriksaan` VALUES ('1', '4', '1');
INSERT INTO `diagnosa_pemeriksaan` VALUES ('2', '4', '2');
INSERT INTO `diagnosa_pemeriksaan` VALUES ('3', '5', '1');
INSERT INTO `diagnosa_pemeriksaan` VALUES ('4', '5', '2');

-- ----------------------------
-- Table structure for `hasil_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `hasil_pemeriksaan`;
CREATE TABLE `hasil_pemeriksaan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `subjek` text NOT NULL,
  `objek` text NOT NULL,
  `assessment` text NOT NULL,
  `planning` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_pemeriksaan
-- ----------------------------
INSERT INTO `hasil_pemeriksaan` VALUES ('1', '1', 'Gusi Berdarah', 'Radang Gigi', '', '');
INSERT INTO `hasil_pemeriksaan` VALUES ('2', '4', 'ini subjective', 'ini objective', 'ini assessment', 'ini planning');
INSERT INTO `hasil_pemeriksaan` VALUES ('3', '5', 'ini subjective', 'ini objective', 'ini assesment', 'ini planning');

-- ----------------------------
-- Table structure for `jadwal_harian`
-- ----------------------------
DROP TABLE IF EXISTS `jadwal_harian`;
CREATE TABLE `jadwal_harian` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode_jadwal` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `validasi` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jadwal_harian
-- ----------------------------
INSERT INTO `jadwal_harian` VALUES ('1', '1', '2018-12-19', '1');
INSERT INTO `jadwal_harian` VALUES ('3', '1', '2018-12-24', '0');
INSERT INTO `jadwal_harian` VALUES ('4', '3', '2019-01-03', '0');
INSERT INTO `jadwal_harian` VALUES ('5', '4', '2019-01-04', '0');

-- ----------------------------
-- Table structure for `jenis_user`
-- ----------------------------
DROP TABLE IF EXISTS `jenis_user`;
CREATE TABLE `jenis_user` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jenis_user
-- ----------------------------
INSERT INTO `jenis_user` VALUES ('1', 'Administration');
INSERT INTO `jenis_user` VALUES ('2', 'Nurse');
INSERT INTO `jenis_user` VALUES ('3', 'Dokter');
INSERT INTO `jenis_user` VALUES ('4', 'Apoteker');
INSERT INTO `jenis_user` VALUES ('5', 'Front Office');
INSERT INTO `jenis_user` VALUES ('6', 'Kasir');

-- ----------------------------
-- Table structure for `konfirmasi_daftar`
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_daftar`;
CREATE TABLE `konfirmasi_daftar` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `email` varchar(25) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_daftar
-- ----------------------------

-- ----------------------------
-- Table structure for `master_diagnosa`
-- ----------------------------
DROP TABLE IF EXISTS `master_diagnosa`;
CREATE TABLE `master_diagnosa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `poli` varchar(10) NOT NULL,
  `diagnosa` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_diagnosa
-- ----------------------------
INSERT INTO `master_diagnosa` VALUES ('1', 'P003', 'Gigi Caries');
INSERT INTO `master_diagnosa` VALUES ('2', 'P003', 'Gigi Tambal');

-- ----------------------------
-- Table structure for `master_dokter`
-- ----------------------------
DROP TABLE IF EXISTS `master_dokter`;
CREATE TABLE `master_dokter` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode_dokter` varchar(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `gelar` varchar(15) NOT NULL,
  `spesialisasi` int(1) NOT NULL,
  `alamat` varchar(25) NOT NULL,
  `hp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`kode_dokter`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_dokter
-- ----------------------------
INSERT INTO `master_dokter` VALUES ('2', 'K001', 'Akhmad Dahlan', 'SpOg', '9', 'Semarang Tengah', '08564631232');
INSERT INTO `master_dokter` VALUES ('3', 'D433', 'Muhammad Thanos', 'SpJ', '9', 'Jl. Soekarno Hatta, Titan', '08546313433');
INSERT INTO `master_dokter` VALUES ('4', 'D445', 'Dr. Stranger', 'dr.', '9', 'Korea', '085645131321');

-- ----------------------------
-- Table structure for `master_jadwal`
-- ----------------------------
DROP TABLE IF EXISTS `master_jadwal`;
CREATE TABLE `master_jadwal` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode_dokter` varchar(15) NOT NULL,
  `kode_poli` varchar(15) NOT NULL,
  `hari` int(1) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jadwal
-- ----------------------------
INSERT INTO `master_jadwal` VALUES ('2', 'K001', 'P002', '2', '07:00:00', '08:00:00', '-');
INSERT INTO `master_jadwal` VALUES ('3', 'D433', 'P002', '4', '07:00:00', '08:00:00', 'Ini Keterangan');
INSERT INTO `master_jadwal` VALUES ('4', 'D433', 'P003', '5', '07:00:00', '08:00:00', '-');

-- ----------------------------
-- Table structure for `master_jasa`
-- ----------------------------
DROP TABLE IF EXISTS `master_jasa`;
CREATE TABLE `master_jasa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode_dokter` varchar(15) NOT NULL,
  `biaya_jasa` int(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jasa
-- ----------------------------

-- ----------------------------
-- Table structure for `master_jenis_obat`
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_obat`;
CREATE TABLE `master_jenis_obat` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `jenis_obat` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jenis_obat
-- ----------------------------
INSERT INTO `master_jenis_obat` VALUES ('1', 'Obat Bebas');
INSERT INTO `master_jenis_obat` VALUES ('2', 'Obat Keras');

-- ----------------------------
-- Table structure for `master_karyawan`
-- ----------------------------
DROP TABLE IF EXISTS `master_karyawan`;
CREATE TABLE `master_karyawan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode_karyawan` varchar(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `gelar` varchar(15) NOT NULL,
  `unit` int(2) NOT NULL,
  `alamat` varchar(25) NOT NULL,
  `hp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`kode_karyawan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_karyawan
-- ----------------------------
INSERT INTO `master_karyawan` VALUES ('1', '001', 'Amran Yobioktabera', 'M.Kom', '2', 'Perum Dolog', '085643213210');
INSERT INTO `master_karyawan` VALUES ('2', '002', 'Daniar', 'S.Lab', '1', 'Tlogosari Wetan', '08556431321');
INSERT INTO `master_karyawan` VALUES ('3', '9888', 'Dwi Windu', 'drg', '1', 'Jl. Parangkusumo Indah', '058649643133');
INSERT INTO `master_karyawan` VALUES ('4', '7799', 'Susilowati', 'S.Kom', '2', 'Jl. Beringin Raya No.34', '08646313213');

-- ----------------------------
-- Table structure for `master_obat`
-- ----------------------------
DROP TABLE IF EXISTS `master_obat`;
CREATE TABLE `master_obat` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode_obat` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `dosis` int(5) NOT NULL,
  `satuan` int(2) NOT NULL,
  `jenis` int(3) NOT NULL,
  `resep_dokter` int(1) NOT NULL,
  `keterangan` text NOT NULL,
  `stok` int(15) NOT NULL DEFAULT '0',
  `harga_jual` int(15) NOT NULL,
  PRIMARY KEY (`id`,`kode_obat`),
  UNIQUE KEY `kode_obat` (`kode_obat`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_obat
-- ----------------------------
INSERT INTO `master_obat` VALUES ('4', '7788', 'Ibuprofen', '15', '1', '1', '1', 'Obat sakit gigi berlubang', '0', '5000');
INSERT INTO `master_obat` VALUES ('5', '46836', 'Naproxen', '100', '2', '2', '0', 'Penghilang rasa sakit yang juga sering digunakan untuk mengatasi sakit gigi', '0', '6000');
INSERT INTO `master_obat` VALUES ('6', '896634', 'Acetaminophen', '100', '2', '1', '1', 'Meredakan rasa sakit di gigi', '0', '80000');

-- ----------------------------
-- Table structure for `master_pekerjaan`
-- ----------------------------
DROP TABLE IF EXISTS `master_pekerjaan`;
CREATE TABLE `master_pekerjaan` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_pekerjaan
-- ----------------------------
INSERT INTO `master_pekerjaan` VALUES ('1', 'Tidak Bekerja');
INSERT INTO `master_pekerjaan` VALUES ('2', 'PNS');
INSERT INTO `master_pekerjaan` VALUES ('3', 'TNI / Polri');
INSERT INTO `master_pekerjaan` VALUES ('4', 'Legislatif');
INSERT INTO `master_pekerjaan` VALUES ('5', 'BUMN');
INSERT INTO `master_pekerjaan` VALUES ('6', 'Swasta');
INSERT INTO `master_pekerjaan` VALUES ('7', 'Wiraswasta');
INSERT INTO `master_pekerjaan` VALUES ('8', 'Pensiunan');
INSERT INTO `master_pekerjaan` VALUES ('9', 'Lainnya');

-- ----------------------------
-- Table structure for `master_poli`
-- ----------------------------
DROP TABLE IF EXISTS `master_poli`;
CREATE TABLE `master_poli` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode_poli` varchar(15) NOT NULL,
  `nama_poli` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`,`kode_poli`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_poli
-- ----------------------------
INSERT INTO `master_poli` VALUES ('1', 'P001', 'Penyakit Mulut dan Periodontik', 'Poliklinik tentang Penyakit Mulut dan Periodontik');
INSERT INTO `master_poli` VALUES ('2', 'P002', 'Perawatan Gigi dan Mulut', 'Perawatan Gigi dan Mulut');
INSERT INTO `master_poli` VALUES ('3', 'P003', 'Pemasangan Gigi Baru', 'Pemasangan Gigi Baru');

-- ----------------------------
-- Table structure for `master_satuan`
-- ----------------------------
DROP TABLE IF EXISTS `master_satuan`;
CREATE TABLE `master_satuan` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `singkatan` varchar(25) NOT NULL,
  `satuan` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_satuan
-- ----------------------------
INSERT INTO `master_satuan` VALUES ('1', 'ml', 'miligram');
INSERT INTO `master_satuan` VALUES ('2', 'box', 'box');

-- ----------------------------
-- Table structure for `master_spesialis`
-- ----------------------------
DROP TABLE IF EXISTS `master_spesialis`;
CREATE TABLE `master_spesialis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gelar` varchar(100) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_spesialis
-- ----------------------------
INSERT INTO `master_spesialis` VALUES ('9', 'Sp.AA', 'Spesialis Anak Anak', '-');
INSERT INTO `master_spesialis` VALUES ('10', 'Sp.G', 'Spesialis Gusi', 'Gusi anak');

-- ----------------------------
-- Table structure for `master_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `master_supplier`;
CREATE TABLE `master_supplier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_supplier
-- ----------------------------
INSERT INTO `master_supplier` VALUES ('1', 'CV. Karya Kita', 'Pedurungan', '085669099980');

-- ----------------------------
-- Table structure for `master_tindakan`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan`;
CREATE TABLE `master_tindakan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tindakan` text NOT NULL,
  `poli` varchar(255) NOT NULL,
  `biaya_tindakan` int(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan
-- ----------------------------
INSERT INTO `master_tindakan` VALUES ('14', 'Perawatan Gigi Berlubang', 'P001', '800000');
INSERT INTO `master_tindakan` VALUES ('15', 'Perawatan Gigi Hitam', 'P002', '800000');

-- ----------------------------
-- Table structure for `master_tindakan_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan_lab_gigi`;
CREATE TABLE `master_tindakan_lab_gigi` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `tipe` int(2) NOT NULL COMMENT '1:prostho;2:ortho;3:sampel penelitian;4:konservasi',
  `tindakan` varchar(50) NOT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan_lab_gigi
-- ----------------------------
INSERT INTO `master_tindakan_lab_gigi` VALUES ('1', '1', 'GTS', '15000');
INSERT INTO `master_tindakan_lab_gigi` VALUES ('2', '1', 'GTC', '20000');
INSERT INTO `master_tindakan_lab_gigi` VALUES ('3', '2', 'Sampel Penelitian', '30000');

-- ----------------------------
-- Table structure for `master_tindakan_penunjang`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan_penunjang`;
CREATE TABLE `master_tindakan_penunjang` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `tindakan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `biaya` int(11) NOT NULL,
  `tipe` int(1) NOT NULL COMMENT '1:laboratorium;2:rontgen',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan_penunjang
-- ----------------------------

-- ----------------------------
-- Table structure for `master_tindakan_radiografi`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan_radiografi`;
CREATE TABLE `master_tindakan_radiografi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `tindakan` text NOT NULL,
  `biaya` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan_radiografi
-- ----------------------------

-- ----------------------------
-- Table structure for `master_unit`
-- ----------------------------
DROP TABLE IF EXISTS `master_unit`;
CREATE TABLE `master_unit` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nama_unit` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of master_unit
-- ----------------------------
INSERT INTO `master_unit` VALUES ('1', 'Unit Penunjang Laboratorium');
INSERT INTO `master_unit` VALUES ('2', 'Unit Penunjang Radiografi');

-- ----------------------------
-- Table structure for `msmhs`
-- ----------------------------
DROP TABLE IF EXISTS `msmhs`;
CREATE TABLE `msmhs` (
  `kdpt` varchar(6) NOT NULL,
  `jen` varchar(1) NOT NULL,
  `kdprodi` varchar(5) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kls` varchar(1) NOT NULL,
  `tplhr` varchar(35) NOT NULL,
  `tglhr` date NOT NULL,
  `lp` varchar(1) NOT NULL,
  `thmasuk` varchar(5) NOT NULL,
  `smtawal` varchar(5) NOT NULL,
  `btstudi` varchar(5) NOT NULL,
  `kdprop` varchar(2) NOT NULL,
  `tgmsk` date NOT NULL,
  `tglls` date NOT NULL,
  `status` varchar(1) NOT NULL,
  `bp` varchar(1) NOT NULL,
  `sksdiakui` varchar(4) NOT NULL,
  `nimasal` varchar(20) NOT NULL,
  `kdptasal1` varchar(6) NOT NULL,
  `jenasal` varchar(1) NOT NULL,
  `kdprodiasal` varchar(5) NOT NULL,
  `kd` varchar(5) NOT NULL,
  `nama_foto` varchar(50) NOT NULL,
  `agama` varchar(10) NOT NULL,
  `gol_dar` varchar(2) NOT NULL,
  `tb` varchar(5) NOT NULL,
  `sek_asal` varchar(35) NOT NULL,
  `jurusan` varchar(100) NOT NULL,
  `thijazah` varchar(5) NOT NULL,
  `nil_ijz` int(11) NOT NULL,
  `nil_uan` int(11) NOT NULL,
  `nama_ortu` varchar(100) NOT NULL,
  `pekerj` varchar(35) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kota` varchar(35) NOT NULL,
  `telp_rmh` varchar(30) NOT NULL,
  `hp` varchar(30) NOT NULL,
  `email` varchar(35) NOT NULL,
  `info_unimus` varchar(35) NOT NULL,
  `ipk` float(5,2) NOT NULL,
  `skslls` varchar(5) NOT NULL,
  `lama_studi` varchar(50) NOT NULL,
  `judul_ta` text NOT NULL,
  `alamatweb` varchar(50) NOT NULL,
  `ukjas` varchar(5) NOT NULL,
  `jpmb` varchar(35) NOT NULL,
  PRIMARY KEY (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of msmhs
-- ----------------------------
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013001', 'RA. Miranti Widya Arianti', 'R', 'Jakarta', '1993-08-04', 'P', '2013', '20131', '20202', '01', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130612041415.jpg', 'Islam', 'B', '170', 'SMA Theresiana 2 Semarang', 'IPA', '2012', '8', '8', 'drg. Sri Suparti', 'dokter gigi', 'Kelet RT 08 RW 02 Keling ', 'Jepara', '', '08112797429', 'mirawidya90@yahoo.com', 'Orangtua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013002', 'Endang Sri Rahayu', 'R', 'Rangan', '1995-05-25', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130613100458.jpg', 'Islam', 'O', '166', 'SMA N 1 Kuaro', 'IPA', '2013', '51', '34', 'Sadri', 'Swasta', 'Jl. Rangan Luar RT.4 Rw.1 Kuaro Paser Kalimantan Timur', '', '', '085346436315', 'ayuendangsri@gmail.com', 'Orangtua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013003', 'Khaleda Shafiratunnisa', 'R', 'Balikpapan', '1995-02-19', 'P', '2013', '20131', '20202', '16', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130613095233.jpg', 'Islam', 'A', '154', 'SMA Negeri 1 Balikpapan', 'IPA', '2013', '105', '49', 'H. Tuban Saparyono, SE', 'BUMN (BRI)', 'Jl. Markoni Atas RT.56 No.42 Balikpapan Kalimantan Timur', '', '0542-417064', '085753173842/0831533', 'khaledashafira@yahoo.com', 'internet', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013004', 'Ishana Raisa Hafid', 'R', 'Pontianak', '1995-04-08', 'P', '2013', '20131', '20202', '13', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20150106074229.jpg', 'Islam', 'B', '153', 'SMA N 1 Sambas', 'IPA', '2013', '50', '51', 'Ir. Daryanto, MT', 'PNS', 'Jl. Sambas RT.1 RW. 1 Lumbang Sambas Kalimantan Barat', '', '', '089665157027', 'IsHaNa_LoMLy@yahoo.com', 'Internet', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013005', 'Muizzudin Azza', 'R', 'Pati', '1995-12-02', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20150320031335.jpg', 'Islam', 'A', '171', 'SMA N 2 Pati', 'IPA', '2013', '0', '49', 'drg. Akhmad Muzenin', 'PNS', 'Jl. Amarta Raya No. 49 Perum Kutoharjo Pati', '', '0295-394210', '085713939871', 'azza.aizawa48@gmail.com', 'Radio & Internet', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013006', 'Sekar Lintang Hapsari', 'R', 'Kendal', '1995-11-15', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20150320031158.jpg', 'Islam', 'O', '170', 'SMA N 1 Kendal', 'IPA', '2013', '8', '8', 'Drs. Arief Suharsono, M.Pd', 'PNS', 'Karangdowo Rt.2 RW.1 Weleri Kendal', '', '0294-641104', '081326063330', 'hapsarisekarlintang@yahoo.com', 'Orangtua,Brosur,Internet', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013007', 'Gilang Ramadhan', 'R', 'Kaitetu', '1996-02-14', 'L', '2013', '20131', '20202', '21', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20150317035059.jpg', 'Islam', '', '163', 'SMA N 3 Waeapo', 'IPA', '2013', '9', '8', 'Marwoto', 'PNS', 'Waenota Waeapo Buru Maluku', '', '', '0878470344191', '', 'Orangtua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013008', 'Bagus Aji Rahmawan', 'R', 'Kendal', '1995-09-04', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20140812014628.jpg', 'Islam', 'B', '189.5', 'SMAN 1 Cepiring', 'IPA', '2013', '8', '7', 'Suliyem', 'PNS (Guru)', 'Ds. Gebang Rt. 2/8 Gemuh ', 'Kendal', '', '083838905833', 'bagusaji.6262@yahoo.com', 'Radio', '0.00', '', '', '', '', 'XXL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013009', 'Tri Utari Sari Dewi', 'R', 'Pangkalanbun', '1995-07-13', 'P', '2013', '20131', '20202', '14', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130613092402.jpg', 'Islam', 'O', '151', 'SMA N 3 Pangkalanbun', 'IPA', '2013', '53', '38', 'Giarso Wandoyo', 'BUMN', 'Jl. Padat Karya No. 101 RT 03 Sidorejo Waringin Barat', 'Pangkalan Bun ', '0532-21759', '085752650486', 'triutarisaridewi@yahoo.com', 'Saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013010', 'Kurnia Adhi Wikanto', 'R', 'Semarang', '1995-09-03', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20150317035118.jpg', 'Islam', 'O', '170', 'SMA N 6 Semarang', 'IPA', '2013', '50', '47', 'Drs. Suhardi, MM', 'PNS', 'Jl. Kedungpane No.140 Rt.3 Rw.5 Mijen Semarang', '', '024-76631153', '085727588770', 'kurniadhi w@yahoo.com', 'Brosur', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013011', 'Maulida Dara Harjanti', 'R', 'Jepara', '1996-08-10', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130617013951.jpg', 'Islam', 'O', '148', 'SMAN 1 Batang', 'IPA', '2013', '9', '8', 'Bambang Jatmiko', 'Guru', 'Jl. Bima 1 B1/31 Perum Korpri Pasekaran Batang', '', '0285-4494174', '085701174917', 'maulidara_lautner@yahoo.co.id', 'Orang Tua', '0.00', '', '', '', '-', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013012', 'Hilma Falhil Mala', 'R', 'Jepara', '1993-11-14', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20140812014603.jpg', 'Islam', 'B', '157', 'SMAN 1 Pecangan', 'IPA', '2012', '9', '9', 'Drs. H. Ahmad Yazid, M.Pd.', 'Kepala Sekolah', 'Bandung Rejo RT.2 RW.2 Kalinyamatan Jepara', '', '0291-3319742', '089668000893', 'hilmafalhil@gmail.com', 'Orang Tua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013013', 'Hana Nabila Syifa', 'R', 'Semarang', '1995-05-20', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20150422050201.jpg', 'Islam', 'AB', '160', 'SMAN 15 Semarang', 'IPA', '2013', '0', '51', 'Yugo Nugroho', 'PNS', 'Jl Sapta Prasetya Raya No 29 ', 'Semarang', '0246700678', '08122816883', 'hananabilasyifa@yahoo.com', 'Orang Tua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013014', 'Steffi Mifta Fattarizqi', 'R', 'Semarang', '1994-06-27', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130618040644.jpg', 'Islam', 'AB', '163', 'SMAN 14 Semarang', 'IPA', '2012', '0', '9', 'Agung Sutarto', 'PNS (Dosen)', 'Jl MH Thamrin No 82', 'Semarang', '', '087731042538/0821366', 'steffymifta@yahoo.com', 'Orang Tua, Brosur', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013015', 'Dhony Miftahul Huda', 'R', 'Demak', '1990-02-02', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20140812014548.jpg', 'Islam', 'AB', '165', 'SMAN 3 Semarang', 'IPA', '2008', '48', '48', 'Suwarno, S. Pd, M. Pd', 'PNS', 'Kadilangu RT. 1 RW.5 No.15 Demak', '', '0291-682000', '085640705789', 'dhony_m_h@yahoo.com', 'internet', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013016', 'Zulfa Isma Lathifah', 'R', 'Tegal', '1995-07-18', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130618030646.jpg', 'Islam', 'B', '', 'MA NU Banat', 'IPA', '2013', '54', '50', 'Drs. H. Iskandar', 'Wiraswasta', 'Jl. Raya Bandasari N0.71 Rt.6/1 Tegal', '', '0283-354116', '085640224176', 'cutezulfa90@yahoo.com', 'saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013017', 'Fara Setyo Dewi', 'R', 'Jepara', '1995-03-01', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130618025236.jpg', 'Islam', 'B', '163', 'SMAN 1 Jepara', 'IPA', '2013', '0', '48', 'H. Setyanto SH,MM', 'PNS', 'Jl. Kusuma Utoyo No.69 RT.3/1 Kauman Jepara', '', '0291-591468', '089678976652', 'farafany14@gmail.com', 'orangtua,brosur,guru,saudara,teman', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013018', 'Asmara Dharma Febi Sasongko', 'R', 'Semarang', '1995-02-25', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130618024649.jpg', 'Islam', 'O', '169', 'SMAN 9 Semarang', 'IPA', '2013', '50', '50', 'Dharma Wahyu Edhy', 'PNS', 'Jl. Ungaran 4 No. 215F Candisari Semarang', '', '024-8448148', '085643588054', 'ASMARA_DHARMA@YAHOO.COM', 'INTERNET', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013019', 'Hanum Laksita Intan Hapsari', 'R', 'Sukoharjo', '1995-04-29', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130618020229.jpg', 'Islam', 'O', '160', 'SMAN 1 Sukoharjo', 'IPA', '2013', '0', '0', 'Dr Sri Lahir MPd', 'PNS', 'Menur Rt 3/5 Grogol', 'Sukoharjo', '0271-7880833', '085728869001', 'hanumlaksita@yahoo.com', 'Orang Tua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013020', 'Nanik Faiqotul Rohmah', 'R', 'Kendal', '1995-06-30', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '580', '20130618015330.jpg', 'Islam', 'AB', '152', 'SMAN 1 Boja', 'IPA', '2013', '0', '0', 'Tukipan', 'PNS', 'Kertasari RT.5 RW.12 Kendal', '', '-', '081326738517', '', 'orangtua,brosur', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013021', 'Prisca Anindia Setyowati', 'R', 'Semarang', '1995-04-24', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20130624040205.jpg', 'Islam', 'A', '168', 'SMAN 1 Semarang', 'IPA', '2013', '9', '8', 'Ahmad Tohir', 'Wiraswasta', 'Jl. WR. Supratman 11/12 Rt.2 Rw.12 Semarang', '', '024-70267909', '085641606286', 'prisca.anindia@ymail.com', 'teman', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013022', 'Nida Khairunnisa', 'R', 'Yogyakarta', '1994-12-03', 'P', '2013', '20131', '20202', '04', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20130624074851.jpg', 'Islam', 'AB', '161', 'SMA Muhammadiyah 1 Yogyakarta', 'IPA', '2013', '0', '0', 'Kristiono, S. SiT', 'BUMN', 'Jl. Kradenan No.43 RT.10 RW.69 Maguharjo Depok Sleman', 'Yogyakarta', '0274-4332433', '085726188876', 'haisafif@gmail.com', 'saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013023', 'Herlin  Ika Yuni Arista', 'R', 'Semarang', '1994-06-10', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20130626074831.jpg', 'Islam', 'O', '152', 'SMAN 1 Semarang', 'IPA', '2012', '9', '9', 'Kustiyono', 'PNS', 'Blumbang Krajan RT.7 RW.1 Bantengan Karanggede Boyolali', '', '0298-3420048', '085740871540', '-', 'orangtua', '0.00', '', '', '', '', 'XXL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013024', 'Kurniawan Cahya Saputra', 'R', 'Balikpapan', '1995-02-24', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014526.jpg', 'Islam', 'B', '165', 'SMA Nasima Semarang', 'IPA', '2013', '8', '8', 'Nurcahyo Widodo', 'Pensiunan Telkom', 'Tegal Sari Barat 4 No.3 Semarang', '', '024-8311863', '08562780080', '-', 'Brosur', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013025', 'Amil Furaihan', 'R', 'Jepara', '1994-06-14', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014509.jpg', 'Islam', 'A', '160', 'Darussalam Gontor', 'IPA', '2012', '0', '7', 'H.M Saifunnur B.A', 'Wiraswasta', 'Gemiring Kidul 4/4 Nalumsari Jepara', 'Jepara', '08122840341', '085741741789', 'afuraihan@yahoo.com', 'teman', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013026', 'Gita Jazaul Aufa', 'R', 'Batang', '1995-02-13', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014453.jpg', 'Islam', 'O', '174', 'SMAN 1 Subah', 'IPA', '2013', '8', '8', 'H. Muhtasin', 'Wiraswasta', 'Jl. Raya Sengon No.24 RT.5 RW.1 Batang', '', '-', '085729709924', '-', 'Teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013027', 'Syabilla Audina', 'R', 'Semarang', '1995-07-01', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '', 'Islam', '-', '175', 'SMAN 2 Serang', 'IPA', '2013', '0', '0', 'Suratno, S.H.', 'PNS', 'JL. Raya Pandeglang Pal 4 Komplek Kejati Serang', 'Banten', '-', '085710038626', '-', 'Saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013028', 'Agguna Wiladatika', 'R', 'Grobogan', '1995-05-14', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20150106074248.jpg', 'Islam', '-', '160', 'SMAN 1 Pulokulon', 'IPA', '2013', '53', '42', 'Sistoyo', 'PNS', 'Nambuhan RT.1 RW.5 Purwodadi Grobogan', '', '-', '081390332727', '-', 'Orang Tua', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013029', 'Farid Sadzali', 'R', 'Demak', '1994-10-27', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014308.jpg', 'Islam', '-', '164', 'SMAN 2 Demak', 'IPA', '2013', '8', '8', 'Sodikan', 'Wiraswasta', 'Jl. Blimbing RT.3 RW.2 Betokan Demak', '', '0291-6905273', '085727773334', '-', 'Saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013030', 'Nisa Soffiyani', 'R', 'Cirebon', '1995-06-04', 'P', '2013', '20131', '20202', '02', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014247.jpg', 'Islam', '', '161', 'SMAN 1 Brebes', 'IPA', '2013', '83', '89', 'E. Suryadi', 'PNS', 'Jl. H. Dul Ngalim Rt. 08/2 Ds. Panggansari Losari Cirebon', 'Cirebon Jawa Barat', '', '085797557048', 'nisasoffiyani@gmail.com', 'Brosur', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013031', 'Geeta Ariyanti', 'R', 'Sungailiat', '1995-09-08', 'P', '2013', '20131', '20202', '30', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014228.jpg', 'Islam', 'O', '160', 'SMA Setia Budi', 'IPA', '2013', '0', '0', 'Ariyanto', 'Swasta', 'Komp. Nangnung Tengah No.514 Sungailiat Bangka Belitung', '', '-', '081632324032', '-', 'Orangtua', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013032', 'Muhammad Iqbal Maal Abror', 'R', 'Jakarta', '1995-07-14', 'L', '2013', '20131', '20202', '01', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013950.jpg', 'Islam', 'O', '165', 'SMAN 75 Jakarta Utara', 'IPA', '2013', '8', '8', 'Imron Rosyadi', 'PNS', 'Jl. Pattimura RT07/03 No.30 Rawa Badau Selatan Hoja', 'Jakarta Utara', '021-4307061', '08988661473', 'mr_am99@yahoo.com', 'Orang Tua', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013033', 'Nahdlia Adibah Hariyono', 'R', 'Kendal', '1995-03-09', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812014731.jpg', 'Islam', 'B', '155', 'SMAN 1 Boja', 'IPA', '2013', '8', '48', 'Hariyono', 'Wiraswasta', 'Gonoharjo RT.2 RW.1 Limbangan Kendal', '', '-', '085740697899', '-', 'Saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013034', 'Mutia Ulfah', 'R', 'Bekasi', '1995-06-24', 'P', '2013', '20131', '20202', '02', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013731.jpg', 'Islam', 'O', '156', 'SMAN 1 Tambun Selatan', 'IPA', '2013', '0', '40', 'Sobirin', 'Wiraswasta', 'Kartika Wanasari Blok G1 No 6', 'Bekasi', '', '085218406162', 'anemutia@gmail.com', 'Brosur', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013035', 'Glory Gustiara Firdaus', 'R', 'Pati', '1994-02-22', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013715.jpg', 'Islam', 'A', '155', 'SMAN 1 Pati', 'IPA', '2012', '9', '9', 'Teguh Wiyono', 'PNS', 'Jl. Mangga 9 RT.2 RW.4 Perum Winong Pati', '', '0295-392201', '085641112678', 'glorygustiara@gmail.com', 'Teman', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013036', 'Salsabila Milatina Askiyah', 'R', 'Pemalang', '1995-03-03', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013659.jpg', 'Islam', 'B', '160', 'SMAN 1 Batang', 'IPA', '2013', '9', '9', 'Moch. Mirza', 'Wiraswasta', 'Jl. RE. Martadinata Gg. Bandeng Klidang Wetan Batang', '', '0285-7929720', '085742000102', 'Salsabillamilatina@yahoo.co.id', 'Saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013037', 'Hesti Widyawati', 'R', 'Semarang', '1995-02-19', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013643.jpg', 'Islam', 'A', '148', 'SMAN 15 Semarang', 'IPA', '2013', '8', '8', 'Herman', 'Wiraswasta', 'Jl. Pucang Permai 15 No.4 Semarang', '', '024-76734369', '08156506834', 'Hesti.widyawati@ymail.com', 'Orang Tua', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013038', 'Desta Yusticia Hervyeny Nugroho', 'R', 'Demak', '1995-12-28', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013621.jpg', 'Islam', 'O', '155', 'MAN 1 Semarang', 'IPA', '2013', '50', '49', 'Heryanto Adi Nugroho', 'Dosen', 'Pondok Majapahit 2 Blok B No.24 Mranggen ', 'Demak', '-', '085740042778', 'Hervytata@gmail.com', 'Orang Tua', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013039', 'Bagas Luthfi Alfat', 'R', 'Tanjung', '1994-08-09', 'L', '2013', '20131', '20202', '15', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013603.jpg', 'Islam', '', '175', 'SMAN 1 Semarang', 'IPA', '2013', '0', '50', 'dr. Suwindi', 'PNS', 'Ds Tlogomulyo Rt 3/5 Gubug', 'Grobogan', '', '08562892830', 'bagasluthfi@gmail.com', 'Orang Tua', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013040', 'Rifqi Muhammad', 'R', 'Demak', '1993-05-04', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '667', '20140812013543.jpg', 'Islam', 'B', '171', 'SMAN 1 Demak', 'IPA', '2013', '9', '9', 'H. Puryanto, S.Pd. M.H.', 'PNS', 'Jl. Bhayangkara Baru No.16 RT.2 RW.2 Demak', '', '0291-685922', '082137044394', 'RIFQI_M93@yahoo.com', 'Teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013041', 'Akhfa Muntaha Anggraini', 'R', 'Jepara', '1996-01-12', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140812013448.jpg', 'Islam', 'A', '156', 'SMAN 1 Jepara', 'IPA', '2013', '0', '8', 'Wiyoto, SH, MHKes', 'PNS', 'Ds Sowan Lor Rt 4 Rw 1 Kedung', 'Jepara', '', '085641431020', '', 'Brosur', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013042', 'Sri Margiyanti', 'R', 'Pati', '1994-12-30', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140812013430.jpg', 'Islam', 'O', '155', 'SMAN 2 Pati', 'IPA', '2013', '9', '8', 'Agus Srihono', 'PNS', 'Pucakwangi RT.3 RW.3 Pucakwangi', 'Pati', '089668966356', '085727447771', 'sri.margiyanti@yahoo.com', 'Brosur', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013043', 'Anggi Wiraswara Raditya', 'R', 'Klaten', '1995-09-01', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140812013414.jpg', 'Islam', 'B', '152', 'SMAN 1 Ungaran', 'IPA', '2013', '8', '8', 'Ujang Rahmat Wijaya', 'PNS', 'Jl. Brawijaya RT.4 RW.2 Langensari Timur Ungaran ', 'Semarang', '-', '08995901622', 'anggi.wiraswara@gmail.com', 'Brosur', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013044', 'Ficky Vimbiyanti Ardelia', 'R', 'Brebes', '1994-11-21', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20150422050016.jpg', 'Islam', 'B', '161', 'SMAN 2 Brebes', 'IPA', '2013', '0', '8', 'H. M. Endaryadi, S.Pd, MM', 'PNS', 'Jl Jawa 88A Limbangan Wetan ', 'Brebes', '0283-6174345', '083837141659', 'fickyardelia06@gmail.com', 'Brosur', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013045', 'Arief Pramono', 'R', 'Semarang', '1995-02-24', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140811035934.jpg', 'Islam', 'O', '170', 'SMAN 6 Semarang', 'IPA', '2013', '0', '0', 'Sunyoto', 'Pensiunan', 'Jl. Kapas Tengah Raya H-26 Semarang', '', '-', '085640695860', 'anandajokams@gmail.com', 'Brosur', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013046', 'Safira Khairunnisa', 'R', 'Jakarta', '1995-06-09', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20130803083635.jpg', 'Islam', 'B', '152', 'SMA Hidayatullah Semarang', 'IPA', '2013', '0', '0', 'dr. Johan Arifin', 'Dokter', 'Jl. Jati Elok No.11 Banyumanik ', 'Semarang', '024-70786279', '085727543702', 'riatriwardhani@gmail.com', 'Orang Tua, Brosur', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013047', 'Dinda Rifka Mutiara', 'R', 'Brebes', '1995-04-08', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140812013302.jpg', 'Islam', 'A', '159', 'SMAN 1 Brebes', 'IPA', '2013', '9', '8', 'H. Wantoro, S.H.', 'PNS', 'Jl. Bawang Merah Selatan No.69 RT.1/4 Brebes', '', '-', '083861427247', '-', 'internet', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013048', 'Roza Restu Pambudi', 'R', 'Pati', '1995-08-24', 'P', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140812013223.jpg', 'Islam', 'O', '153', 'SMAN 1 Tayu', 'IPA', '2013', '0', '0', 'Bambang Kuswahyudin', 'PNS', 'Ds Tayu Wetan Rt 7 Rw 1', 'Pati', '', '085641169090', '', 'Brosur', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013049', 'Mohammad Danil Ahsan Murdiputra', 'R', 'Pekalongan', '1995-11-24', 'L', '2013', '20131', '20202', '03', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140811070355.jpg', 'Islam', 'A', '161', 'SMAN 1 Pekalongan', 'IPA', '2013', '0', '50', 'Moh. Hasyim Purwadi', 'PNS Dokter', 'Jl Raya Pisma Blok A/2 Kedungwuni Timur Rt 1 Rw 17', 'Pekalongan', '0285 4482558', '089636622353', 'danil_dt@yahoo.co.id', 'Orang Tua', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A013050', 'Ririn Aprilia Lacana', 'R', 'Toli-Toli', '1996-04-09', 'P', '2013', '20131', '20202', '18', '2013-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20140812013205.jpg', 'Islam', 'O', '157', 'SMA Terpadu Wira Bhakti Gorontalo', 'IPA', '2013', '9', '8', 'Amirudin Lacana', 'Pegawai BUMN', 'Jl. Elang No.16 Tuweley Toh-Toh', 'Sulawesi Tengah', '-', '082192499985', '-', 'Saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014001', 'Nur Amaliana Ayu Nisa', 'R', 'Jepara', '1997-07-11', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20141013075327.jpg', 'Islam', 'O', '155', 'SMAN 1 Pati', 'IPA', '2014', '9', '8', 'Moh Nukin ', 'PNS/TNI/POLRI', 'Desa Saripan Rt 04 Rw 06 Jepara', '', '', '082134963000', 'liananisa11@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014002', 'Mahanani Elma Baskhara', 'R', 'Semarang', '1996-06-24', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20141013074538.jpg', 'Islam', 'B', '164', 'SMAN 15 Semarang', 'IPA', '2014', '0', '8', 'Bambang Sugiarto, S.h, M.hum', 'Pns/tni/polri', 'perum. tulus harapan blok c1/12. Semarang', '', '0246701417', '08982168290', 'mahananielma@gmail.com', 'Guru Smu/smk', '0.00', '', '', '', '-', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014003', 'Edo Phurbo Wicaksono', 'R', 'Temanggung', '1996-07-09', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140613011902.jpg', 'Islam', 'B', '167', 'MAN 1 Temanggung', 'IPA', '2014', '0', '0', 'Umarno', 'Wirausaha', 'desa krajan1 rt. 01/07 kandangan temanggung ', '', '', '085228169699', 'edophurbo@ymail.com', 'internet', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014004', 'Nida Ulfa', 'R', 'Brebes', '1996-01-05', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140612090735.jpg', 'Islam', 'B', '158', 'SMAN 1 Tegal', 'IPA', '2014', '9', '8', 'Wirahmana', 'Pns/tni/polri', 'jl samiaji 19 rt 01/rw 05 kradenan kersana brebes', '', '', '087733379640', 'nida_ulfa@yahoo.co.id', 'Mitra/teman', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014005', 'Ninda Anisa Erika Safura', 'R', 'Semarang', '1995-10-15', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140612081009.jpg', 'Islam', 'B', '158', 'SMAN 6 Semarang', 'IPA', '2013', '9', '9', 'Eko Budi Susatyo', 'Pns/tni/polri', 'jl. gajah timur dalam 1 no.9 semarang ', '', '0246735946', '085865270898', '-', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014006', 'Aniq Malikha Triana', 'R', 'Pemalang', '1996-05-13', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', 'J2A014006.jpg', 'Islam', 'O', '159', 'SMAN 1 Belik', 'IPA', '2014', '0', '0', 'Slamet Santosa', 'Pns/tni/polri', 'Ds. Majalangu RT04/02 Kec. Watukumpul Kab. Pemalang', 'Pemalang', '-', '082313052111', 'anikmalikhatriana@gmail.com', '', '0.00', '', '', '', '-', '', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014007', 'Dea Intania Dewi', 'R', 'Purwakarta', '1995-11-20', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140613014232.jpg', 'Islam', 'B', '156', 'SMAN 1 Darangdan', 'IPA', '2013', '8', '8', 'Marno', 'Pns/tni/polri', 'ds. sawit rt 11/05  darangdan purwakarta jawa barat', '', '-', '081931350559', 'dheaintania@yahoo.co.id', 'Media Elektronik', '0.00', '', '', '', '-', '', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014008', 'Muhammad Ibnu Fadhli', 'R', 'Sorong', '1996-01-17', 'L', '2014', '20141', '20212', '25', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140613082322.jpg', 'Islam', 'A', '165', 'MA Muhammadiyah Aimas', 'IPA', '2014', '0', '0', 'Muhammad Abidin', 'Pns/tni/polri', 'jl wortel rt.05/03 malawele aimas sorong papua barat', '', '081344066516', '082397965878', 'mfadli483@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014009', 'Ajeng Narita Caustina', 'R', 'Balikpapan', '1996-11-25', 'P', '2014', '20141', '20212', '16', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140619063533.jpg', 'Islam', 'O', '152', 'SMAN 1 Balikpapan', 'IPA', '2014', '8', '7', 'Hadi Prayitno', 'Swasta', 'jalan telaga sari 3 rt 35 no 24 ', 'balikpapan', '', '082150604292', 'adrianne9@ymail.com', 'Media Elektronik', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014010', 'Nisma Dinastiti', 'R', 'Ngawi', '1995-11-13', 'P', '2014', '20141', '20212', '05', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '175', '20140619045347.jpg', 'Islam', 'O', '150', 'SMAN 2 Ngawi', 'IPA', '2014', '46', '47', 'Gatot Hari Soekarno', 'Wirausaha', 'ds tawun rt03/rw08 kasreman  ngawi', '', '0351-745764', '085735498642', 'kimhyesun992@gmail.com', 'Media Elektronik', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014011', 'Suaeni Kurnia Wirda', 'R', 'Pasuruan', '1993-01-07', 'P', '2014', '20141', '20212', '05', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20141013074616.jpg', 'Islam', 'O', '151', 'SMAN 1 Pandaan', 'IPA', '2010', '48', '53', 'Suen', 'Pns/tni/polri', 'dsn. arjosari rt.03/18  kejapanan gempol pasuruan ', '', '085604500107', '081252629163', '-', 'Media Elektronik', '0.00', '', '', '', '-', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014012', 'Qurrota A`yun Azhar', 'R', 'Jakarta', '1996-01-16', 'P', '2014', '20141', '20212', '01', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140618065207.jpg', 'Islam', 'AB', '155', 'SMAN 2 Krakatau Steel', 'IPA', '2014', '9', '8', 'Abdul Aziz', 'BUMN', 'jl. sumampir raya komp. bank mandiri eks. bbd no.1 cilegon ', '', '0254 391518', '08568089941', 'gauravaziz@gmail.com', 'Media Elektronik', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014013', 'Nanda Wirawan', 'R', 'Kudus', '1993-10-10', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140618072150.jpg', 'Islam', 'O', '166', 'SMAN 1 Kudus', 'IPA', '2012', '9', '9', 'Mulyohadi', 'Pns/tni/polri', 'karangmalang rt 3 rw 3 gebog kudus', '', '085641518225', '081329594109', 'wirawannanda@gmail.com', 'Mitra/teman', '0.00', '', '', '', '-', '', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014014', 'Muhammad Fachmi Faris', 'R', 'Tegal', '1996-01-16', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140618071927.jpg', 'Islam', 'O', '170', 'SMA AL - Irsyad Tegal', 'IPA', '2014', '0', '0', 'Ir. H. Sukhaemi', 'Pns/tni/polri', 'Jl. sulawesi b 26 limbangan wetan brebes 52218', '', '02836174448', '085729599005', '-', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014015', 'Noor Laili Misbahul Khoir', 'R', 'Rembang', '1995-10-16', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140618070159.jpg', 'Islam', 'O', '175', 'SMAN 1 Jepara', 'IPA', '2014', '0', '0', 'Katijo ', 'Pns/tni/polri', 'desa sinanggul rt/rw 24/04 mlonggo jepara ', 'JEPARA ', '', '089634324746', 'noor.lailimk@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014016', 'Rosalia', 'R', 'Sampit', '1996-08-07', 'P', '2014', '20141', '20212', '14', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140619090353.jpg', 'Islam', 'O', '158', 'SMAN 2 Sampit', 'IPA', '2014', '8', '7', 'Triyono', 'Pns/tni/polri', 'jl. wengga agung raya no. 9a, baamang barat kotawarigin ', '', '053123781', '082155733374', 'tigayana@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', '', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014017', 'Muhamad Taufik Noor Dianzyah', 'R', 'Boyolali', '1995-08-09', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140619084727.jpg', 'Islam', 'A', '168', 'SMAN 1 Teras', 'IPA', '2013', '8', '9', 'Sunarno', 'Pns/tni/polri', 'kiyaran 01/01 gombang sawit boyolali', '', '02763295594', '085642716474', 'taufik_nd@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '-', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014018', 'Rifka Zahrotun Nisa', 'R', 'Kudus', '1996-05-21', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140619075256.jpg', 'Islam', 'O', '150', 'SMAN 1 Bae Kudus', 'IPA', '2014', '9', '8', 'Ngusmin', 'Wirausaha', 'desa gulang rt 02/05. kecamatan mejobo. kabupaten kudus', '', '-', '085713106170', 'rifka_znisa@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014019', 'Amalia Rieska Mauliddya', 'R', 'Blora', '1996-08-14', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140619064242.jpg', 'Islam', 'B', '150', 'SMAN 3 Rembang', 'IPA', '2014', '0', '0', 'Dr. Henny Indriyanti M.kes', 'Pns/tni/polri', 'jl. selamet riyadi km 2 desa sumberjo rt 05 rw 1', 'REMBANG', '0295 693665', '08990986344', 'aiim_mas14@yahoo.co.id', 'Brosur/leaflet/poster', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014020', 'Siti Nur Aini Ayu Ningjanah', 'R', 'Blora', '1995-11-01', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140618083315.jpg', 'Islam', 'O', '159', 'SMAN 1 Randu Blatung', 'IPA', '2013', '8', '8', 'Sukir', 'Wirausaha', 'ds. dukuh kedung sambi rt3/3 temulus randu blatung blora ', '', '', '087833161711', 'siti nur aini ayu ningjanah', 'Mitra/teman', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014021', 'Yogi Nanda Kharismawan', 'R', 'Bantul', '2014-01-07', 'L', '2014', '20141', '20212', '04', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140624064458.jpg', 'Islam', 'A', '175', 'SMAN 2 Banguntapan Bantul', 'IPA', '2014', '8', '7', 'Drs. Hasantosa', 'Wirausaha', 'desa paduresan rt 09/08 imogiri bantul ', '', '', '087739588884', 'nandayogi2575@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014022', 'Dewi Purnamaningtias', 'R', 'Salatiga', '1995-08-18', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140908040354.jpg', 'Islam', 'O', '165', 'SMAN 1 Tengaran', 'IPA', '2013', '8', '8', 'Sri Siyami', 'Wirausaha', 'kwangsan rt05 rw03 desa ketapang susukan kabupaten semarang', 'SEMARANG', '0298615048', '082225260484', 'dnurjanatun@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014023', 'Arinanda Sekar Palupi', 'R', 'Klaten', '1996-01-13', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20141013074727.jpg', 'Islam', 'O', '158', 'SMAN 1 JATINOM', 'IPA', '2014', '8', '8', 'Agus Supriyanto', 'Swasta', 'jatirejo, tibayan, jatinom, klaten', 'KLATEN', '', '085642242000', 'bungaku_s@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014024', 'Hoerul Anam', 'R', 'Subang', '1997-06-27', 'L', '2014', '20141', '20212', '02', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140725043929.jpg', 'Islam', 'O', '167', 'SMAN 3 Subang', 'IPA', '2014', '8', '7', 'Mahdi', 'Pns/tni/polri', 'kp.binong tengah ds.citra jaya rt13/04 binong subang ', '', '0260453334', '081223297535', 'erull1_27@hotmail.co.id', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014025', 'M Ghozy El Yussa', 'R', 'Jepara', '1996-07-05', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20141013074654.jpg', 'Islam', 'B', '165', 'SMAN 1 Jepara', 'IPA', '2014', '9', '8', 'Susanto', 'Pns/tni/polri', 'karanggondang rt 05/ rw 04, mlonggo, jepara', 'JEPARA', '-', '081390576438', 'ghozyell@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014026', 'Shela Rizki Amalia', 'R', 'Semarang', '1995-10-26', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140725035741.jpg', 'Islam', 'O', '155', 'SMAN 15 Semarang', 'IPA', '2014', '8', '8', 'Anis Zafaron', 'Swasta', 'jl. soekarno hatta no:15 semarang', 'SEMARANG', '02470192345', '085786818400', 'amaliashela@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014027', 'Dian Utari Puspitaningtyas', 'R', 'Pati', '1996-02-23', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140725034623.jpg', 'Islam', 'B', '154', 'SMAN 4 Semarang', 'IPA', '2014', '0', '0', 'Wiwik Nuryanti', 'Wirausaha', 'jl bengawan g-99 rt 6, rw 10 payungmas semarang', '', '0247475781', '085740225853', 'dianutari11@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014028', 'Mughni Permatasari', 'R', 'Jepara', '1995-06-07', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20141013074555.jpg', 'Islam', 'O', '155', 'SMAN 1 Pecangaan', 'IPA', '2013', '8', '8', 'Suhadi Atmoko', 'Pns/tni/polri', 'ds.sidigede 08/02 welahan jepara ', 'JEPARA', '-', '085727273702', 'gelizh.blitz@yahoo.co.id', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014029', 'Kurnia Bagas Triwibowo', 'R', 'Kab Semarang', '2014-02-07', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140723081701.jpg', 'Islam', 'A', '170', 'SMAN 1 Ambarawa', 'IPA', '2014', '8', '7', 'Suwarno', 'Pns/tni/polri', 'dsn baan rt03 rw03 desa asinan kec bawen kab semarang', 'KAB SEMARANG', '0298595205', '089669295244', 'kurniabagasnaharize@gmail.com', 'Mitra/teman', '0.00', '', '', '', '-', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014030', 'Dzaki Ala Muttaqien', 'R', 'Ponorogo', '1996-02-11', 'L', '2014', '20141', '20212', '05', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '795', '20140723022747.jpg', 'Islam', 'Ab', '167', 'SMAN 3 Ponorogo', 'IPA', '2014', '8', '8', 'Suyadi', 'Pns/tni/polri', 'jl. r m suryo 7b ploso pacitan jawa timur', 'PONOROGO', '0357885525', '085853218812', 'dokterdzaki@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014031', 'Nisrina Afif Diah Sari', 'R', 'Tangerang', '1996-07-08', 'P', '2014', '20141', '20212', '02', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140723082152.jpg', 'Islam', 'B', '156', 'SMAN 8 Tangerang', 'IPA', '2014', '7', '7', 'Suradi', 'Pns/tni/polri', 'karawaci residence cluster catleya blok c6 no.1 tangerang', 'TANGERANG', '021-29312330', '085693689960', 'nisrinaafif@ymail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014032', 'Anshori Fahruddin', 'R', 'Pangkalan Bun', '1996-04-09', 'L', '2014', '20141', '20212', '14', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20141013075311.jpg', 'Islam', 'A', '160', 'SMAN 2 Pangkalan Bun', 'IPA', '2014', '8', '6', 'Masradin, S.h, M.h', 'Pns/tni/polri', 'jl.pasanah 118 rt.25 kel. sidorejo, kec. arut selatan', 'PANGKALAN BUN', '0532-24693', '085249215936', 'anshori123456@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014033', 'Lovina Julia Kuswandi', 'R', 'Argamakmur', '1996-07-17', 'P', '2014', '20141', '20212', '26', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140724054413.jpg', 'Islam', 'A', '155', 'SMAN 2 Bengkulu', 'IPA', '2014', '7', '7', 'Ahmad Kuswandi, Sh.', 'Swasta', 'Jl. M.S Batu Bara No.39 RT 12 Purwodadi Arga Makmur, Bengkulu Utara', 'Bengkulu', '-', '085368800901', 'lovina_julia@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014034', 'Muhammad Hyoga Putra Delin', 'R', 'Semarang', '1996-06-18', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140724045631.jpg', 'Islam', 'B', '166', 'SMAN 2 Tegal', 'IPA', '2014', '8', '8', 'Dedy Akhirudin', 'Swasta', 'Jalan Raya Talang Gg. Central No.13 Kec. Talang Kab. Tegal', 'Tegal', '-', '081548018002', 'muhammadhyoga@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014035', 'Ivan Febiyanto', 'R', 'Brebes', '1996-02-01', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140724031715.jpg', 'Islam', 'Ab', '173', 'SMAN 1 Tanjung', 'IPA', '2014', '8', '7', 'H.rinanto / Hj.cati', 'Wirausaha', 'desa jagapura rt 04/04 no.40, kec. kersana, kab. brebes', 'Kab. Brebes', '882074', '087824408086', 'febiyantoivan@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014036', 'Wisnu Umaroh Faizal Abdau', 'R', 'Bontang', '1996-05-18', 'L', '2014', '20141', '20212', '16', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140724045258.jpg', 'Islam', 'B', '175', 'SMAN 1 Bontang', 'IPA', '2014', '7', '7', 'Kasdi', 'Pns/tni/polri', 'jalan cumi-cumi 2 rt 01 no 34 tanjung laut indah bontang', '', '-', '085251478904', 'wiznugtl@gmail.com', 'Media Elektronik', '0.00', '', '', '', '-', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014037', 'Bachtiar Dwi Nugroho', 'R', 'Kudus', '1994-05-17', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140724042513.jpg', 'Islam', 'A', '164', 'SMAN 5 Semarang', 'IPA', '2012', '8', '8', 'Dr.aufar', 'PNS', 'Jl.Wolter Monginsidi 91B', 'Semarang', '(024)8454494', '089659141763', 'broo_apple@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014038', 'Claudia Ayu Larasati', 'R', 'Temanggung', '1995-12-29', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140908045419.jpg', 'Islam', 'B', '171', 'SMAN 1 Kuta Badung', 'IPA', '2014', '9', '9', 'Lestari Aji', 'Wirausaha', 'perum taman penta i blok a.33 jimbaran, kuta selatan ', 'BADUNG', '0361703718', '085339135040', 'claudia.ayu.larasati@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014039', 'Erlita Nindya Gushyana ', 'R', 'Tangerang', '1996-01-08', 'P', '2014', '20141', '20212', '28', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140911031524.jpg', 'Islam', 'O', '156', 'SMAN 1 Kabupaten Tangerang', 'IPA', '2014', '8', '8', 'Agus Dwiwanto', 'Swasta', 'kp. pos sentul rt001/rw003 balaraja-tangerang', 'TANGERANG', '', '087808642035', 'erlitanindyag@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014040', 'Ayu Puji Lestari', 'R', 'Palembang', '1997-01-05', 'P', '2014', '20141', '20212', '11', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20141013074635.jpg', 'Islam', 'B', '158', 'SMAN 4 OKU', 'IPA', '2014', '8', '8', 'Darsun', 'Pns/tni/polri', 'jalan padat karya air paoh sekarjaya, baturaja pwkt', '', '08127301445', '085788639438', 'ayupuji97@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014041', 'Ahyi Alfia Husna', 'R', 'Grobogan', '1996-12-22', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '', 'Islam', 'O', '158', 'SMA Assalaam Sukoharjo', 'IPA', '2014', '7', '0', 'Ahmad Syafii', 'Wirausaha', 'desa karangwader  rt 01/04 kec. penawangan kab. grobogan', 'SURAKARTA', '', '085747232226', 'laihuh@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014042', 'Tyas Nur Fadlilah Amaliyah ', 'R', 'Kendal ', '1994-10-31', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140922014630.jpg', 'Islam', 'B', '163', 'SMAN 1 Kendal', 'IPA', '2012', '8', '9', 'Drs Temon ', 'Pns/tni/polri', 'karangsari rt 03 rw 02 kendal ', 'KENDAL ', '', '081296663252', 'ttyasnurfadlilah@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014043', 'Ahmad Aliemuddin Suyudi', 'R', 'Kudus', '1996-12-25', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140906063544.jpg', 'Islam', 'A', '178', 'SMAN 2 Pati', 'IPA', '2014', '9', '7', 'Siti Nafiah', 'Swasta', 'jl. dr. susanto 114b pati', 'PATI', '(0295)381045', '085640859430', 'alim.alfaruq@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014044', 'Septi Duvasti Kurnia Illahi', 'R', 'Batam', '1996-09-19', 'P', '2014', '20141', '20212', '33', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140922073620.jpg', 'Islam', 'B', '165', 'SMA Islamic Villave', 'IPA', '2014', '8', '7', 'Ismail Eddy Syafri', 'Wirausaha', 'sungai pancur bkl. d no.22 rt.02/02 tanjung piau sungai bedug batam', '', '07787028801', '081266961966', '-', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014045', 'Syafira Adera Putri Nurhasanah ', 'R', 'Jakarta', '1996-07-08', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140922074107.jpg', 'Islam', 'O', '150', 'SMA Islam AL- Azhar 1 JKT', 'IPA', '2014', '9', '8', 'Yulianto Adisasmito', 'Swasta', 'perum serpong park blok a5 no1 cluster amethys bsd tangerang ', '', '', '082115472196', 'aderaputri@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'XXL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014046', 'Muhammad Prigel Nashrullah', 'R', 'Nganjuk', '1993-06-09', 'L', '2014', '20141', '20212', '05', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140922072753.jpg', 'Islam', 'O', '169', 'MA YTP Kertosono', 'IPA', '2012', '8', '8', 'Suparno', 'Pns/tni/polri', 'jl.mayjend sutoyo jatirejo,nganjuk,64416 jawa timur', 'NGANJUK', '-', '081231879164', 'mnashrullah34@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '-', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014047', 'Citra Desi Deriya', 'R', 'Kab.semarang', '1996-07-17', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'K', 'B', '', '', '', '', '', '804', '20141013075219.jpg', 'Islam', 'B', '158', 'SMAN 1 Semarang', 'IPA', '2014', '9', '8', 'Andi Sukamto', 'Wirausaha', 'lingkungan bandungan 007/003 bandungan kab semarang', 'SEMARANG', '0298712122', '085641787388', 'citradesideriya@rocketmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014048', 'Triagus Nursasongko Sutiyono', 'R', 'Kudus', '1997-08-01', 'L', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140906032204.jpg', 'Islam', 'B', '156', 'SMAN 1 Pati', 'IPA', '2014', '0', '0', 'Drs. Saripin', 'Pns/tni/polri', 'ds. menawan, 5/5, kec. gebog, kab. kudus', 'PATI', '', '085277684788', 'triagussutiyono@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014049', 'Nadya Chikita Everhard', 'R', 'Semarang', '1996-03-11', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140906023532.jpg', 'Islam', 'A', '159', 'SMAN 14 Semarang', 'IPA', '2014', '8', '7', 'Roberto Everhard', 'Wirausaha', 'jl.tegalsari barat 1 no 27 rt-05 rw-13', 'SEMARANG', '8317442', ':0818292750', 'nadyaeverhard@yahoo.co.id', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A014050', 'Latifa Hanif Zuhri', 'R', 'Kendal', '1996-10-08', 'P', '2014', '20141', '20212', '03', '2014-09-09', '0000-00-00', 'A', 'B', '', '', '', '', '', '804', '20140905091712.jpg', 'Islam', 'O', '160', 'SMA IT ABU BAKAR', 'IPA', '2014', '7', '0', 'Imam Zuhri', 'Swasta', 'desa bumiayu rt 23 rw 07 weleri kendal jawa tengah 51355', 'YOGYAKARTA', '', '082226206468', 'latifahanif.lh@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015001', 'Rizky Nurhidayah', 'R', 'Pangkalan Bun', '1997-10-22', 'P', '2015', '20151', '', '14', '0000-00-00', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150610024635.jpg', 'Islam', '', '151', 'SMAN 1 Pangkalan Bun', 'IPA', '2015', '0', '0', 'H.moehammad Daoed ', 'PNS', 'jl. pasanah gg.kasturi rt 12 madurejo ', 'PANGKALAN BUN', '', '085750895115', 'amanejewel@ymail.com', '', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015002', 'Seffy Vera Faylina', 'R', 'Cirebon', '1997-09-13', 'P', '2015', '20151', '20222', '02', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609072412.jpg', 'Islam', 'A', '160', 'SMA Sekar Kemuning', 'IPA', '2015', '87', '0', 'Sukana', 'Karyawan Swasta', 'taman pulomas block c3 no 2 kedawung', 'CIREBON', '(0231)849368', '089699231938', 'seffyvera13@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015003', 'Yunita Sholekhatul Rizki', 'R', 'Demak', '1997-06-30', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20151104032126.jpg', 'Islam', 'O', '', 'SMAN 1 Demak', 'IPA', '2015', '0', '0', 'Muh.mukson', 'Swasta', 'ds. Bakalrejo rt 08/ 05 guntur ', 'DEMAK', '', '081326085634', 'yunitarizki77@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015004', 'Nindya Firdina Mahya', 'R', 'Demak', '1997-03-21', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609041051.jpg', 'Islam', 'A', '159', 'SMA Islam Sultan Agung 1 Semarang', 'IPA', '2015', '0', '0', 'Edy Soesanto', 'Swasta', 'senggrong rt 03 rw 03 kangkung mranggen', 'Demak', '02470792752', '081228592844', 'firdinamahya@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015005', 'Nabila Farah Khoirunnisa', 'R', 'Pati', '1997-07-18', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609040905.jpg', 'Islam', 'O', '145', 'SMAN 2 Pati', 'IPA', '2015', '0', '0', 'Moh. Matori', 'PNS', 'Ds. Margorejo Rt 02/07 ', 'Pati', '', '081326233915', 'nabilakhoirunnisa18@gmail.com', '', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015006', 'Rizqi Indah Septiyani', 'R', 'Pekalongan', '1997-09-17', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609040733.jpg', 'Islam', 'B', '155', 'SMAN 1 Comal', 'IPA', '2015', '0', '0', 'M. Edi Susilo', 'Wirausaha', 'muncang rt 02 rw 01 bodeh ', 'PEMALANG', '-', '087764780100', 'rizqiindah64@yahoo.com', '', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015007', 'Dhiyas Mareda Hilmanaufar', 'R', 'Tegal', '1997-03-11', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609035544.jpg', 'Islam', 'B', '177', 'SMA Islam Hidayatullah', 'IPA', '2015', '87', '68', 'Dra. Bintang Yulisetyaningtyas M.si', 'PNS', 'Jln. bukit Kelapa Sawit Ii Ah 40 Bukit Kencana Jaya ', 'Semarang', '024-7477371', '085325936033', 'dhiyas03@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015008', 'Silfi Atika Hertanti', 'R', 'Rembang', '1997-02-12', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609035123.jpg', 'Islam', 'O', '167', 'SMAN 2 Rembang', 'IPA', '2015', '80', '0', 'Hamid', 'Swasta', 'ds. kalipang rt 02 rw 04 kec. sarang ', 'REMBANG', '', '08562801098', 'silfi.atika@yahoo.com', 'Spanduk', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015009', 'Farkhi Muhammad', 'R', 'Kendal', '1995-06-16', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150608073242.jpg', 'Islam', 'O', '', 'SMA Muhammadiyah 4 Kendal', 'IPA', '2015', '0', '0', 'Rozikhan', 'PNS', 'ds. kumpulrejo rt.03/01  kaliwungu ', 'KENDAL', '', '087700184927', 'farckhimuhammad@yahoo.co.id', 'Spanduk', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015010', 'Taufik Nur Ikhsan', 'R', 'Semarang', '1996-09-07', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '807', '20150609042536.jpg', 'Islam', 'B', '180', 'SMAN 15 Semarang', 'IPA', '2015', '0', '0', 'Suranto', 'PNS', 'jl. kencur iv/231 rt 9/8 perumahan korpri ', 'SEMARANG', '-', '085842313214', 'taufik.ikhsan7996@gmail.com', '', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015011', 'Megaratih Berliana Kusuma Wardani', 'R', 'Blora', '1998-01-22', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150609044505.jpg', 'Islam', 'B', '157', 'SMAN 1 Randublatung', 'IPA', '2015', '85', '66', 'Agustinus Kistyoko', 'Pns/tni/polri', 'ds. randulawang rt 01 / 01, kel. jati, ', 'BLORA', '', '085232101045', 'megaratih203@yahoo.co.id', 'Spanduk', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015012', 'Vika Dhian Rahmandasari', 'R', 'Kab. Semarang', '1997-05-26', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20151104032200.jpg', 'Islam', 'O', '155', 'SMAN 1 Ungaran', 'IPA', '2015', '0', '0', 'Muh. Hartadi, S.pd., M.si', 'PNS', 'jl. karimunjawa no.34 gedanganak ', 'UNGARAN', '-', '085742219355', 'vikadhianr@gmail.com', '', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015013', 'Zazkia Zita Zhafira Soni', 'R', 'Tegal ', '1997-09-24', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20151104032232.jpg', 'Islam', 'Ab', '155', 'SMAN 1 Slawi', 'IPA', '2015', '0', '0', 'Suyitno', 'PNS', 'jl. raya simpang tiga rt 4 rw 2 kel. yamansari lebaksiu', 'KAB. TEGAL', '-', '085742110647', 'zazkiazita30@gmail.com', 'Spanduk', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015014', 'Delila Rahma', 'R', 'Pekalongan', '1997-12-12', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150610034359.jpg', 'Islam', 'O', '156', 'SMAN 1 Kajen', 'IPA', '2015', '0', '0', 'Drg. Mohammad Asmuni', 'PNS', 'jalan raya wonopringgo pasar lawas rt 10/05 rowokembu ', 'PEKALONGAN', '02854483433', '085743455297', 'delira.dr@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015015', 'Wika Putri Rizkiah', 'R', 'Jepara', '1997-12-07', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20151104032302.jpg', 'Islam', 'O', '159', 'SMAN 1 Welahan', 'IPA', '2015', '0', '0', 'Karmo', 'Wirausaha', 'ds. teluk wetan rt 12/2 kec. welahan ', 'JEPARA', '', '089620001451', 'putry.angel11@yahoo.com', 'Spanduk', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015016', 'Arinta Kusuma Dewi', 'R', 'Batang', '1997-06-23', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150610070631.jpg', 'Islam', 'O', '155', 'SMAN 1 Subah', 'IPA', '2015', '0', '0', 'Burhanudin', 'PNS', 'Karangtengah RT03/05 Subah ', 'Batang', '', '085786043285', 'arintakusumadewi@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015017', 'Haura Hafizhah Zain', 'R', 'Grobogan', '1997-09-14', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150611015725.jpg', 'Islam', 'B', '161', 'SMAN 2 Semarang', 'IPA', '2015', '0', '0', 'Edy Wuryanto', 'Dosen PNS', 'ds. pepe rt 01/02, kec. tegowanu', 'Grobogan', '', '085600122020', 'haurazainzain14@gmail.com', 'Spanduk', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015018', 'Yuda Dwi Anggara', 'R', 'Blora', '1997-11-18', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150611033317.jpg', 'Islam', 'Ab', '162', 'SMAN 1 Randublatung', 'IPA', '2015', '0', '0', 'Bambang Edi Sutomo', 'PNS', 'ds. jeruk rt 05/01 kec. randublatung ', 'BLORA', '-', '082220248478', 'anggarayuda08@gmail.com', 'internet', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015019', 'Ramzy Haidar ', 'R', 'Pekalongan ', '1997-06-06', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150611040828.jpg', 'Islam', 'A', '', 'SMA Islam Hidayatullah ', 'IPA', '2015', '0', '0', 'Islam Setiyanto', 'Wiraswasta', 'patemon rt 01 rw 03 no 3 gunung pati', 'SEMARANG ', '024 70405665', '089666280051', 'ramzyhaidar72@gmail.com', 'Guru Smu/smk', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015020', 'Taqia Zulfa', 'R', 'Grobogan', '1997-07-25', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20150611072538.jpg', 'Islam', 'A', '154', 'SMAN 5 Semarang', 'IPA', '2015', '85', '83', 'Luluk', 'Ibu Rumah Tangga', 'jl. sekayu kepatihan 264 ', 'SEMARANG', '024 3515774', '087832129045', 'taqiazulfa@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015021', 'Nurul Fitri', 'R', 'Empang', '1997-01-29', 'P', '2015', '20151', '20222', '02', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150619034419.jpg', 'Islam', 'Ab', '155', 'SMAN 1 Empang', 'IPA', '2015', '0', '0', 'H. Abdullah Hf', 'Petani', 'ds. awo rt.001/003 ds. empang atas empang ', 'SUMBAWA', '-', '081916827656', 'djarista1729@gmail.com', 'internet', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015022', 'Ariqo Jauza Ulhaq', 'R', 'Sleman', '1996-12-15', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150623031413.jpg', 'Islam', 'B', '175', 'SMAIT Nur Hidayah', 'IPA', '2015', '0', '0', 'Edi Karyadi', 'Karyawan Swasta', 'jl abimanyu rt 01/01 pucangan kartasura ', 'SUKOHARJO', '0271-783267', '085799513137', 'jauzaariqo@gmail.com', 'internet', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015023', 'Nurul Fadhila Addini', 'R', 'Yogyakarta', '1997-08-21', 'P', '2015', '20151', '20222', '04', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150810022131.jpg', 'Islam', 'B', '160', 'SMAN 1 Tanjungpandan', 'IPA', '2015', '73', '86', 'Gugun Suhirman', 'Wiraswasta', 'jl betet no 39 perumnas, air pelempang jaya', 'TANJUNGPANDAN', '-', '081949112131', 'nuruladdini@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015024', 'Yulia Amanda', 'R', 'Bandar Jaya ', '1997-07-05', 'P', '2015', '20151', '20222', '12', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150715042450.jpg', 'Islam', 'O', '160', 'MA Diniyyah Putri Lampung', 'IPA', '2015', '0', '0', 'Riki', 'Wiraswasta', 'Jln.Dahlia RT005/003 Gedung Karyajitu Rawajitu Selatan ', 'Tulang Bawang', '', '081540086987', 'yuliaamanda67@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015025', 'Berliana Dwi Saputri Puryanto', 'R', 'Grobogan', '1997-07-28', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150710075505.jpg', 'Islam', 'O', '160', 'SMAN 2 Semarang', 'IPA', '2015', '0', '0', 'Maspuryanto', 'Pns', 'ds. dorolegi rt:01 rw:03 kec. godong ', 'kab. grobogan', '-', '085727545705', 'berlian.sp@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015026', 'Nur Ismah Cahyani', 'R', 'Polmas', '1997-04-05', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150710065959.jpg', 'Islam', 'O', '160', 'SMA Muhammadiyah 4 Kendal', 'IPA', '2015', '0', '0', 'Dewi Indriana', 'Karyawan Swasta', 'perum. kaliwungu indah, blok b.12 no.14 rt.13/10 ', 'KENDAL', '', '085725903211', 'ismah.cahyani@yahoo.co.uk', 'Spanduk', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015027', 'Muhammad Hoesin Dwi Saputra', 'R', 'Palembang', '1997-11-12', 'L', '2015', '20151', '20222', '11', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20151104031916.jpg', 'Islam', 'B', '168', 'SMAN 1 Slawi', 'IPA', '2015', '0', '0', 'Suharso', 'PNS', 'Jln.Kartini Gang Jatayu 1RT01/08 Slawi Kulon Slawi ', 'TEGAL', '-', '089608685966', 'jrs_saputra17@yahoo.co.id', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015028', 'Charaka Sanghajanna', 'R', 'Blora', '1996-06-11', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150713074005.jpg', 'Islam', 'B', '170', 'SMAN 2 Blora', 'IPA', '2014', '7', '7', 'Teguh Sedio Utomo', 'Wiraswasta', 'Jl. a. yani Iii No.11', 'Blora', '', '085799181386', 'sanghajana.crk@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015029', 'Ika Mustika Wati', 'R', 'Rembang', '1997-06-07', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150714012406.jpg', 'Islam', 'B', '160', 'SMAN 1 Rembang', 'IPA', '2015', '84', '0', 'Rukin', 'Wirausaha', 'ds. tlogotunggal rt 01 rw 05 kec. sumber', 'REMBANG', '', '089699553275', 'ika.mustika1234@gmail.com', 'Guru Smu/smk', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015030', 'Azoura Tamarinda', 'R', 'Tanjung', '1997-08-17', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '839', '20150714023756.jpg', 'Islam', 'O', '160', 'SMAN 1 Randublatung', 'IPA', '2015', '0', '0', 'Sunyoto', 'Polri', 'jl.ronggolawe no.55 rt 01/rw 03 kel.wulung, kec.randublatung', 'BLORA', '', '085640500617', 'azoura17@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015031', 'Hinggadita Adjeng Noor Fadhila', 'R', 'Sragen', '1997-06-23', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150714023402.jpg', 'Islam', 'Ab', '160', 'SMAN 1 Gemolong', 'IPA', '2015', '0', '0', 'Sugeng Hanggono', 'Karyawan BUMD', 'perum gemolong permai no.63 rt.10 gemolong,sragen', 'SRAGEN', '', '085868236931', 'hinggadita10@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015032', 'Galih Lendra Adi Pratama', 'R', 'Pangkalan Bun', '1996-07-14', 'L', '2015', '20151', '', '14', '0000-00-00', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150714024353.jpg', 'Islam', 'B', '165', 'SMAN 2 Pangkalan Bun', 'IPA', '2014', '6', '0', 'Sutiyo', 'Pns', 'jl. HM rafi i btn beringin rindang gg semangka 2 nomor 14 rt 08 pasir panjang ', 'PANGKALAN BUN', '', '081250632321', 'galihlap@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015033', 'Hidnita Durrotul Hanun', 'R', 'Semarang', '1997-09-19', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150715035024.jpg', 'Islam', 'A', '150', 'SMAN 1 Semarang', 'IPA', '2015', '0', '0', 'Subur', 'PNS', 'perumahan permata ngaliyan ii no. 59 rt 10/03 ngaliyan ', 'SEMARANG', '024. 7619113', '085866879165', 'hidnita_dh@live.com', 'Spanduk', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015034', 'Maila Majda Aya Maulida', 'R', 'Jepara', '1997-11-08', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150714062502.jpg', 'Islam', 'B', '160', 'SMA Khadijah Surabaya', 'IPA', '2015', '90', '69', 'Dr. Ali Maimun, M. Kes', 'Pns', 'berahan wetan rt 06 rw 04 kecamatan wedung ', 'Demak', '', '082220225885', 'mmaulidaa@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015035', 'Nurul Arifianti', 'R', 'Kerta Bumi', '1995-05-24', 'P', '2015', '20151', '20222', '02', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150715033743.jpg', 'Islam', 'O', '160', 'SMAN 1 Kuaro', 'IPA', '2013', '9', '7', 'Misron', 'Petani', 'ds. kerta bumi jl. jendral sudirman blok j rt.13 ', 'KUARO', '', '082250494718', '-', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015036', 'Propana Fema Pamungkas', 'R', 'Blora', '1997-09-06', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150724022807.jpg', 'Islam', 'O', '160', 'SMAN 2 Blora', 'IPA', '2015', '0', '0', 'H Sakroni', 'Pensiunan PNS', 'jalan gunung lawu no 56b, rt 008/ rw 001 kel. tempelan kab. blora', 'BLORA', '0296533667', '082133666356', 'propanafemapamungkas@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015037', 'Esti Dwi Oktavia', 'R', 'Bengkalis', '1996-10-09', 'P', '2015', '20151', '20222', '09', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150723072531.jpg', 'Islam', 'O', '159', 'SMAS Cendana', 'IPA', '2015', '0', '0', 'Mohamad Subkhan', 'Polri', 'jl. wonosari km. 3,5 rt 01/05 balaimakam duri kec. mandau', 'kab. bengkalis - riau', '', '085376799922', 'estidwi.oktavia@ymail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015038', 'Ovellia Artita Ryudensa', 'R', 'Semarang', '1997-10-01', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20151104032017.jpg', 'Islam', 'B', '160', 'SMAN 2 Semarang', 'IPA', '2015', '86', '0', 'Martono', 'Karyawan Swasta', 'jl. plamongan abadi no. 153 rt01/09 perum plamongan hijau pedurungan', 'SEMARANG', '0246703632', '082265055067', 'ovelia.artita@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015039', 'Habib Rizqi Samdani', 'R', 'Semarang', '1996-11-15', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150902030903.jpg', 'Islam', 'B', '165', 'SMAN 5 Semarang', 'IPA', '2015', '0', '0', 'Samsudi', 'PNS', 'Jl. klentengsari No.7 Pedalangan Rt03/02 Banyumanik', 'Semarang', '', '085713035571', 'habibsamdani@yhoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'l', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015040', 'Dwi Putro Setiyantomo', 'R', 'Wonogiri', '1996-11-12', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '843', '20150902045119.jpg', 'Islam', 'O', '165', 'SMAN 2 Wonogiri', 'IPA', '2015', '0', '0', 'H.Satiman,SE.MM', 'PNS', 'Ngepoh Kidul RT01/07 Balepanjang Baturetno ', 'Wonogiri ', '', '081215865095', 'putrosetiyantomo40@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015041', 'Naufal Ardi Rachmanda', 'R', 'Bantul', '1996-05-11', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150902065927.jpg', 'Islam', 'B', '160', 'SMAN 1 Bantul', 'IPA', '2014', '0', '0', 'H.mawardi Abdrachman', 'Wiraswasta', 'Jln. parangtritis Km 14 Jetis Patalan Bantul', 'Yogyakarta', '02747459464', '085786666613', 'ardi_naufal@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015042', 'Rahmatus Sani Mutolaatul Kafi', 'R', 'Kudus', '1997-08-03', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150903030031.jpg', 'Islam', 'B', '160', 'MAN 2 Kudus', 'IPA', '2015', '0', '0', 'Drs.h. Karsidi Mpd', 'Pns', 'peganjaran rt. 07/03 bae', 'KUDUS', '0291 445253', '081575940744', 'rahmakahfi@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015043', 'Rizka Fadiqta Awwalin', 'R', 'Bogor', '1997-02-05', 'P', '2015', '20151', '20222', '02', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150903035254.jpg', 'Islam', 'O', '160', 'SMAN 1 Semarang', 'IPA', '2015', '0', '0', 'Sukamto', 'Pns', 'jl. zebra mukti utara ii no.9 rt3/2 pedurungan kidul pedurungan', 'SEMARANG', '(024)6702219', '082133860557', 'fadiqtarizka@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015044', 'Ayu Purudita', 'R', 'Semarang', '1997-06-09', 'P', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150905042657.jpg', 'Islam', 'B', '160', 'SMAN 3 Semarang', 'IPA', '2015', '0', '0', 'Bimo Prayogo,mme', 'Karyawan Swasta', 'Srondol Bumi Indah U-8 RT8/5 Banyumanik', 'Semarang', '0247498210', '085728558900', 'ayupurudhita@ymail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015045', 'Dhindi Dwi Andani', 'R', 'Bandar Lampung', '1997-05-24', 'P', '2015', '20151', '20222', '12', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150905031338.jpg', 'Islam', '', '', 'SMA Sugar Group', 'IPA', '2015', '0', '0', 'Wahyu Hidayat', 'Karyawan Swasta', 'Housing 1 Gpm Blok C-07 Rt003/001 Kel. mataram Udik bandar ', 'Lampung ', '-', '085764952141', 'dhindidwiandani@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015046', 'Nyiemas Ayu Rarashifaa', 'R', 'Tebing Tinggi', '1997-07-04', 'P', '2015', '20151', '20222', '17', '2015-09-08', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20160315045155.jpg', 'Islam', 'B', '160', 'SMAN 4 Lahat', 'IPA', '2015', '0', '0', 'Drg. Ganjar Saras Setyowibowo', 'PNS', 'Jl. Mayor Ruslan III No.35 RT001/001 Pasar Lama', 'Lahat', '(0731)323635', '085382384449', 'nys.ayu.r.shifaa@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015047', 'Raihan Muhammad Alif Farras', 'R', 'Pekalongan', '1997-12-15', 'L', '2015', '20151', '20222', '03', '2015-09-08', '0000-00-00', 'C', 'B', '', '', '', '', '', '908', '20151104032051.jpg', 'Islam', 'B', '165', 'SMAN 3 Pekalongan', 'IPA', '2015', '0', '0', 'Agust Marhaendayana', 'Pns', 'Jl. Tunas 1 No.6 Perum Gama Permai RT01/10 Bendan Kergon ', 'Pekalongan', '02854415651', '085066227510', 'raihanmaf@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015048', 'Firda Rizki Amalia', 'R', 'Balikpapan', '1997-07-04', 'P', '2015', '20151', '', '16', '0000-00-00', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150909040838.jpg', 'Islam', 'O', '160', 'SMAN 5 Balikpapan', 'IPA', '2015', '0', '0', 'Hambali,s.sos,m.ap', 'PNS', 'Perum Korpri Blok H-1 No.10 RT23 Sepinggan Baru ', 'Balikpapan', '(0542) 87627', '085388885184', 'firdarizki85@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015049', 'Layyinatussyifa` Azkia', 'R', 'Semarang', '1997-03-03', 'P', '2015', '20151', '', '03', '0000-00-00', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20150930030208.jpg', 'Islam', 'O', '160', 'SMAN 8 Semarang', 'IPA', '2015', '0', '0', 'H. Falichin Bachtiar', 'Karyawan Swasta', 'Jl. stasiun Rt001/003 Jerakah Tugu ', 'Semarang', '7621016', '085648327660', 'layyinatussyifaazkia@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A015050', 'Deliana Ratih Eva Aryani', 'R', 'Rembang', '1995-12-30', 'P', '2015', '20151', '', '03', '0000-00-00', '0000-00-00', 'A', 'B', '', '', '', '', '', '908', '20151104031216.jpg', 'Islam', 'A', '160', 'SMAN 2 Rembang', 'IPA', '2014', '0', '0', 'Suryan', 'TNI', 'Genengrejo Gg. V/02 RT006/003 Leteh', 'Rembang', '-', '085727517880', 'delya.boya@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016001', 'Agung Setiawan', 'R', 'Lampung Barat', '1998-02-20', 'L', '2016', '20161', '20232', '12', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160503085518.jpg', 'Islam', 'O', '', 'SMAN 1 Liwa', 'IPA', '2016', '0', '0', 'Udin Prawito', 'Karyawan Swasta', 'Desa Tanjung Raya RT01/11 Sukau ', 'Liwa', '', '082175373650', 'dr.diahhastuti@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016002', 'Mufidatul Rohmah', 'R', 'Lada Mandala Jaya', '1998-10-16', 'P', '2016', '20161', '20232', '14', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160503085723.jpg', 'Islam', 'O', '', 'SMAN 1 Pangkalan Lada', 'IPA', '2016', '0', '0', 'Asmuni', 'PNS', 'Ds. Lada Mandala Jaya RT 004/001 Pangkalan Lada Kotawaringin Barat', 'Kotawaringin Barat', '-', '082242381248', 'mufidatulrohmah@gmail.com', 'Guru Smu/smk', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016003', 'Shafira Fitri Anisya', 'R', 'Jepara', '1998-01-26', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160503090045.jpg', 'Islam', 'B', '', 'SMAN 06 Semarang', 'IPA', '2016', '0', '0', 'Nur Sanhadi', 'Karyawan Swasta', 'Jl. Bringin Asri RT 08/Xl Wonosari Ngaliyan ', 'Semarang', '', '087832790412', 'shafiraanisya@gmail.com', 'Guru Smu/smk', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016004', 'Muhammad Destyatosa Irfani', 'R', 'Semarang', '1998-04-26', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160615071803.jpg', 'Islam', 'A', '172', 'SMAN 15 Semarang', 'IPA', '2016', '0', '0', 'Budi Santosa', 'Karyawan Swasta', 'Jalan Bukit Kenanga II d/347 RT04/15 Sendangmulyo Tembalang ', 'Semarang', '', '085727900985', 'ifanmdirfani@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016005', 'Fajar Meganovi', 'R', 'Grobogan', '1996-11-29', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160615073833.jpg', 'Islam', 'O', '160', 'SMA Assalam Sukoharjo', 'IPA', '2015', '0', '0', 'Ermedi', 'Wiraswasta', 'Jalan Hasan Anwar RT04/08 Kuripan Purwodadi', 'Grobogan', '', '085290330635', 'fajarnov29@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016006', 'Sulistyo Shanti Nur Addukha', 'R', 'Brebes', '1998-08-30', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160620033210.jpg', 'Islam', 'B', '150', 'SMAN 1 Bumiayu', 'IPA', '2016', '0', '0', 'Drs.h Susetyo', 'PNS', 'Jl. Rumono No.10 Dkh. Krajan 1 RT02/02 Ds Jatisawit Bumiayu ', 'Brebes', '', '089529035847', 'sulistyoshanti30@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016007', 'Ima Muhimmatun Nisa', 'R', 'Sragen', '1998-03-05', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160620043353.jpg', 'Islam', 'B', '-', 'MA AL-Mukmin Ngruki', 'IPA', '2016', '0', '0', 'Kamid, M.Pdi', 'PNS', 'Karangbendo RT08/04 Krikilan Masaran', 'Sragen', '', '08121523843', 'imamuhim98@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016008', 'Saskia Nourma Yuri Alfalahi', 'R', 'Semarang', '1997-12-23', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160617021157.jpg', 'Islam', 'B', '162', 'SMAN 3 Demak', 'IPA', '2015', '0', '0', 'Arie Budianto ', 'PNS', 'Jl.Cempaka 8 Perumnas Wijaya Kusuma RT01/03 Katonsari Demak', 'Demak', '', '082137313106', 'saskiayurii@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016009', 'Mahshanah Arum Panuntun', 'R', 'Cilacap', '1997-01-30', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160622025556.jpg', 'Islam', 'B', '158', 'SMAN 5 Purwokerto', 'IPA', '2015', '0', '0', 'Drs. Dalhudi Nyamun', 'PNS', 'Jl. Masjid Barat RT28/08 Ds. Widarapayung Wetan Binangun ', 'Cilacap', '', '082221763985', 'mahshanaharum@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016010', 'Muhamad Muchlisin', 'R', 'Demak', '1995-07-04', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '166', 'MA Ponpes Pabelan Magelang', 'IPA', '2014', '0', '0', 'Ahmad Munaji', 'Wiraswasta', 'Gaji RT002/003 Guntur', 'Demak', '-', '085786966687', 'muhlisin04@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'ODS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016011', 'Ulyana Safitri', 'R', 'Pekalongan', '1998-11-29', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160617064347.jpg', 'Islam', 'O', '155', 'SMAN 1 Kajen', 'IPA', '2016', '0', '0', 'Sugiyono', 'PNS', 'Karangemas RT02/01 Sumub Kidul Sragi', 'Pekalongan', '', '089667125480', 'ulyanasafitri73@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016012', 'Shabrina Salma Silmina', 'R', 'Semarang', '1998-02-27', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160629035244.jpg', 'Islam', 'O', '158', 'SMA Islam Hidayatullah', 'IPA', '2016', '0', '0', 'Muhammad Asrori', 'Karyawan Swasta', 'perum tembalang pesona asri blok e no.3', 'SEMARANG', '', '081542557098', '-', 'Guru Smu/smk', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016013', 'Arlanda Diane Mahendra', 'R', 'Sukoharjo', '1998-05-03', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160621031249.jpg', 'Islam', 'B', '165', 'SMAN 3 Sukoharjo', 'IPA', '2016', '0', '0', 'Sutejo', 'PNS', 'Jalan Raden Ayu Serang Mulur RT01/01 Mulur Bendosari', 'Sukoharjo', '', '085728584848', 'arlanda.diane@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016014', 'Aziza Ayu Lestari', 'R', 'Semarang', '1996-07-20', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160621015842.jpg', 'Islam', 'O', '168', 'SMA Prisma Akselerasi Kota Serang', 'IPA', '2013', '0', '0', 'Ilyas', 'Wiraswasta', 'Puri Citra Blok E2 /27  Pipitan-walantaka Serang', 'Banten', '', '089643865532', 'ayuaziza2007@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L4', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016015', 'Nasiha Aulia Khansa', 'R', 'Grobogan', '1998-09-18', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160628020620.jpg', 'Islam', 'O', '167', 'SMAN 1 Kudus', 'IPA', '2016', '0', '0', 'Moh Sofwan', 'Karyawan Swasta', 'Dusun Klatak RT03/05 Desa Tlogomulyo Gubug ', 'Grobogan', '', '082135179213', 'nasihaaulia@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016016', 'Lukman Sikha Prasetyo', 'R', 'Demak', '1998-12-22', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160623063627.jpg', 'Islam', 'Ab', '172', 'SMAN 2 Bae Kudus', 'IPA', '2016', '0', '0', 'Sutardi', 'Pns', 'karanganyar  rt/ rw 003/ 002 karanganyar', 'KUDUS', '081225748282', '087733776019', 'sikhalukman@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016017', 'Isnaini Indana Zulfa', 'R', 'Sukanegara', '1998-09-16', 'P', '2016', '20161', '20232', '11', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160623071121.jpg', 'Islam', 'O', '159', 'MA Futuhiyyah 2 Mranggen', 'IPA', '2016', '0', '0', 'Ishari', 'Wiraswasta', 'Ds. Bedilan Belitang ', 'Oku Timur', '', '085788213643', 'isna.zulfa@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016018', 'Dewi Sejati Kusumaningrum', 'R', 'Bondowoso', '1998-05-21', 'P', '2016', '20161', '20232', '05', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160720022104.jpg', 'Islam', 'O', '159', 'SMAN 3 Blitar', 'IPA', '2016', '0', '0', 'Ir. Haris Suseno, Mm', 'Pns', 'jalan jendral basuki rachmad no. 31/55 jambean sukorejo,', 'Bojonegoro', '', '089634101805', 'drdewi98@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016019', 'Widi Rabiulsani Kamal', 'R', 'Subang', '1998-08-08', 'P', '2016', '20161', '20232', '02', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160714024301.jpg', 'Islam', 'B', '161', 'SMAN 3 Subang', 'IPA', '2016', '0', '0', 'Encep Kamaludin SKM, M.Kes', 'PNS', 'Kp. Warung Loa RT03/06 Desa Gunung Tua Cijambe ', 'Subang', '', '082321266888', 'kamalwidi@yahoo.com', 'Mitra/teman', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016020', 'Farich Fahmi Arsyad', 'R', 'Jepara', '1998-07-06', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160804083030.jpg', 'Islam', 'B', '', 'SMAN 1 Jepara', 'IPA', '2016', '0', '0', 'Ahmad Yazid', 'PNS', 'Bandung Rejo RT002/002 Kalinyamatan ', 'Jepara', '', '082226847577', 'farichfahmiarsyad@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016021', 'Vivy Amalia Ramila', 'R', 'Sukadana', '1998-11-26', 'P', '2016', '20161', '20232', '13', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160804035302.jpg', 'Islam', 'O', '154', 'SMAIT Al-Fityan Boarding School', 'IPA', '2016', '0', '0', 'Tasfirani', 'Wiraswasta', 'Jl. Tanah Merah Sukadana ', 'Kanyong Utara', '', '085345548004', 'vivyamaliar@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016022', 'Dita Noviani', 'R', 'Madiun', '1997-11-07', 'P', '2016', '20161', '20232', '05', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160820044122.jpg', 'Islam', '-', '150', 'SMAN 3 Madiun', 'IPA', '2016', '0', '0', 'Didik Sugeng Royanto', 'Wiraswasta', 'Jl. Srigunting 17 RT14/05 Nambangan Lor Mangu Harjo ', 'Madiun', '(0351)497563', '081231732159', 'ditanovianii@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016023', 'Rezki An Najmi Fathan', 'R', 'Indramayu', '1998-04-25', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'A', '168', 'SMA Al-Azhar 5 Cirebon', 'IPA', '2015', '0', '0', 'Budi Santoso', 'PNS', 'Jl Kertamulya RT04/02 Kertamulya Bongas ', 'Indramayu', '081324010174', '081324010174', 'rezkifatan25@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016024', 'Faradis Salsabila', 'R', 'Semarang', '1998-02-14', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '147', 'SMAN 2 Semarang', 'IPA', '', '0', '0', 'Afenan', 'PNS', 'Jl. Rogojembangan Timur RT06/05 Tandang Tembalang ', 'Semarang', '', '089622222235', 'faradis.salsabila@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016025', 'Amalia Nurhidayah', 'R', 'Grobogan', '1998-01-20', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160819085228.jpg', 'Islam', 'B', '161', 'SMAN 1 Purwodadi', 'IPA', '2016', '0', '0', 'Dr. Iman Santosa. M,Kes', 'Pensiunan', 'Jl. D.I. Panjaitan No.70 RT004/014 Purwodadi ', 'Grobogan', '0292426555', '089665970524', 'amalianurhidayah77@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L3', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016026', 'Achmad Muhandis Nabila', 'R', 'Grobogan', '1998-03-28', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160819090557.jpg', 'Islam', '-', '165', 'MA NU TBS Kudus', 'IPA', '2016', '0', '0', 'H. Parsito', 'PNS', 'Klambu Rt04/03 ', 'Grobogan', '', '081228496555', 'handisachmad@yahoo.com', 'Mitra/teman', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016027', 'Khairunnisa Hanifah', 'R', 'Kudus', '1997-11-11', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '157', 'SMAN 1 Kudus', 'IPA', '2015', '0', '0', 'H. Noor Dahlan, Sp', 'Wiraswasta', 'Pasuruhan Lor RT01/09 Jati ', 'Kudus', '', '087733837377', 'khairunnisa_hanifah@ymail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016028', 'Nadila Rahmi', 'R', 'Kolonodale', '1997-11-30', 'P', '2016', '20161', '20232', '18', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160813033905.jpg', 'Islam', 'O', '155', 'SMAN 1 Petasia', 'IPA', '2015', '0', '0', 'Aziz Mansyur', 'Wiraswasta', 'Jl Yos Sudarso No.12A Petasia ', 'Morowali Utara', '', '082227152069', 'nadila.rahmy@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016029', 'Hulman Miftah', 'R', 'Jakarta', '1998-07-05', 'L', '2016', '20161', '20232', '05', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160813033953.jpg', 'Islam', 'O', '168', 'SMAN 2 Bondowoso', 'IPA', '2016', '0', '0', 'Dr. Burhan Miftah, M.p.d.', 'Wiraswasta', 'Jl. Letjen S. Parman No.21 RT01/01 Badean', 'Bondowoso', '', '089520573950', 'hulmanboy@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016030', 'Novita Maharani', 'R', 'Demak', '1997-06-08', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160811081126.jpg', 'Islam', '-', '155', 'SMAN 3 Demak', 'IPA', '2015', '0', '0', 'H. Ngatimin', 'Wiraswasta', 'Klitih RT19/04 Karangtengah ', 'Demak', '081914617880', '085726401173', 'novitajunianto1814@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016031', 'Muhammad Irfanuddin', 'R', 'Rembang', '1997-08-14', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160810064400.jpg', 'Islam', '-', '161', 'SMAN 1 Rembang', 'IPA', '2015', '0', '0', 'Muchammad Munfid', 'Wiraswasta', 'Sumbergirang RT02/08 Lasem ', 'Rembang', '081336820833', '081336820833', 'muhammadirfanuddin97555@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016032', 'Belanita', 'R', 'Serang', '1998-07-19', 'P', '2016', '20161', '20232', '16', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '155', 'SMAN 1 Balikpapan', 'IPA', '2016', '0', '0', 'Muslimin Jam`an', 'Wiraswasta', 'Jl. Marsma R. Iswahyudi RT023/025 No.74 Sepinggan ', 'Balikpapan', '-', '082350197088', 'bebenitaa@yahoo.com', 'Mitra/teman', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016033', 'Leonita Reza Palevi', 'R', 'Pati', '1998-08-21', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160811080615.jpg', 'Islam', 'A', '163', 'SMAN 2 Pati', 'IPA', '2016', '0', '0', 'Puji Hartono', 'Polri', 'kutoharjo rt 3 rw 1 karangdowo ', 'PATI', '', '085290434335', 'leonitareza21@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016034', 'Astri Kusuma Pradika', 'R', 'Klaten', '1997-08-16', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160827070950.jpg', 'Islam', 'O', '161', 'SMAN 2 Klaten', 'IPA', '2015', '0', '0', 'Basuki', 'PNS', 'Krapyak RT001/008 Merbung Klaten Selatan', 'Klaten', '0272324340', '085743359800', 'astripradika@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016035', 'Septi Ari Widianti', 'R', 'Semarang', '1998-09-29', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160824020232.jpg', 'Islam', 'O', '155', 'SMAN 1 Ungaran', 'IPA', '2016', '0', '0', 'Kusnun', 'Wiraswasta', 'Jl. Mr. Koesbiono Tjondrowibowo No.10 RT02/01 Sekaran Gunungpati ', 'Semarang', '0248507922', '081283363890', 'septiariwidianti@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016036', 'Attidhira Citra Lestari Sudrajat', 'R', 'Kupang', '1997-09-19', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160827034145.jpg', 'Islam', 'O', '159', 'SMAN 2 Semarang', 'IPA', '2015', '0', '0', 'Koeshartono Arif Sudrajat', 'Polri', 'Jl. Singa Tengah Dalam 2 No.17 RT07/06 Pedurungan ', 'Semarang', '', '081225239797', 'attidhiracitra@ymail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016037', 'Hilda Dwi Handayani', 'R', 'Serang', '1998-01-26', 'P', '2016', '20161', '20232', '28', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160827022252.jpg', 'Islam', 'B', '', 'SMAN 1 Kramatwatu', 'IPA', '2016', '0', '0', 'Suhartono', 'Karyawan Swasta', 'Pondok Cilegon Indah Blok C 64 No.29 RT01/06 Harja Tani Keramatwatu', 'Serang', '-', '089694805537', 'hildadwi.handayani@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016038', 'Diyah Nur Fitria Munawaroh', 'R', 'Grobogan', '1999-01-25', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '148', 'SMA Al-Muayyad Surakarta', 'IPA', '2016', '0', '0', 'Masdi', 'Wiraswasta', 'Jl. Blora km 9 Jangkung Jono RT001/010 Tawangharjo ', 'Grobogan', '-', '085643618817', 'dfitriamunawaroh@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016039', 'Emilia Nurul Sholekah', 'R', 'Grobogan', '1998-07-29', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'B', '153', 'SMAN 1 Gubug', 'IPA', '2016', '0', '0', 'Wagiman', 'Petani', 'Kembang Gading RT01/04 Ginggangtani Gubug', 'Grobogan', '', '085727054934', 'emilianuruls29@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'ODS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016040', 'Satriyo Atmojo Sri Pamungkas', 'R', 'Tenggarong', '1997-07-09', 'L', '2016', '20161', '20232', '16', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '165', 'SMAN 10 Samarinda', 'IPA', '2015', '0', '0', 'Sutikno', 'Wiraswasta', 'Telaga Kencana RT06/02 Manunggal Jaya Tenggarong Seberang ', 'Kutai Kartanegara', '', '085250525152', 'nfsmayo@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016041', 'Azzuhra Zhafirah Rizviar', 'R', 'Semarang', '1998-09-17', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160902034729.jpg', 'Islam', 'B', '156', 'SMAN 5 Semarang', 'IPA', '2016', '0', '0', 'Dr. H. Umar Utoyo', 'Karyawan Swasta', 'jl. randusari pos ii nomor 330 rt/rw 002/002 semarang selatan ', 'SEMARANG', '', '08156573000', 'azzuhrarizviar@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016042', 'Andra Mahyuza', 'R', 'Semarang', '1998-02-02', 'L', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160905014711.jpg', 'Islam', 'O', '183', 'SMA Semesta', 'IPA', '2016', '0', '0', 'Mashuri', 'Polri', 'Jl.Parang Barong 3 No.6 RT002/009 Tlogosari Kulon Pedurungan', 'Semarang', '', '082136812355', 'mahyuzaandra@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016043', 'Elrizkha Adinda Anugraheni', 'R', 'Semarang', '1998-09-21', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160811065803.jpg', 'Islam', 'O', '157', 'SMAN 4 Semarang', 'IPA', '2016', '0', '0', 'Suwignyo', 'PNS', 'Jl. Taman Durian 2 No.6 RT002/001 Srondol Wetan ', 'Semarang', '', '083838305822', 'elrizkhadinda@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016044', 'W Reza Noviati Septiyan', 'R', 'Tegal', '1998-11-07', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '152', 'MAN Babakan Ciiwaringin', 'IPA', '2016', '0', '0', 'Syaefudin', 'Wiraswasta', 'Kertaharja RT05/02 Jl Beringin Gg Sirsak Kramat', 'Tegal', '', '085293400180', 'eza.septiyan@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016045', 'Zulfah Aghnia Hurin', 'R', 'Kendal', '1997-12-07', 'P', '2016', '20161', '20232', '03', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '20160907032902.jpg', 'Islam', 'O', '150', 'SMAN 1 Boja', 'IPA', '2016', '0', '0', 'Muhamad Sahuri', 'Pns', 'jl. sunan katon, rt.01/03 tabet limbangan ', 'KENDAL', '-', '081805915009', 'zulfahaghnia07@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A016046', 'Dewi Yunita Sari', 'R', 'Atambua', '1996-05-07', 'P', '2016', '20161', '20232', '24', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '', '', 'Islam', 'O', '160', 'Pondok Modern Darussalam Gontor Pon', 'IPA', '2016', '0', '0', 'Riyanto', 'Wiraswasta', 'tubakioan 001/001 fatukbot atambua ', 'kab.belu ntt', '', '082146996189', 'thewiesari76@yahoo.com', 'Mitra/teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('61026', 'C', '12201', 'J2A016047', 'Luluk Hanifa Zahraniarachma', 'R', 'Semarang', '1998-04-17', 'P', '2016', '20161', '20232', '3', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20160920020034.jpg', 'Islam', 'B', '150', 'SMAN 15 Semarang', 'IPA', '2016', '0', '0', 'Rachmad Pamudji', 'PNS', 'Jl. Bukit Kenanga II D/357 RT04/15 Perumnas Bukit Sendangmulyo Tembalang ', 'Semarang', '024-76742136', '82137424033', 'luklukhanifa17@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('61026', 'C', '12201', 'J2A016048', 'Maghfira Sekar Anissa Devega', 'R', 'Bitung', '1999-05-03', 'P', '2016', '20161', '20232', '17', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20160919040507.jpg', 'Islam', 'O', '153', 'SMAN 2 Bitung', 'IPA', '2016', '0', '0', 'Heru Purwanto', 'Pns', 'kadoodan rt/rw. 007/002 lingkungan 2, kec.madidir, ', 'kota bitung, sulawesi utara', '', '85342502851', 'anishadevega.ad@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('61026', 'C', '12201', 'J2A016049', 'Yusril Akhmad Dwiyafi', 'R', 'Pekalongan', '1998-02-07', 'L', '2016', '20161', '20232', '3', '2016-09-13', '0000-00-00', 'A', 'B', '', '', '', '', '', '837', '20160921101818.JPG', 'Islam', 'O', '175', 'SMAN 1 Brebes', 'IPA', '2016', '0', '0', 'Karyanto', 'PNS', 'Dukuhtengah RT 006/001 Dukuhtengah Ketanggungan ', 'BREBES', '', '85743698856', 'yusrilad@gmail.com', 'internet', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017001', 'Alifa Putri Noor Ikhsanti', 'R', 'Pekanbaru', '1999-03-07', 'P', '2017', '20171', '20242', '09', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '', 'Islam', 'O', '157', 'SMAN 1 Pekanbaru', 'IPA', '2017', '0', '0', 'Noor Fuad, S.Hut, M.Si.', 'Karyawan Swasta', 'Jl. Handayani Residence Blok C 7 RT002/007 Sidomulyo Timur Marpoyan Damai ', 'Pekanbaru', '', '08117528415', 'alifapnoor@gmail.com', 'Lainnya', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017002', 'Izaz Zayyan Listy Putri', 'R', 'Grobogan', '1999-09-28', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170508084541.jpg', 'Islam', 'Ab', '157', 'SMAN 1 Purwodadi', 'IPA', '2017', '0', '0', 'Budi Sasmito', 'PNS', 'Bandungharjo RT007/02 Toroh ', 'Grobogan', '', '082242688550', 'ichaicha987@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017003', 'Dea Hardyana Putri', 'R', 'Kudus', '1999-02-05', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '', 'Islam', 'A', '152', 'MAN 2 Kudus', 'IPA', '2017', '0', '0', 'Anhar', 'PNS', 'Bendo RT04/02 Bae Bae ', 'Kudus', '', '081325506369', 'deahardyana@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017004', 'Mellyka Amalia Sabrina', 'R', 'Jepara', '1998-11-20', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170805111955.jpg', 'Islam', 'B', '168', 'SMAN 1 Tahunan', 'IPA', '2017', '0', '0', 'Ali Syafik', 'PNS', 'Pecangaan Kulon RT004/006 Pecangaan ', 'Jepara', '', '089606701460', 'shirenabrina@yahoo.co.id', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017005', 'Aisyah Nafa Agustin', 'R', 'Probolinggo', '1999-08-19', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170710023449.jpg', 'Islam', 'O', '153', 'SMA Budi Utomo', 'IPA', '2017', '0', '0', 'Mokhamad Fauji', 'Karyawan Swasta', 'Graha Citra Santosa 1 No.4 RT004/005 Leteh Rembang ', 'Rembang', '085292761703', '085231379531', 'mokhamadfauji72@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017006', 'Asy Syifa Brillian Avicenna', 'R', 'Tarakan', '1999-11-05', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170521043553.jpg', 'Islam', 'O', '163', 'SMAN 1 Tanjung', 'IPA', '2017', '0', '0', 'Bambang Rudiyanto', 'PNS', 'Jl. Pancasila No.2 RT004/003 Ciampel Kersana ', 'Brebes', '', '081391351927', 'brillianavicenna@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017007', 'Julio Sesco Artamulananda', 'R', 'Kab. Semarang', '1999-07-06', 'L', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170610094108.jpg', 'Islam', 'O', '165', 'SMAN 5 Magelang', 'IPA', '2017', '0', '0', 'Mustofa', 'PNS', 'Jambu RT002/001 Jambu ', 'Semarang', '', '081225527048', 'juliokoko@rocketmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017008', 'Julio Andro Artamulandika', 'R', 'Kab.semarang', '1999-07-06', 'L', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170616080751.jpg', 'Islam', 'O', '165', 'SMA Muhamaddiyah 1 Kota Magelang', 'IPA', '2017', '0', '0', 'Purnomo', 'Karyawan Swasta', 'Jambu RT002/001 Jambu', 'Semarang', '', '081225292015', 'julio_andro@rocketmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017009', 'Keby Pratama Cesaria ', 'R', 'Tegal', '1998-08-13', 'L', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170628023129.jpg', 'Islam', 'O', '172', 'MAN 2 Kudus', 'IPA', '2016', '0', '0', 'I Ketut Setiawan', 'Wiraswasta', 'Ngembal Kulon RT001/003 Jati ', 'Kudus', '', '085876319038', 'kebycesaria13@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'XXL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017010', 'Selma Islamiyah', 'R', 'Sukamara', '1999-10-21', 'P', '2017', '20171', '20242', '14', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20170629024318.jpg', 'Islam', 'Ab', '158', 'SMAN 1 Sukamara', 'IPA', '2017', '0', '0', 'Suhandi', 'PNS', 'Jl.Ahmidi RT05/03 Sukamara ', 'Sukamara', '', '081225225586', 'selmaislamiyah97@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017011', 'Ismaya Aria Sari', 'R', 'Trengguli', '1999-06-28', 'P', '2017', '20171', '20242', '18', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170630103918.jpg', 'Islam', 'O', '155', 'SMAN 4 Kendari', 'IPA', '2017', '0', '0', 'Sarmanto', 'Karyawan Swasta', 'Jl. Dr Sutomo RT001/001 Tobuuwa Puuwatu', 'Kendari', '', '082187700312', 'ismayaaria@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017012', 'Laily Rahmah', 'R', 'Pajukungan', '1999-07-20', 'P', '2017', '20171', '20242', '15', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170805111752.jpg', 'Islam', 'O', '157', 'SMAN 1 Barabai', 'IPA', '2017', '0', '0', 'Muhammad Ilyas, SE', 'PNS', 'Pajukungan RT01/01 Barabai', 'Barabai', '', '081250761464', 'lailyrahmah99@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017013', 'Firdhan Adhie Fawwazillah', 'R', 'Majalengka', '1999-01-02', 'L', '2017', '20171', '20242', '02', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170629025102.jpg', 'Islam', 'B', '163', 'SMAN 1 Majalengka', 'IPA', '2017', '0', '0', 'H. Dede Supena Nurbahar', 'PNS', 'Jl. Pemuda No.5 RT003/010 Llingkungan Kamulyan Majalengka Kulon Majalengka ', 'Majalengka', '', '085295956281', 'firdhan.adi17@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017014', 'Nabela Intania Sekarini', 'R', 'Semarang', '1998-12-11', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170616014707.jpg', 'Islam', 'B', '151', 'SMAN 2 Semarang', 'IPA', '2017', '0', '0', 'Sutanto.SH', 'POLRI', 'Jl.Klipang Pesona Asri 1 No.11 RT004/017 Sendangmulyo Tembalang', 'Semarang', '02476738790', '081347357799', '', 'Lainnya', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017015', 'Isnadia Rachmah Ika', 'R', 'Surakarta', '1999-03-26', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170619114121.jpg', 'Islam', 'A', '149', 'SMAN 3 Wonogiri', 'IPA', '2017', '0', '0', 'Subandi', 'PNS', 'Kepyar RT01/06 Ngadirojo Kidul Ngadirojo ', 'Wonogiri', '', '085200692646', 'isnadiarachmahika@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017016', 'Rika Widya Kartika', 'R', 'Brebes', '1999-01-18', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170619112244.jpg', 'Islam', 'B', '159', 'SMAS Unggulan Pondok Modern Selamat', 'IPA', '2017', '0', '0', 'Zamroni, ST', 'PNS', 'Cikeusal Kidul RT002/001 Ketanggungan ', 'Brebes', '', '081902673590', '', 'Mitra/teman', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017017', 'Vina Widya Putri', 'R', 'Batam', '1999-04-04', 'P', '2017', '20171', '20242', '07', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170619104929.jpg', 'Islam', 'B', '150', 'SMAN 3 Batam', 'IPA', '2017', '0', '0', 'Muhammad Rafi', 'Pns', 'baloi harapan 2 blk a no.124 rt/rw 001/003 kec. bengkong indah kota batam; kepulauan riau', 'BATAM', '081270116313', '082171322649', 'rafi1202@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017018', 'Hety Rahmawati', 'R', 'Jepara', '1998-06-03', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170629023852.jpg', 'Islam', '-', '160', 'SMAN 1 Welahan', 'IPA', '2017', '0', '0', 'Henri Setiawan', 'Wiraswasta', 'Pelang RT02/01 Mayong ', 'Jepara', '', '089512133422', 'rahmawatihety@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017019', 'Shafira Varianda Fatimah', 'R', 'Pekalongan', '1999-06-20', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170630103152.jpg', 'Islam', 'A', '155', 'SMAN 4 Pekalongan', 'IPA', '2017', '0', '0', 'Sunarto', 'Polri', 'jl. anggrek ii perum  binagriya no. 432 rt/rw 001/003 kec. pekalongan barat kota pekalongan; jateng', 'PEKALONGAN', '(0285)441596', '085742799384', 'varianda20shvr@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017020', 'Amalia Asri Ayuningtyas', 'R', 'Biak Numfor', '1999-08-04', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20170629024747.jpg', 'Islam', 'B', '158', 'SMAN 1 Semarang', 'IPA', '2017', '0', '0', 'Agung Aristyawan Adhi', 'PNS', 'Perumahan Graha Wahid Cluster Madrid A11 Tembalang ', 'Semarang', '', '081327239615', 'amalia.asri33@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017021', 'Hakmi Adhimah', 'R', 'Kendal', '1998-09-30', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170620082111.jpg', 'Islam', 'B', '', 'PONDOK MODERN DARUSSALAM GONTOR PUT', 'IPA', '2017', '0', '0', 'Rozikhan', 'PNS', 'Kumpul Rejo RT003/001 Kaliwungu ', 'Kendal', '', '08122800206', 'amikadhimah@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017022', 'Melinda Savira Ayudyawati', 'R', 'Semarang', '1998-12-11', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170629101132.jpg', 'Islam', 'B', '151', 'SMAN 2 Semarang', 'IPA', '2017', '0', '0', 'Sutanto.SH', 'POLRI', 'Jl.Klipang Pesona Asri 1 No.11 RT004/017 Sendangmulyo Tembalang ', 'Semarang', '02476738790', '081347357799', '', 'Lainnya', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017023', 'Syarafina Ummu Salamah', 'R', 'Sragen', '1998-09-19', 'P', '2017', '20171', '20242', '01', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170628110714.jpg', 'Islam', 'O', '160', 'SMA Muhammadiyah Boarding School Yo', 'IPA', '2017', '0', '0', 'Sunarto, Skm. Mm', 'PNS', 'Dalangan RT25 Kliwonan Masaran ', 'Sragen', '-', '08121530282', 'saraffina24@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017024', 'Ayu Anggraeni Mardian Ningrum', 'R', 'Jakarta', '1999-09-24', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170628111322.jpg', 'Islam', 'O', '157', 'SMAN 1 Ambarawa', 'IPA', '2017', '0', '0', 'Mardi', 'Tni', 'asrama yon zipur 4/tk rt/rw 001/013 kebondowo banyubiru ', 'kab. semarang; jateng', '', '081391791254', 'anggraeniayu24.aa@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017025', 'Silvia Hanum Salsabella', 'R', 'Temanggung', '1999-06-30', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170628022748.jpg', 'Islam', 'O', '160', 'SMAN 1 Temanggung', 'IPA', '2017', '0', '0', 'Kamiso', 'Pns', 'mulyosari rt/rw 003/003 kec. parakan ', 'kab. temanggung; jateng', '', '085868180444', 'silfiahanumsalsabella@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017026', 'Sahara Sa`adillah Isri', 'R', 'Pangkalan Bun', '1999-05-12', 'P', '2017', '20171', '20242', '14', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170707105406.jpg', 'Islam', 'O', '151', 'SMAN 1 Pangkalan Bun', 'IPA', '2017', '0', '0', 'Isdiyanto, SE', 'PNS', 'Jl. Iskandar Gang Salak RT17/05 Madurejo Pangkalan Bun ', 'Kotawaringin Barat', '053224523', '085249111449', '-', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017027', 'Thania Olivia Fahrie', 'R', 'Koto Agung', '1999-10-30', 'P', '2017', '20171', '20242', '08', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170911050159.JPG', 'Islam', 'A', '157', 'SMAN 1 Sitiung', 'IPA', '2017', '0', '0', 'Zulfahrie', 'Wiraswasta', 'Jl Jendral Sudirman Jorong Koto Agung Kanan Blok B Sitiung 1 Sitiung', 'Dharmasraya', '08126700566', '0822826994689', 'thaniaolivia54@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017028', 'Annindya May Annur', 'R', 'Brebes', '1999-05-08', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170630090542.jpg', 'Islam', 'B', '145', 'SMAN 1 Brebes', 'IPA', '2017', '0', '0', 'Hadi Susanto', 'Wiraswasta', 'Jl. Rendu Gede RT005/004 Kemukten Kersana ', 'Brebes', '0283889357', '08998970647', 'annindya0808@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017029', 'Khairunnisa Pulungan`', 'R', 'Padangsidimpuan', '1999-03-02', 'P', '2017', '20171', '20242', '07', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170805111518.jpg', 'Islam', 'O', '160', 'SMA Nurul Ilmi Padangsidimpuan', 'IPA', '2017', '0', '0', 'Erizal Pulungan', 'Wiraswasta', 'Jl. Kapten Koimah Wek 1', 'Padangsidimpuan', '', '082276780535', 'kpulungan5@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017030', 'Aulia Adila Rizqi', 'R', 'Sukajaya', '1999-05-23', 'P', '2017', '20171', '20242', '12', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '506', '20170805111300.jpg', 'Islam', 'B', '144', 'MA Mualimat Muhammadiyah YK', 'IPA', '2017', '0', '0', 'Sukiman', 'Pns', 'jl. raya mekar sari jaya rt/rw 001/002  lambu kibang ', 'kab. tulang bawang barat', '-', '082241962442', 'auliaadilqi65@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017031', 'Febriana Sulistya Utami', 'R', 'Batang', '1999-02-04', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '', 'Islam', 'A', '154', 'SMAN 1 Subah', 'IPA', '2017', '0', '0', 'Suharta', 'PNS', 'Tembok RT03/01 Limpung ', 'Batang', '', '082325762764', 'sulistyafebriana@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'ODS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017032', 'Annisa Husna Faadhila', 'R', 'Semarang', '1999-10-24', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '', 'Islam', 'O', '154', 'SMAN 2 Semarang', 'IPA', '2017', '0', '0', 'Ariyanto Budiarto', 'Karyawan Swasta', 'Jl. Kapas Raya D/335 RT02/03 Gebangsari Genuk', 'Semarang', '0246582779', '085776694380', 'annisahusna@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'ODS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017033', 'Dela Martha Devi', 'R', 'Grobogan', '2000-03-03', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170803114358.jpg', 'Islam', 'O', '160', 'SMAN 1 Purwodadi', 'IPA', '2017', '0', '0', 'Jadi', 'PNS', 'Selo RT05/05 Tawangharjo ', 'Grobogan', '', '082133344459', 'marthadela39@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017034', 'Aprilia Fajrin', 'R', 'Banjarmasin', '1999-04-21', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170808104318.jpg', 'Islam', '-', '145', 'SMAN 12 Semarang', 'IPA', '2017', '0', '0', 'Rulyanton Suyatno', 'Karyawan Swasta', 'Jl. Randu Asri 1 RT005/001 Sumurejo Gunung Pati', 'Semarang', '024 6926179', '081325461899', 'apriliafjrn@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017035', 'Rif`an Irham Maulana', 'R', 'Sragen', '1998-05-07', 'L', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170807110531.jpg', 'Islam', 'O', '', 'SMAN SBBS', 'IPA', '2017', '0', '0', 'Agus Ulinuha', 'Karyawan Swasta', 'Sidomulyo RT24b/V Krikilan Masaran', 'Sragen', '--', '085702190266', 'maulana.aan5@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017036', 'Juliana Nursetyaningtyas', 'R', 'Jakarta', '1999-07-10', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170808103853.jpg', 'Islam', 'A', '163', 'SMAN 4 Semarang', 'IPA', '2017', '0', '0', 'Tyas Koesharjadi', 'Pns', 'Jl. Karangrejo 5B No.9 RT02/03 Banyumanik Banyumanik ', 'Semarang', '', '081326690913', 'julianannsasha@yahoo.co.id', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017037', 'Muhammad Maulana Aji', 'R', 'Semarang', '1999-11-24', 'L', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170808103647.jpg', 'Islam', 'O', '175', 'SMAN 11 Semarang', 'IPA', '2017', '0', '0', 'Muhamad Masrofi', 'PNS', 'Jl. Kedungmundu Perum Graha Wahid Cluster Madrid Blok G 12 B RT003/027 Sendangmulyo Tembalang ', 'Semarang', '0246709795', '087832335915', 'ajimaulanaalan@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017038', 'Shoffan Marshush', 'R', 'Jakarta', '1999-04-24', 'L', '2017', '20171', '20242', '28', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170809100317.jpg', 'Islam', '', '168', 'MA MA`HAD ALZAYTUN INDRAMAYU', 'IPA', '2017', '0', '0', 'Sardi S Sos', 'Swasta', 'Jl Bhakti no.32 RT01/07 Ciputan Ciputat', 'Tangerang Selatan', '-', '082214381654', 'sardikertataruna@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017039', 'Ovie Luksita Lathifa', 'R', 'Semarang', '1999-08-01', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '20170809100526.jpg', 'Islam', '-', '160', 'SMAN 1 Semarang', 'IPA', '2017', '0', '0', 'Rukan', 'Wiraswasta', 'Jl Menoreh Utara IX/42 RT07/01 Sampangan Gajah mungkur', 'Semarang', '8507140', '081222864270', 'ovieluksita@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017040', 'Resty Annisya', 'R', 'Sampit', '1999-03-31', 'P', '2017', '20171', '20242', '14', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '989', '', 'Islam', 'O', '150', 'SMAN 2 Sampit', 'IPA', '2017', '0', '0', 'Rahidin,sp', 'Karyawan Swasta', 'Jl. Bukit Raya 4 No.97 RT002/001 Baamang Barat Baamang ', 'Kotawaringin Timur', '', '082254601885', 'restyannisya3103@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017041', 'Dinar Ali', 'R', 'Pemalang', '1998-01-22', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '', 'Islam', 'O', '155', 'SMAN 2 Pemalang', 'IPA', '2017', '0', '0', 'Kasmali', 'Wiraswasta', 'Jl. Narasoma RT02/09 Taman Pemalang ', 'Pemalang', '', '085602986924', 'madakusuma.mk@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017042', 'Ferry Aprilia Rizki Irawan', 'R', 'Tabalong', '1998-04-13', 'L', '2017', '20171', '20242', '15', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '20170812093127.jpg', 'Islam', 'O', '163', 'SMAN 1 Muara Uya', 'IPA', '', '0', '0', 'H. Mulkah A', 'Wiraswasta', 'Jl.Muara Uya No.101 RT001 Muara Uya Muara Uya', 'Tabalong', '', '081251819025', 'irawanzx07@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017043', 'Finandia Laras Saputri', 'R', 'Semarang', '1998-11-19', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '', 'Islam', 'O', '167', 'SMAN 11 Semarang', 'IPA', '2017', '0', '0', 'Ngadino', 'Wiraswasta', 'Jl Majapahit No.238 Kalicari Pedurungan', 'Semarang', '', '081329533690', 'finandialaraskartaatmaja@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017044', 'Briliana Ikrimazahra', 'R', 'Pemalang', '2001-01-29', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '20170906110343.jpg', 'Islam', 'B', '158', 'SMA Takhassus Al Quran', 'IPA', '2017', '0', '0', 'Winarto', 'PNS', 'Perum. Taman Lestari Blok C No.2 Taman Taman', 'Pemalang', '', '0895381176016', 'falasifahmalisa@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017045', 'Ira Naca Gistina Saputri ', 'R', 'Trenggalek ', '1997-08-25', 'P', '2017', '20171', '20242', '05', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '', 'Islam', 'O', '150', 'SMA Assalaam Sukoharjo', 'IPA', '2015', '0', '0', 'Sutamsir ', 'Wiraswasta', 'Watulimo RT10/03 Watulimo ', 'Trenggalek', '-', '081331307616', 'ira.naca6@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017046', 'Raissa Ridha Rahmandhiya', 'R', 'Semarang', '1997-12-05', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '20170906110548.jpg', 'Islam', 'O', '155', 'SMAN 1 Semarang', 'IPA', '2015', '0', '0', 'Lukito', 'PNS', 'Jl.Arjuna 1 No.79 RT01/01 Semarang Tengah', 'Semarang', '0243551947', '085802209228', 'raissaridha89@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017047', 'Kamilatusyar`iyah', 'R', 'Mataram', '1994-07-28', 'P', '2017', '20171', '20242', '23', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '', 'Islam', 'B', '154', 'SMAN 1 Kota Bima', 'IPA', '2017', '0', '0', 'Abubakar M Yakub', 'PNS', 'Jl. Jenderal Sudirman RT007/003 Rabangodu Raba', 'Bima', '-', '081238141041', 'kamilatusyariah@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017048', 'Aulia Rizqi Niami', 'R', 'Tegal', '2000-05-17', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '', 'Islam', 'O', '156', 'SMAN 3 Slawi', 'IPA', '2017', '0', '0', 'Aminudin ', 'PNS', 'Jl. Taman Sirna Raga RT01/03 Pagerbarang Pagerbarang', 'Tegal', '', '081804684475', 'niamiaulia@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017049', 'Arkhamatul Wafiroh', 'R', 'Grobogan', '1999-07-18', 'P', '2017', '20171', '20242', '03', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '20170912020223.jpg', 'Islam', '-', '148', 'MA Futuhiyyah 2 Demak', 'IPA', '2017', '0', '0', 'Muh Zaini', 'Karyawan Swasta', 'tungu,rt 05/ rw 01 godong ', 'Grobogan', '', '085642034125', 'arkhamatulwafiroh2@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A017050', 'Idzhar Qolby Fatichin', 'R', 'Salatiga ', '1998-04-18', 'L', '2017', '20171', '20242', '18', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '990', '', 'Islam', 'A', '168', 'Pondok Modern Darussalam Gontor', 'IPA', '2017', '0', '0', 'Priyono', 'Lainnya', 'Jl Suprapto No 7 lrng pekuburan ', 'Kendari', '', '085290039242', 'idhar690alfatih@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('61026', 'C', '12201', 'J2A017051', 'Shinta Alfiani Shabrina', 'R', 'Brebes', '1999-11-10', 'P', '2017', '20171', '20242', '3', '2017-09-05', '0000-00-00', 'A', 'B', '', '', '', '', '', '639', '20170926082736.jpg', 'Islam', 'A', '162', 'SMAN 1 Brebes', 'IPA', '2017', '0', '0', 'Azminurdin Latief', 'Pns', 'jl kyai kholid barat rt 05 rw 10  pasar batang brebes ', 'Brebes', '2836176904', '85870657552', 'shintaalfiani12@gmail.com', 'Guru Smu/smk', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018001', 'Nurul Ikhrimah', 'R', 'Subang', '2000-10-23', 'P', '2018', '20181', '20252', '02', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180223101510.jpg', 'Islam', 'A', '155', 'SMA 1 Subang', 'IPA', '2018', '0', '0', 'H. Undang Kundang, S.km', 'PNS', 'Jl. Duren 1 No.9 Bblok 5 Perumnas RT35/15 Sukamelang Subang ', 'Subang', '0260414336', '082217151585', 'nurulikhrimah@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018002', 'Faradilla Ainun Zahra', 'R', 'Semarang', '2000-07-02', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180223021123.jpg', 'Islam', 'O', '158', 'SMA Islam Hidayatullah', 'IPA', '2018', '0', '0', 'Mockhamad Akris Zunaidi', 'Karyawan Swasta', 'Perum Polri Durenan Indah Blok Gg 08 Mangunharjo Tembalang ', 'Semarang 50272', '02476580677', '082133465401', 'faradillaaz123@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018003', 'Tsania Nahda Zanuba', 'R', 'Batang', '1999-11-16', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180426113705.jpg', 'Islam', 'O', '164', 'SMA 3 Pekalongan', 'IPA', '2018', '0', '0', 'Moch. Mirza', 'Wiraswasta', 'Jl. RE Martadinata RT02/01 Gg.Bandeng Klidang Wetan 51226', 'Batang', '', '081548027701', 'tsanianahda.zn@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018004', 'Tania Putri Ratnasari', 'R', 'Depok', '2000-01-03', 'P', '2018', '20181', '20252', '02', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180509105927.jpg', 'Islam', 'O', '160', 'SMA 9 Depok', 'IPA', '2018', '0', '0', 'Suratno', 'PNS', 'Jl. H. Lihun No.34c RT06/01 Krukut Limo ', 'Depok', '', '081383391622', 'taniaputriratnasari@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018005', 'Naufan Hafy Fauzi', 'R', 'Semarang', '1999-11-29', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180226031553.jpg', 'Islam', 'A', '165', 'SMA 5 Semarang', 'IPA', '2018', '0', '0', 'Hendro Cahyadianto', 'PNS', 'Jl. Proton IV Blok AB V/12 RT004/014 Bringin Ngaliyan', 'Semarang', '', '08112881185', 'naufancars@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018006', 'Fitri Aniowati', 'R', 'Pati', '2002-02-12', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'K', 'B', '', '', '', '', '', '611', '20180302115932.jpg', 'Islam', '', '148', 'SMA 1 Pati', 'IPA', '2018', '0', '0', 'Imam Fitrianto', 'PNS', 'Purwosari RT03/01 Tlogowungu 59161', 'Pati', '', '085642812088', 'faniowati@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018007', 'Fidela Matta Nydia ', 'R', 'Grobogan', '2000-05-31', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '', 'Islam', 'O', '160', 'SMA 1 Purwodadi', 'IPA', '2018', '0', '0', 'Edy Mulyono', 'Karyawan Swasta', 'Jl. Purwodadi-Solo Km. 06 Rt 09/Rw 011 Toroh ', 'Purwodado', '-', '082314621897', 'dela.nydia@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', '', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018008', 'Mega Dwi Setyaningrum', 'R', 'Pati', '2000-04-03', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180315084511.jpg', 'Islam', 'B', '153', 'SMA 1 Jakenan', 'IPA', '2018', '0', '0', 'Sukarmin', 'Wiraswasta', 'Serutsadang RT05/02 Winong 59181', 'Pati', '081326925873', '085226925165', 'megadwis.19md@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018009', 'Aura Syibil Azzahriyah', 'R', 'Pangkalpinang', '2000-08-08', 'P', '2018', '20181', '20252', '30', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '', 'Islam', 'O', '155', 'SMA 1 Koba', 'IPA', '2018', '0', '0', 'Wahyu Nurrakhman', 'PNS', 'Jl. Sukarno Hatta Gg Arrahman RT08 No.15 Arung Dalam Koba 33181', 'Bangka Tengah', '-', '082278385112', 'aurasyibil08@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', '', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018010', 'Mohammad Dwiky Priambodo', 'R', 'Semarang', '2000-04-28', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '611', '20180316025013.jpg', 'Islam', '-', '172', 'SMA 1 Kendal', 'IPA', '2018', '0', '0', 'H. R. Jhony Pujono Wiwoho', 'PNS', 'Tosari RT01/01 Brangsong 51371', 'Kendal', '', '081229944334', 'dwikyuky28@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018011', 'Tria Kusuma Wardani', 'R', 'Kab. Semarang', '2000-08-31', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180316030418.jpg', 'Islam', 'O', '157', 'SMA 1 Ungaran', 'IPA', '2018', '0', '0', 'Hadi Sunoto, SH, MH', 'PNS', 'Jl. Jatiraya 1 No.4 RT001/001 Kalirejo Ungaran Timur 50511', 'Semarang', '082135918527', '081226228242', 'triakusumawardani08@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'Permasi');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018012', 'Ika Puji Lestari', 'R', 'Kendal', '1999-04-28', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180607023757.jpg', 'Islam', 'B', '156', 'SMA 1 Weleri', 'IPA', '2017', '0', '0', 'Subarjo', 'Pensiunan', 'jatipurwo rt002 / rw001 kec. rowosari', 'Kendal', '', '082227008746', 'ekapuji99.ep@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018013', 'Carissa Firdaus', 'R', 'Semarang', '2000-09-15', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'K', 'B', '', '', '', '', '', '987', '20180607030521.jpg', 'Islam', 'B', '158', 'SMA 11 Semarang', 'IPA', '2018', '0', '0', 'Widiyarto, S.sos', 'Swasta', 'jl. plamongan indah blok i.9/no. 11 rt 08/rw 31 batursari mranggen', 'Demak', '', '081578009928', 'firda.carissa9@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018014', 'Aini Ulin Na`mah', 'R', 'Kudus', '2000-02-24', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180607032759.jpg', 'Islam', '-', '152', 'SMAN 2 KUDUS', 'IPA', '2018', '0', '0', 'Masrian', 'Pns', 'tanjung karang rt 02 rw 02 jati ', 'Kudus', '', '08122501486', 'ainiulin36@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018015', 'Anissa Sukmawardani', 'R', 'Pati', '2000-01-18', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180608091358.jpg', 'Islam', 'O', '152', 'SMAN 1 JAKENAN', 'IPA', '2018', '0', '0', 'Satmoko', 'Swasta', 'ds arumanis rt 004 rw 005 kec jaken', 'PATI', '', '085225722329', 'anisasukmawardani818@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018016', 'Noor Dyah Permata Sari', 'R', 'Pati', '2000-05-17', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180608094632.jpg', 'Islam', 'Ab', '153', 'SMA 1 Bae', 'IPA', '2018', '0', '0', 'Kambari', 'Swasta', 'kaliampo rt 3/rw 1   wangunrejo  margorejo ', 'Pati', '', '08156520208', 'nidadyah17@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018018', 'Luthfan Asyafiq', 'R', 'Kebumen', '1998-12-23', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180611100946.jpg', 'Islam', 'A', '174', 'SMA 2 PURBALINGGA', 'IPA', '2018', '0', '0', 'Cipto Mudiyono', 'Pns', 'desa jingkang rt 08 rw 03 kec. karangjambu', 'PURBALINGGA', '', '082243508846', 'luthfanasyafiq13@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'L3', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018019', 'Divi Talitha Olivia Sandra', 'R', 'Semarang', '2001-01-19', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180611121742.jpg', 'Islam', 'O', '163', 'SMA 15 Semarang', 'IPA', '2018', '0', '0', 'Budi Wasono (alm.)', 'Swasta', 'sumber barat rt.09/rw.01, sumberjosari,  karangrayung', 'Semarang', '', '085743663092', 'divitalitha.os@gmail.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018020', 'Rika Cahya Melinia', 'R', 'Surabaya', '2000-07-11', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180612110239.jpg', 'Islam', 'O', '160', 'SMA 2 Semarang', 'IPA', '2018', '0', '0', 'Abdul Rozaq S.pd', 'Swasta', 'jl. fatmawati no. 1b dukuh gendong rt 05 rw 03 kel. mangunharjo kec. tembalang ', 'Semarang', '02476580860', '089604733227', 'rikacahya.melinia@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018021', 'Talitha Avisa Zahra', 'R', 'Wonosobo', '2000-01-14', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '987', '20180625104310.jpg', 'Islam', 'A', '157', 'SMA Islam Hidayatullah Semarang', 'IPA', '2018', '0', '0', 'H. Abdul Wahib Amsa, SH.MH', 'Pns', 'Jl. Setuk rt.01 rw.04 Pudak Payung Banyumanik ', 'Semarang', '', '082138950253', 'talithaavisaa@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018023', 'Azalia Nurisma', 'R', 'Bandung', '2000-07-14', 'P', '2018', '20181', '20252', '01', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180725042401.jpg', 'Islam', 'B', '162', 'SMA Al-Masoem', 'IPA', '2018', '0', '0', 'Nursaman', 'Wiraswasta', 'Blok X Gg.IV Semper Barat Cilincing ', 'Jakarta Utara', '', '081314552955', 'araki724@yahoo.com', 'Mitra/teman', '0.00', '', '', '', '', 'L3', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018024', 'Nadia Hakul Ulya', 'R', 'Grobogan', '1998-11-26', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180801033324.jpg', 'Islam', 'B', '162', 'SMA 1 Gubug', 'IPA', '2016', '0', '0', 'H. Faizin (alm)', 'Wiraswasta', 'Mijen RT05/01 Kebonagung 59573', 'Demak', '', '082241705795', 'nadiaulya26@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018025', 'Rosa Jinandya', 'R', 'Semarang', '1999-06-12', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180802111036.jpg', 'Islam', '-', '155', 'SMA Kesatrian 1 Semarang', 'IPA', '2018', '0', '0', 'R. Sapto Widodo', 'PNS', 'Jl. WR. Supratman Kav.45 RT01/01 Manyaran Semarang Barat 50147', 'Semarang', '(024)7602048', '085878888042', 'jinandya.rosa.jinandya@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018026', 'Reza Purwa Aji Setiya', 'R', 'Kab.semarang', '2000-03-21', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180802111546.jpg', 'Islam', 'B', '172', 'SMA 3 Salatiga', 'IPA', '2018', '0', '0', 'Sulistiyono', 'Wiraswasta', 'Krajan rt04 rw01 Purworejo Suruh', 'Salatiga', '', '085786957170', 'ajireza67@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018027', 'Alfitra Miranti', 'R', 'Jakarta', '2000-12-29', 'P', '2018', '20181', '20252', '02', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180802034725.jpg', 'Islam', 'B', '156', 'SMA 14 BEKASI', 'IPA', '2018', '0', '0', 'Wardaya', 'Swasta', 'Kavling Taman Wisata,Taman Duta blok d20/7 rt 004 rw 039 Bahagia Babelan', 'Bekasi', '', '081776322682', 'alfitramiranti92@gmail.com', 'Mitra/teman', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018028', 'Sania Astantia Sekar Handayani', 'R', 'Semarang', '1999-11-13', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180803093032.jpg', 'Islam', 'O', '168', 'SMA 6 Semarang', 'IPA', '2017', '0', '0', 'Nurtantio Sony Putro', 'Swasta', 'jl. mega raya 3 no 274 perum koveri rt/rw 006/007 Ngaliyan', 'Kota Semarang', '0247628198', '081327662221', 'saniasekar13@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018029', 'Zirran Dhashytha Elzhar', 'R', 'Kab. Sanggau', '2001-01-11', 'P', '2018', '20181', '20252', '13', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180803103847.jpg', 'Islam', 'O', '159', 'SMA 2 SANGGAU', 'IPA', '2018', '0', '0', 'Dedy Yunizar', 'Wiraswasta', 'jl. h. agus salim no. 78 rt/rw 006/002 Kapuas', 'SANGGAU', '', '082251505734', 'zirrandhashythaelzhar@yahoo.co.id', 'Lainnya', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018030', 'Muhammad Vegisa Nusantara', 'R', 'Kebumen', '1998-11-17', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180803022602.jpg', 'Islam', 'A', '173', 'SMA 1 Gombong', 'IPA', '2016', '0', '0', 'Parjuli', 'Pns', 'puring wetan, rt/rw 004/001, sitiadi puring,', 'Kebumen', '', '082310463902', 'nvegisa@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018031', 'Sandra Mooney', 'R', 'Depok', '2000-08-15', 'P', '2018', '20181', '20252', '02', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180803032327.jpg', 'Islam', 'O', '155', 'SMA 1 CIAMIS', 'IPA', '2018', '0', '0', 'Sunarsono', 'Swasta', 'Warung Wetan no 120 rt 5 rw 3 Imbanagara Ciamis', 'Ciamis', '02652752224', '082240952715', 'sandraramny@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018032', 'Rasyida Reika Dewinta', 'R', 'Kudus', '2000-04-01', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1028', '20180803033130.jpg', 'Islam', 'B', '156', 'MA MUHAMMADIYAH KUDUS', 'IPA', '2018', '0', '0', 'Bambang Sulistiono', 'Pns', 'Kalipucang Wetan rt 01 rw 01 Welahan', 'Jepara', '', '085217262040', 'dewinta.reika@gmail.com', 'Lainnya', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018033', 'Indah Purnamasari', 'R', 'Kudus', '2000-10-02', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180803033839.jpg', 'Islam', 'B', '150', 'SMA 1 BAE Kudus', 'IPA', '2018', '0', '0', 'Maskuri,sh', 'Lainnya', 'Samirejo rt/rw 005/002 Dawe', 'Kudus', '', '085325380502', 'indahsari1736@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018034', 'Isnaini Lailatul Adha Mujiningtyas', 'R', 'Kudus', '2000-03-13', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180803035446.jpg', 'Islam', '-', '162', 'SMA 1 KUDUS', 'IPA', '2018', '0', '0', 'Mujiman', 'Wiraswasta', 'Pecinan  Bulungcangkring rt 01 rw 01 Jekulo ', 'Kudus', '', '081390812473', 'isnainimujiningtyas@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018035', 'Farida Azzah Khoirunnisa', 'R', 'Kebumen', '2000-03-13', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180804094541.jpg', 'Islam', 'A', '159', 'SMA 1 GOMBONG', 'IPA', '2018', '0', '0', 'Sudiono', 'Swasta', 'Mergosono rt/rw 001/005,Buayan', 'Kebumen', '', '082242892683', 'farida.azzahk@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018036', 'Rizki Ninik Lestari', 'R', 'Pati', '1998-10-04', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180804114415.jpg', 'Islam', 'A', '149', 'SMA 2 Rembang', 'IPA', '2017', '0', '0', 'Supartono', 'Wiraswasta', 'trimulyo rt.4/rw.4 juwana', 'Pati', '', '085225497025', 'rizki.ninikl@yahoo.com', 'Media Elektronik', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018037', 'Anisya Febiola Ababelia', 'R', 'Alas', '2000-01-01', 'P', '2018', '20181', '20252', '23', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180806093625.jpg', 'Islam', '-', '153', 'MA 2 Mataram', 'IPA', '2018', '0', '0', 'Ir.h.syamsul Rijal', 'Karyawan Swasta', 'Stowe Berang RT01/08 Luar Alas 84353', 'Sumbawa', '', '085921389402', 'icafebi828@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018038', 'Nadya Zhafira Asfihani', 'R', 'Depok', '2000-10-05', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180806013453.jpg', 'Islam', 'O', '155', 'SMA Darul Ulum 3 Peterongan Jombang', 'IPA', '2018', '0', '0', 'Zaenal Arifin', 'PNS', 'Pamijen Tengah RT01/03 Pamijen Bumiayu 52273', 'Brebes', '02834533130', '082323883773', 'nadyazaw123@yahoo.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018039', 'Alzahra Nadiva Zein', 'R', 'Bandung', '2018-01-29', 'P', '2018', '20181', '20252', '02', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180806014822.jpg', 'Islam', 'A', '160', 'SMA 1 Bandung', 'IPA', '2018', '0', '0', 'Edy Junaedi', 'Pns', 'jl. sarijadi blok 6 no 89 rt/rw 008/003 sarijadi sukasari 40151', 'Bandung', '', '087875932332', 'achaalzahra@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018040', 'Muhammad Malik Bahrain', 'R', 'Sidoarjo', '2000-02-02', 'L', '2018', '20181', '20252', '05', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180806034641.jpg', 'Islam', 'Ab', '175', 'SMA Insan Cendekia', 'IPA', '2018', '0', '0', 'Mulyono', 'Karyawan Swasta', 'Jl Jeruk 2/C No.33 RT17/2 Geluran Taman ', 'Sidoarjo', '', '081235128428', 'malikbahrain41@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018041', 'Krisna Bayu Gumelar Santoso', 'R', 'Pemalang', '1999-02-28', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180806035738.jpg', 'Islam', '-', '171', 'SMA 2 Pemalang', 'IPA', '2017', '0', '0', 'Joko Santoso', 'PNS', 'Belik Krajan Wetan RT09/07 Belik', 'Pemalang', '087871718988', '087764867396', 'kbayugs@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018042', 'Mohamad Himawan Mahendra Pradana', 'R', 'Klaten', '1997-11-08', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '1032', '20180806042344.jpg', 'Islam', '-', '173', 'SMA 1 Karanganom', 'IPA', '2015', '0', '0', 'Jaka Purwanto', 'PNS', 'Platen RT02/01 Sawahan Juwiring 57472', 'Klaten', '', '085700399708', 'indrabima08@gmail.com', 'Surat Kabar', '0.00', '', '', '', '', 'L3', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018043', 'Rizqika Rahmawati', 'R', 'Rembang', '1997-09-05', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180807095534.jpg', 'Islam', 'O', '158', 'SMA 1 Lasem', 'IPA', '2015', '0', '0', 'Yatiyono', 'Pensiunan', 'Soditan RT06 /03 No.67 Lasem 59271', 'Rembang', '', '081226357444', 'rizqykarahma@yahoo.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018044', 'Natasya Widya Apsari', 'R', 'Semarang', '2000-03-03', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180808023446.jpg', 'Islam', 'O', '160', 'SMA 15 Semarang', 'IPA', '2018', '0', '0', 'Yurahardono', 'Wiraswasta', 'Padepokan Ganesa 1 Blok C No.3 RT02/09 Pandean Lamper Gayamsari 50167', 'Semarang', '0246716392', '083838612465', 'natasyawidyaap@gmail.com', 'Lainnya', '0.00', '', '', '', '', 'L3', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018045', 'Afrizal Faby Firmansyah', 'R', 'Demak', '2001-02-05', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180814114717.jpg', 'Islam', '-', '172', 'SMA 1 Gubug', 'IPA', '2018', '0', '0', 'Edy Mulyantomo.se.mm.', 'Dosen', 'Sidorejo RT03/04 Karangawen 59566', 'Demak', '02476591349', '081575123324', 'fabyspensate@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L5', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018046', 'Amrina Rosyada', 'R', 'Cilacap', '2001-01-20', 'P', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180815021009.jpg', 'Islam', 'B', '155', 'SMA 1 Cilacap', 'IPA', '2018', '0', '0', 'Sapto Sukoyono', 'PNS', 'Jl. Sirkaya No.246 RT04/07 Tambakreja Cilacap Selatan 53213', 'Cilacap', '', '085600212426', 'ramrina02@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018047', 'Galuh Almira', 'R', 'Jakarta', '1999-06-09', 'P', '2018', '20181', '20252', '01', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180903114724.jpg', 'Islam', 'O', '157', 'SMA 73 Jakarta', 'IPA', '2017', '0', '0', 'Andi Purwanto', 'Swasta', 'Jl. Kalibaru Timur III D No 37 Rt 06 Rw 02 Cilincing kec. cilincing ', 'Jakarta Utara', '082293355513', '081316961177', 'galuhalmira27@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018048', 'Zakiyya Laila Nur Aziza ', 'R', 'Sleman', '1999-09-03', 'P', '2018', '20181', '20252', '04', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180903115521.jpg', 'Islam', 'A', '165', 'SMA 1 Ngaglik', 'IPA', '2018', '0', '0', 'Isnan Noor Wasith', 'PNS', 'Sebayu RT04/01 Triharjo Sleman ', 'Sleman', '-', '085743151827', 'lilaaziza112@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018049', 'Andrea Okta Wijayanti', 'R', 'Kobar', '2000-10-20', 'P', '2018', '20181', '20252', '14', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180904115304.jpg', 'Islam', 'O', '150', 'SMA 1 Sukorejo', 'IPA', '2018', '0', '0', 'Suyaman', 'Wiraswasta', 'Bukit Raya RT/RW 01/03 Menthobiraya 74668', 'Lamandau', '', '081225319814', 'okta.wijayanti59@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'S', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018050', 'Faisal Ardi', 'R', 'Demak', '1998-01-25', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180904023743.jpg', 'Islam', 'O', '167', 'SMA 5 Semarang', 'IPA', '2015', '0', '0', 'Mahfudl', 'Wiraswasta', 'Bulusari RT 001 RW 002 Sayung 59563', 'Demak', '', '082292465415', 'faisalardii@gmail.com', 'Brosur/leaflet/poster', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018051', 'Muhammad Hendry Alkahfi', 'R', 'Banjarmasin', '2000-09-12', 'L', '2018', '20181', '20252', '16', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180905114935.jpg', 'Islam', 'O', '163', 'MA Tebuireng', 'IPA', '2018', '0', '0', 'Jurisa Fahrozi', 'PNS', 'Jl. Sultan Ibrahim Khaliluddin RT05/11 Tanah Grogot Tanah Grogot', 'Paser', '', '082256269035', 'alkahfihendry0900@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'L', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018052', 'Karnelia Primadewi', 'R', 'Samarinda', '2000-09-19', 'P', '2018', '20181', '20252', '16', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180906090150.jpg', 'Islam', 'A', '155', 'SMA 1 Samarinda', 'IPA', '2018', '0', '0', 'Karmijo', 'PNS', 'Jl. P. Suryanata Perum. Bukit Pinang Blok K No.6 RT10 Bukit Pinang Samarinda Ulu 75124', 'Samarinda', '0541-290217', '082158626848', 'karneliaprima@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018053', 'Syahda Apriliya Rizki', 'R', 'Pontianak', '2001-04-28', 'P', '2018', '20181', '20252', '13', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180906093200.jpg', 'Islam', 'O', '160', 'SMA 3 Pontianak', 'IPA', '2018', '0', '0', 'Dedy Syahriyanto', 'PNS', 'Jl. Gst Situt Machmud Gg. Slt. Maluku RT04/05 Siantan Hulu Pontianak Utara', 'Pontianak', '-', '082350662375', 'syahdaapriliyarizki@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'M', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018054', 'Sovia Kurnia Sari', 'R', 'Musi Rawas', '1998-10-15', 'P', '2018', '20181', '20252', '11', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180906024147.jpg', 'Islam', 'O', '160', 'MAN 1 (MODEL)', 'IPA', '2018', '0', '0', 'Sukimin', 'Wiraswasta', 'blok b 0 dsn sukoharjo rt/rw 001/003 bumi makmur nibung', 'Lubuklinggau', '-', '081274633429', 'sovia.k.sari@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');
INSERT INTO `msmhs` VALUES ('061026', 'C', '12201', 'J2A018055', 'Noviaji Bagas Imam Prakasa', 'R', 'Grobogan', '1998-11-30', 'L', '2018', '20181', '20252', '03', '2018-09-10', '0000-00-00', 'A', 'B', '', '', '', '', '', '769', '20180906031956.jpg', 'Islam', 'A', '167', 'SMA 1 Purwodadi', 'IPA', '2018', '0', '0', 'Moh Hajirin', 'PNS', 'Jl Getas Pendowo RT01/07 Kuripan Purwodadi 58112', 'Grobogan', '', '081390778660', 'noviajibagas@gmail.com', 'Orang Tua/saudara', '0.00', '', '', '', '', 'XL', 'UTS');

-- ----------------------------
-- Table structure for `obat_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `obat_pemeriksaan`;
CREATE TABLE `obat_pemeriksaan` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `kode_obat` varchar(10) NOT NULL,
  `qty` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of obat_pemeriksaan
-- ----------------------------
INSERT INTO `obat_pemeriksaan` VALUES ('1', '1', '7788', '3');
INSERT INTO `obat_pemeriksaan` VALUES ('2', '1', '46836', '1');
INSERT INTO `obat_pemeriksaan` VALUES ('3', '4', '7788', '3');
INSERT INTO `obat_pemeriksaan` VALUES ('4', '4', '46836', '4');
INSERT INTO `obat_pemeriksaan` VALUES ('5', '5', '7788', '2');
INSERT INTO `obat_pemeriksaan` VALUES ('6', '5', '46836', '3');

-- ----------------------------
-- Table structure for `pembelian`
-- ----------------------------
DROP TABLE IF EXISTS `pembelian`;
CREATE TABLE `pembelian` (
  `id` int(15) NOT NULL,
  `faktur_beli` varchar(25) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `kode_supplier` int(3) NOT NULL,
  `total_beli` int(10) NOT NULL,
  PRIMARY KEY (`id`,`faktur_beli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pembelian
-- ----------------------------

-- ----------------------------
-- Table structure for `pemeriksaan_awal`
-- ----------------------------
DROP TABLE IF EXISTS `pemeriksaan_awal`;
CREATE TABLE `pemeriksaan_awal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `keluhan` text NOT NULL,
  `berat` int(3) NOT NULL,
  `tinggi` int(3) NOT NULL,
  `tensi` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pemeriksaan_awal
-- ----------------------------

-- ----------------------------
-- Table structure for `plot_dpjp`
-- ----------------------------
DROP TABLE IF EXISTS `plot_dpjp`;
CREATE TABLE `plot_dpjp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `dpjp` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `slot` int(2) NOT NULL,
  `status` int(1) NOT NULL COMMENT '0:non aktif;1:aktif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of plot_dpjp
-- ----------------------------
INSERT INTO `plot_dpjp` VALUES ('1', '9888', '2019-01-18', '5', '1');

-- ----------------------------
-- Table structure for `plot_mahasiswa_dpjp`
-- ----------------------------
DROP TABLE IF EXISTS `plot_mahasiswa_dpjp`;
CREATE TABLE `plot_mahasiswa_dpjp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_plot` int(5) NOT NULL,
  `nim` varchar(15) NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of plot_mahasiswa_dpjp
-- ----------------------------

-- ----------------------------
-- Table structure for `radiografi_tindakan`
-- ----------------------------
DROP TABLE IF EXISTS `radiografi_tindakan`;
CREATE TABLE `radiografi_tindakan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `status_kirim` int(1) NOT NULL COMMENT '1:dokter;2:pasien;3:dua2nya',
  `tanggal_jadi` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of radiografi_tindakan
-- ----------------------------

-- ----------------------------
-- Table structure for `radiografi_tindakan_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `radiografi_tindakan_gigi`;
CREATE TABLE `radiografi_tindakan_gigi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `nomor_gigi` int(2) NOT NULL,
  `tindakan` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of radiografi_tindakan_gigi
-- ----------------------------

-- ----------------------------
-- Table structure for `rekam_medis`
-- ----------------------------
DROP TABLE IF EXISTS `rekam_medis`;
CREATE TABLE `rekam_medis` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rekam_medis` varchar(15) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_identitas` varchar(25) NOT NULL,
  `tipe_identitas` int(1) NOT NULL,
  `status_nikah` int(1) NOT NULL,
  `pekerjaan` int(1) NOT NULL COMMENT '<option value="1">Tidak Bekerja</option>\r\n<option value="2">PNS</option>\r\n<option value="3">TNI / Polri</option>\r\n<option value="4">Legislatif</option>\r\n<option value="5">BUMN</option>\r\n<option value="6">Swasta</option>\r\n<option value="7">Wiraswasta</option>\r\n<option value="8">Pensiunan</option>\r\n<option value="9">Lainnya</option>',
  `alamat` varchar(30) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `golongan_darah` varchar(2) NOT NULL,
  `file_foto` varchar(50) NOT NULL DEFAULT '-',
  `nama_wali` varchar(30) NOT NULL,
  `hubungan_wali` int(1) NOT NULL,
  `hp_wali` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`rekam_medis`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rekam_medis
-- ----------------------------
INSERT INTO `rekam_medis` VALUES ('1', '000001', 'amrano', 'semarang', '1988-10-14', '33744324654354', '1', '1', '1', 'dolog', '08564032435', '0', 'test.jpeg', 'aminatin', '1', '08564035465435');
INSERT INTO `rekam_medis` VALUES ('2', '000002', 'Ugandi Listanto', 'Semarang', '2018-12-12', '337402133165433', '1', '1', '6', 'Tlogosari', '0856431321', 'A', '-', 'Michael Tjandra', '1', '08564313369');
INSERT INTO `rekam_medis` VALUES ('3', '000003', 'Akhmad Fathurrohmanx', 'Ciamisx', '1998-12-16', '33740362132146549', '2', '1', '2', 'Singosari Rayax', '0818457763', 'AB', '-', 'Sitix', '3', '08564651329');
INSERT INTO `rekam_medis` VALUES ('4', '000004', 'Ahmad Peter Parker', 'Semarang', '2019-01-04', '3374026453521321', '1', '2', '3', 'Tlogosari Semarang', '08654654', '0', '-', 'Wali Band', '1', '08564131235');

-- ----------------------------
-- Table structure for `status_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `status_pemeriksaan`;
CREATE TABLE `status_pemeriksaan` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `status` int(2) NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_pemeriksaan
-- ----------------------------

-- ----------------------------
-- Table structure for `tindakan_detail_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_detail_lab_gigi`;
CREATE TABLE `tindakan_detail_lab_gigi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(5) NOT NULL,
  `detail_tindakan` int(5) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_detail_lab_gigi
-- ----------------------------
INSERT INTO `tindakan_detail_lab_gigi` VALUES ('1', '5', '1', 'lengkap ya');
INSERT INTO `tindakan_detail_lab_gigi` VALUES ('2', '5', '2', 'kalau bisa lengkap');
INSERT INTO `tindakan_detail_lab_gigi` VALUES ('3', '5', '3', 'yang lengkap ya');

-- ----------------------------
-- Table structure for `tindakan_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_lab_gigi`;
CREATE TABLE `tindakan_lab_gigi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(5) NOT NULL,
  `posisi_rahang` int(1) NOT NULL COMMENT '1:atas;2:bawah',
  `warna` text NOT NULL,
  `keterangan` text,
  `waktu_jadi` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_lab_gigi
-- ----------------------------
INSERT INTO `tindakan_lab_gigi` VALUES ('1', '5', '1', 'kuning kecoklatan', 'banyak gudalnya', '2019-01-15 02:48:41');

-- ----------------------------
-- Table structure for `tindakan_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_pemeriksaan`;
CREATE TABLE `tindakan_pemeriksaan` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tindakan` int(10) NOT NULL,
  `persen_biaya` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_pemeriksaan
-- ----------------------------
INSERT INTO `tindakan_pemeriksaan` VALUES ('1', '1', '4', '30');
INSERT INTO `tindakan_pemeriksaan` VALUES ('2', '1', '2', '0');
INSERT INTO `tindakan_pemeriksaan` VALUES ('3', '4', '14', '0');
INSERT INTO `tindakan_pemeriksaan` VALUES ('4', '4', '15', '0');
INSERT INTO `tindakan_pemeriksaan` VALUES ('5', '5', '14', '50');
INSERT INTO `tindakan_pemeriksaan` VALUES ('6', '5', '15', '0');

-- ----------------------------
-- Table structure for `tindakan_pemeriksaan_penunjang`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_pemeriksaan_penunjang`;
CREATE TABLE `tindakan_pemeriksaan_penunjang` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tindakan` int(10) NOT NULL,
  `persen_biaya` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_pemeriksaan_penunjang
-- ----------------------------

-- ----------------------------
-- Table structure for `transaksi_bayar`
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_bayar`;
CREATE TABLE `transaksi_bayar` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `ambil_obat` int(1) NOT NULL,
  `total_tindakan` int(25) NOT NULL,
  `total_obat` int(25) NOT NULL,
  `total_bayar` int(25) NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi_bayar
-- ----------------------------
INSERT INTO `transaksi_bayar` VALUES ('13', '1', '1', '185000', '21000', '206000', '2018-12-19 23:42:12');
INSERT INTO `transaksi_bayar` VALUES ('14', '4', '1', '1600000', '39000', '1639000', '2019-01-04 07:00:06');
INSERT INTO `transaksi_bayar` VALUES ('15', '5', '1', '1200000', '28000', '1228000', '2019-01-04 07:35:45');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '1');
INSERT INTO `user` VALUES ('2', 'fo1', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '5');
INSERT INTO `user` VALUES ('3', 'kasir', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '6');
INSERT INTO `user` VALUES ('4', 'A113', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '2');
INSERT INTO `user` VALUES ('5', 'keuangan1', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '8');
INSERT INTO `user` VALUES ('6', 'apoteker', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '4');
INSERT INTO `user` VALUES ('7', '002', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '6');
INSERT INTO `user` VALUES ('8', 'D433', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '3');
INSERT INTO `user` VALUES ('9', 'perawat', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '2');
INSERT INTO `user` VALUES ('10', 'labgigi', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '10');
INSERT INTO `user` VALUES ('11', '7799', '$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK', '11');
