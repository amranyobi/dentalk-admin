/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-07 13:31:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_tindakan_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan_lab_gigi`;
CREATE TABLE `master_tindakan_lab_gigi` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `tipe` int(2) NOT NULL COMMENT '1:prostho;2:ortho;3:sampel penelitian;4:konservasi',
  `tindakan` varchar(50) NOT NULL,
  `biaya` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan_lab_gigi
-- ----------------------------

-- ----------------------------
-- Table structure for `tindakan_detail_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_detail_lab_gigi`;
CREATE TABLE `tindakan_detail_lab_gigi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(5) NOT NULL,
  `detail_tindakan` int(5) NOT NULL,
  `keterangan` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_detail_lab_gigi
-- ----------------------------

-- ----------------------------
-- Table structure for `tindakan_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_lab_gigi`;
CREATE TABLE `tindakan_lab_gigi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(5) NOT NULL,
  `posisi_rahang` int(1) NOT NULL COMMENT '1:atas;2:bawah',
  `warna` text NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_lab_gigi
-- ----------------------------
