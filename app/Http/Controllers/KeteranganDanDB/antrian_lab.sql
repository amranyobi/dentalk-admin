/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-07 13:29:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `antrian_lab`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_lab`;
CREATE TABLE `antrian_lab` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_lab
-- ----------------------------
INSERT INTO `antrian_lab` VALUES ('1', '3', '2019-01-03', '1', '2019-01-03 11:31:34', '0');

-- ----------------------------
-- Table structure for `antrian_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_lab_gigi`;
CREATE TABLE `antrian_lab_gigi` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_lab_gigi
-- ----------------------------
INSERT INTO `antrian_lab_gigi` VALUES ('1', '3', '2019-01-03', '1', '2019-01-03 11:31:34', '0');

-- ----------------------------
-- Table structure for `antrian_radiografi`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_radiografi`;
CREATE TABLE `antrian_radiografi` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_radiografi
-- ----------------------------
