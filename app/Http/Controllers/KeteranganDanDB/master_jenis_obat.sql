/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-07 14:20:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_jenis_obat`
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_obat`;
CREATE TABLE `master_jenis_obat` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `jenis_obat` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jenis_obat
-- ----------------------------
INSERT INTO `master_jenis_obat` VALUES ('1', 'Obat Bebas');
INSERT INTO `master_jenis_obat` VALUES ('2', 'Obat Keras');
