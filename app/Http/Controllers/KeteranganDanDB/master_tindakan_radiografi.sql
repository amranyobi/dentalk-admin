/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-08 14:19:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_tindakan_radiografi`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan_radiografi`;
CREATE TABLE `master_tindakan_radiografi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `tindakan` text NOT NULL,
  `biaya` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan_radiografi
-- ----------------------------
