-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: simrs
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `datamedis_umum`
--

DROP TABLE IF EXISTS `datamedis_umum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `datamedis_umum` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rekam_medis` varchar(15) NOT NULL,
  `tekanan_darah` int(1) NOT NULL,
  `penyakit_jantung` int(1) NOT NULL,
  `diabetes_melitus` int(1) NOT NULL,
  `hemofilia` int(1) NOT NULL,
  `riwayat_asma` int(1) NOT NULL,
  `hepatitis` int(1) NOT NULL,
  `epilepsy` int(1) NOT NULL,
  `gastritis` int(1) NOT NULL,
  `asma` int(1) NOT NULL,
  `tbc` int(1) NOT NULL,
  `penyakit_lain` int(1) NOT NULL,
  `obatan` int(1) NOT NULL,
  `alergi_makanan` int(1) NOT NULL,
  PRIMARY KEY (`id`,`rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datamedis_umum`
--

LOCK TABLES `datamedis_umum` WRITE;
/*!40000 ALTER TABLE `datamedis_umum` DISABLE KEYS */;
/*!40000 ALTER TABLE `datamedis_umum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detail_beli`
--

DROP TABLE IF EXISTS `detail_beli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail_beli` (
  `id` int(15) NOT NULL,
  `id_beli` varchar(25) NOT NULL,
  `faktur_beli` varchar(25) NOT NULL,
  `kode_obat` varchar(10) NOT NULL,
  `harga_beli` int(15) NOT NULL,
  `qty` int(10) NOT NULL,
  `total` int(25) NOT NULL,
  `expired` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detail_beli`
--

LOCK TABLES `detail_beli` WRITE;
/*!40000 ALTER TABLE `detail_beli` DISABLE KEYS */;
/*!40000 ALTER TABLE `detail_beli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_dokter`
--

DROP TABLE IF EXISTS `master_dokter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_dokter` (
  `id` int(5) NOT NULL,
  `kode_dokter` varchar(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `gelar` varchar(15) NOT NULL,
  `spesialisasi` int(1) NOT NULL,
  `alamat` varchar(25) NOT NULL,
  `hp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`kode_dokter`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_dokter`
--

LOCK TABLES `master_dokter` WRITE;
/*!40000 ALTER TABLE `master_dokter` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_dokter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_jadwal`
--

DROP TABLE IF EXISTS `master_jadwal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_jadwal` (
  `id` int(10) NOT NULL,
  `kode_jadwal` varchar(10) NOT NULL,
  `kode_dokter` varchar(15) NOT NULL,
  `kode_poli` varchar(15) NOT NULL,
  `hari` int(1) NOT NULL,
  `jam_mulai` varchar(5) NOT NULL,
  `jam_selesai` varchar(5) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_jadwal`
--

LOCK TABLES `master_jadwal` WRITE;
/*!40000 ALTER TABLE `master_jadwal` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_jadwal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_karyawan`
--

DROP TABLE IF EXISTS `master_karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_karyawan` (
  `id` int(5) NOT NULL,
  `kode_karyawan` varchar(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `gelar` varchar(15) NOT NULL,
  `unit` int(1) NOT NULL,
  `alamat` varchar(25) NOT NULL,
  `hp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`kode_karyawan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_karyawan`
--

LOCK TABLES `master_karyawan` WRITE;
/*!40000 ALTER TABLE `master_karyawan` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_obat`
--

DROP TABLE IF EXISTS `master_obat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_obat` (
  `id` int(5) NOT NULL,
  `kode_obat` varchar(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `dosis` int(5) NOT NULL,
  `satuan` int(2) NOT NULL,
  `jenis` int(3) NOT NULL,
  `resep_dokter` int(1) NOT NULL,
  PRIMARY KEY (`id`,`kode_obat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_obat`
--

LOCK TABLES `master_obat` WRITE;
/*!40000 ALTER TABLE `master_obat` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_obat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_poli`
--

DROP TABLE IF EXISTS `master_poli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_poli` (
  `id` int(5) NOT NULL,
  `kode_poli` varchar(15) NOT NULL,
  `nama_poli` varchar(25) NOT NULL,
  `keterangan` varchar(25) NOT NULL,
  PRIMARY KEY (`id`,`kode_poli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_poli`
--

LOCK TABLES `master_poli` WRITE;
/*!40000 ALTER TABLE `master_poli` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_poli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_spesialis`
--

DROP TABLE IF EXISTS `master_spesialis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_spesialis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gelar` varchar(100) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_spesialis`
--

LOCK TABLES `master_spesialis` WRITE;
/*!40000 ALTER TABLE `master_spesialis` DISABLE KEYS */;
INSERT INTO `master_spesialis` VALUES (9,'Sp.AA','Spesialis Anak Anak','Coba EDIT'),(10,'Sp.G','Spesialis Gusi','Gusi anak');
/*!40000 ALTER TABLE `master_spesialis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembelian`
--

DROP TABLE IF EXISTS `pembelian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembelian` (
  `id` int(15) NOT NULL,
  `faktur_beli` varchar(25) NOT NULL,
  `tanggal_beli` date NOT NULL,
  `kode_supplier` int(3) NOT NULL,
  `total_beli` int(10) NOT NULL,
  PRIMARY KEY (`id`,`faktur_beli`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembelian`
--

LOCK TABLES `pembelian` WRITE;
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;
/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rekam_medis`
--

DROP TABLE IF EXISTS `rekam_medis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rekam_medis` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `rekam_medis` varchar(15) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `tempat_lahir` varchar(25) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_identitas` varchar(25) NOT NULL,
  `tipe_identitas` int(1) NOT NULL,
  `status_nikah` int(1) NOT NULL,
  `suku_ras` int(1) NOT NULL,
  `pekerjaan` int(1) NOT NULL,
  `alamat` varchar(30) NOT NULL,
  `hp` varchar(15) NOT NULL,
  `golongan_darah` varchar(2) NOT NULL,
  `file_foto` varchar(50) NOT NULL,
  `nama_wali` varchar(30) NOT NULL,
  `hubungan_wali` int(1) NOT NULL,
  `hp_wali` varchar(15) NOT NULL,
  PRIMARY KEY (`id`,`rekam_medis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rekam_medis`
--

LOCK TABLES `rekam_medis` WRITE;
/*!40000 ALTER TABLE `rekam_medis` DISABLE KEYS */;
/*!40000 ALTER TABLE `rekam_medis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','$2y$10$wq18CsWyZGEGr2st.LNWnOsmRQq.51cFi4ZhLlwfcZqP5DHS5zZdK',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'simrs'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-04  7:56:17
