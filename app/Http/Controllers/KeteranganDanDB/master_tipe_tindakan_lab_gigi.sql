/*
Navicat MySQL Data Transfer

Source Server         : Server RSGM
Source Server Version : 50560
Source Host           : localhost:3306
Source Database       : simrsgm

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-01-29 17:10:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_tipe_tindakan_lab_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `master_tipe_tindakan_lab_gigi`;
CREATE TABLE `master_tipe_tindakan_lab_gigi` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_tipe_tindakan_lab_gigi
-- ----------------------------
INSERT INTO `master_tipe_tindakan_lab_gigi` VALUES ('1', 'PROSTHO');
INSERT INTO `master_tipe_tindakan_lab_gigi` VALUES ('2', 'ORTHO');
INSERT INTO `master_tipe_tindakan_lab_gigi` VALUES ('3', 'SAMPEL PENELITIAN');
INSERT INTO `master_tipe_tindakan_lab_gigi` VALUES ('4', 'KONSERVASI');
