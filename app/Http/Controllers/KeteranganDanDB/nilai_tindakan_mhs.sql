/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-02-08 19:55:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `nilai_tindakan_mhs`
-- ----------------------------
DROP TABLE IF EXISTS `nilai_tindakan_mhs`;
CREATE TABLE `nilai_tindakan_mhs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tindakan` int(11) NOT NULL,
  `nim` varchar(12) NOT NULL,
  `nilai` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of nilai_tindakan_mhs
-- ----------------------------
INSERT INTO `nilai_tindakan_mhs` VALUES ('15', '11', 'J2A016045', '100');
INSERT INTO `nilai_tindakan_mhs` VALUES ('16', '12', 'J2A016045', '80');
