/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-02-08 19:53:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dokter_integrasi`
-- ----------------------------
DROP TABLE IF EXISTS `dokter_integrasi`;
CREATE TABLE `dokter_integrasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `dokter_integrasi` varchar(15) NOT NULL,
  `id_plot_mahasiswa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dokter_integrasi
-- ----------------------------
INSERT INTO `dokter_integrasi` VALUES ('1', '7', 'J2A016011', '15');
INSERT INTO `dokter_integrasi` VALUES ('2', '8', 'J2A016045', '16');
INSERT INTO `dokter_integrasi` VALUES ('3', '9', 'J2A016045', '17');
INSERT INTO `dokter_integrasi` VALUES ('5', '10', 'J2A016045', '17');
