/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-07 10:40:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_tindakan_radiografi`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan_radiografi`;
CREATE TABLE `master_tindakan_radiografi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `tindakan` text NOT NULL,
  `biaya` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan_radiografi
-- ----------------------------

-- ----------------------------
-- Table structure for `radiografi_tindakan`
-- ----------------------------
DROP TABLE IF EXISTS `radiografi_tindakan`;
CREATE TABLE `radiografi_tindakan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `tindakan` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of radiografi_tindakan
-- ----------------------------

-- ----------------------------
-- Table structure for `radiografi_tindakan_gigi`
-- ----------------------------
DROP TABLE IF EXISTS `radiografi_tindakan_gigi`;
CREATE TABLE `radiografi_tindakan_gigi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `nomor_gigi` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of radiografi_tindakan_gigi
-- ----------------------------
