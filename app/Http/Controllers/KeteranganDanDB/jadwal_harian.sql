/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-07 15:42:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `jadwal_harian`
-- ----------------------------
DROP TABLE IF EXISTS `jadwal_harian`;
CREATE TABLE `jadwal_harian` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode_jadwal` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `validasi` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jadwal_harian
-- ----------------------------
