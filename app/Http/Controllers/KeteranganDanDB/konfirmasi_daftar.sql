/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-19 13:53:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `konfirmasi_daftar`
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_daftar`;
CREATE TABLE `konfirmasi_daftar` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `email` varchar(25) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_daftar
-- ----------------------------
