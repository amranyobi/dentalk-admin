/*
Navicat MySQL Data Transfer

Source Server         : Server RSGM
Source Server Version : 50560
Source Host           : localhost:3306
Source Database       : simrsgm

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-02-09 07:26:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `odontogram`;
CREATE TABLE `odontogram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rekam_medis` varchar(15) NOT NULL,
  `waktu` datetime NOT NULL,
  `kode_dokter` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of odontogram
-- ----------------------------
