/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-22 13:18:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `diagnosa_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `diagnosa_pemeriksaan`;
CREATE TABLE `diagnosa_pemeriksaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `id_diagnosa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of diagnosa_pemeriksaan
-- ----------------------------
