/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-22 13:08:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `pemeriksaan_awal`
-- ----------------------------
DROP TABLE IF EXISTS `pemeriksaan_awal`;
CREATE TABLE `pemeriksaan_awal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(11) NOT NULL,
  `keluhan` text NOT NULL,
  `berat` int(3) NOT NULL,
  `tinggi` int(3) NOT NULL,
  `tensi` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pemeriksaan_awal
-- ----------------------------
