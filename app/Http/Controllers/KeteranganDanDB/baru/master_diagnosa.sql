/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-22 11:11:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_diagnosa`
-- ----------------------------
DROP TABLE IF EXISTS `master_diagnosa`;
CREATE TABLE `master_diagnosa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `poli` varchar(10) NOT NULL,
  `diagnosa` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_diagnosa
-- ----------------------------
