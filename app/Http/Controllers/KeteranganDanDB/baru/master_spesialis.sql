/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-21 10:56:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_spesialis`
-- ----------------------------
DROP TABLE IF EXISTS `master_spesialis`;
CREATE TABLE `master_spesialis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gelar` varchar(100) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_spesialis
-- ----------------------------
INSERT INTO `master_spesialis` VALUES ('9', 'Sp.AA', 'Spesialis Anak Anak', '-');
INSERT INTO `master_spesialis` VALUES ('10', 'Sp.G', 'Spesialis Gusi', 'Gusi anak');
INSERT INTO `master_spesialis` VALUES ('11', 'SpC', 'Spesialis Cabut', 'Spesialis Cabut Gigi');
