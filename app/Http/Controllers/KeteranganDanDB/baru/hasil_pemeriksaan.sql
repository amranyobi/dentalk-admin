/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-28 11:22:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hasil_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `hasil_pemeriksaan`;
CREATE TABLE `hasil_pemeriksaan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `subjek` text NOT NULL,
  `objek` text NOT NULL,
  `assessment` text NOT NULL,
  `planning` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_pemeriksaan
-- ----------------------------
INSERT INTO `hasil_pemeriksaan` VALUES ('1', '1', 'Gusi Berdarah', 'Radang Gigi', '', '');
