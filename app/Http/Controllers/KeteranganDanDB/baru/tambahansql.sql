/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-12-20 14:25:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `antrian_apotek`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_apotek`;
CREATE TABLE `antrian_apotek` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_apotek
-- ----------------------------
INSERT INTO `antrian_apotek` VALUES ('4', '1', '2018-12-19', '1', '2018-12-19 23:42:12', '0');

-- ----------------------------
-- Table structure for `antrian_kasir`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_kasir`;
CREATE TABLE `antrian_kasir` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_kasir
-- ----------------------------

-- ----------------------------
-- Table structure for `hasil_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `hasil_pemeriksaan`;
CREATE TABLE `hasil_pemeriksaan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `gejala` text NOT NULL,
  `diagnosa` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hasil_pemeriksaan
-- ----------------------------
INSERT INTO `hasil_pemeriksaan` VALUES ('1', '1', 'Gusi Berdarah', 'Radang Gigi');

-- ----------------------------
-- Table structure for `konfirmasi_daftar`
-- ----------------------------
DROP TABLE IF EXISTS `konfirmasi_daftar`;
CREATE TABLE `konfirmasi_daftar` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `email` varchar(25) NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of konfirmasi_daftar
-- ----------------------------

-- ----------------------------
-- Table structure for `master_jasa`
-- ----------------------------
DROP TABLE IF EXISTS `master_jasa`;
CREATE TABLE `master_jasa` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `kode_dokter` varchar(15) NOT NULL,
  `biaya_jasa` int(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_jasa
-- ----------------------------

-- ----------------------------
-- Table structure for `master_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `master_supplier`;
CREATE TABLE `master_supplier` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_supplier
-- ----------------------------
INSERT INTO `master_supplier` VALUES ('1', 'CV. Karya Kita', 'Pedurungan', '085669099980');

-- ----------------------------
-- Table structure for `master_tindakan`
-- ----------------------------
DROP TABLE IF EXISTS `master_tindakan`;
CREATE TABLE `master_tindakan` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tindakan` text NOT NULL,
  `poli` varchar(255) NOT NULL,
  `biaya_tindakan` int(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of master_tindakan
-- ----------------------------
INSERT INTO `master_tindakan` VALUES ('1', 'Tambal Gigi', 'P001', '250000');
INSERT INTO `master_tindakan` VALUES ('2', 'Penanganan Gusi', 'P001', '150000');
INSERT INTO `master_tindakan` VALUES ('3', 'Pemasangan Gigi Palsu', 'P001', '1000000');
INSERT INTO `master_tindakan` VALUES ('4', 'Konsultasi', 'P001`', '50000');

-- ----------------------------
-- Table structure for `obat_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `obat_pemeriksaan`;
CREATE TABLE `obat_pemeriksaan` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `kode_obat` varchar(10) NOT NULL,
  `qty` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of obat_pemeriksaan
-- ----------------------------
INSERT INTO `obat_pemeriksaan` VALUES ('1', '1', '7788', '3');
INSERT INTO `obat_pemeriksaan` VALUES ('2', '1', '46836', '1');

-- ----------------------------
-- Table structure for `status_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `status_pemeriksaan`;
CREATE TABLE `status_pemeriksaan` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(10) NOT NULL,
  `status` int(2) NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of status_pemeriksaan
-- ----------------------------

-- ----------------------------
-- Table structure for `tindakan_pemeriksaan`
-- ----------------------------
DROP TABLE IF EXISTS `tindakan_pemeriksaan`;
CREATE TABLE `tindakan_pemeriksaan` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tindakan` int(10) NOT NULL,
  `persen_biaya` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tindakan_pemeriksaan
-- ----------------------------
INSERT INTO `tindakan_pemeriksaan` VALUES ('1', '1', '4', '30');
INSERT INTO `tindakan_pemeriksaan` VALUES ('2', '1', '2', '0');

-- ----------------------------
-- Table structure for `transaksi_bayar`
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_bayar`;
CREATE TABLE `transaksi_bayar` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `ambil_obat` int(1) NOT NULL,
  `total_tindakan` int(25) NOT NULL,
  `total_obat` int(25) NOT NULL,
  `total_bayar` int(25) NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi_bayar
-- ----------------------------
INSERT INTO `transaksi_bayar` VALUES ('13', '1', '1', '185000', '21000', '206000', '2018-12-19 23:42:12');
