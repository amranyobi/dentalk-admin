/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : rsgm

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-01-07 13:22:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `antrian_radiografi`
-- ----------------------------
DROP TABLE IF EXISTS `antrian_radiografi`;
CREATE TABLE `antrian_radiografi` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `id_periksa` int(15) NOT NULL,
  `tanggal` date NOT NULL,
  `no_antri` int(3) NOT NULL,
  `waktu` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of antrian_radiografi
-- ----------------------------
