/*
Navicat MySQL Data Transfer

Source Server         : Server RSGM
Source Server Version : 50560
Source Host           : localhost:3306
Source Database       : simrsgm

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2019-02-09 07:25:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `master_daftar_odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `master_daftar_odontogram`;
CREATE TABLE `master_daftar_odontogram` (
  `id` int(11) NOT NULL,
  `singkatan` varchar(100) NOT NULL,
  `arti` varchar(255) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `id_jenis` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_daftar_odontogram
-- ----------------------------
INSERT INTO `master_daftar_odontogram` VALUES ('1', 'M', 'MESIAL', '-', '1');
INSERT INTO `master_daftar_odontogram` VALUES ('2', 'O', 'OCCLUSAL', '-', '1');
INSERT INTO `master_daftar_odontogram` VALUES ('3', 'D', 'DISTAL', '-', '1');
INSERT INTO `master_daftar_odontogram` VALUES ('4', 'V', 'VESTIBULAR / BUKAL / LABIAL', '-', '1');
INSERT INTO `master_daftar_odontogram` VALUES ('5', 'L', 'LINGUAL / PALATAL', '-', '1');
INSERT INTO `master_daftar_odontogram` VALUES ('6', 'sou', 'Gigi sehat, normal, tanpa kelainan', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('7', 'non', 'Gigi tidak ada / tidak diketahui', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('8', 'une', 'Un-erupted', 'Perlu dukungan Ro photo (usia dewasa)', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('9', 'pre', 'Partial ertupted', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('10', 'imv', 'Impacted visible', 'impaksi', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('11', 'ano', 'Anomali', 'Peg shape, rudimeter, supernumerary DLL', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('12', 'dia', 'Diastema', 'Ada jarak antar gigi', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('13', 'att', 'Atrisi', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('14', 'abr', 'Abrasi', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('15', 'car', 'Caries / karies', 'Harus diikuti permukaan giginya (MODVL), misal : O car, OM car', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('16', 'cfr', 'Crown Fracture / Fraktur makhota', 'Bisa di tambahkan informasi frakturnya, misal : crf `1/2 inisial`', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('17', 'nvt', 'Gigi Non Vital', 'Biasanya diikuti kondisi karies / tumpatan, misal : O car-nvt', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('18', 'rrx', 'Sisa Akar', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('19', 'mis', 'Gigi Hilang', '-', '2');
INSERT INTO `master_daftar_odontogram` VALUES ('20', 'amf', 'Amalgam Filling', 'Harus di ikuti permukaan gigi (MODVL) Misal O amf', '3');
INSERT INTO `master_daftar_odontogram` VALUES ('21', 'gif', 'GIC / Silika', 'Misal : O gif', '3');
INSERT INTO `master_daftar_odontogram` VALUES ('22', 'cof', 'Composite filling', 'Misal : MO cof', '3');
INSERT INTO `master_daftar_odontogram` VALUES ('23', 'fis', 'Fissure Sealant', 'Misal : O fis', '3');
INSERT INTO `master_daftar_odontogram` VALUES ('24', 'inl', 'Inlay', '-', '3');
INSERT INTO `master_daftar_odontogram` VALUES ('25', 'fmc', 'Full Metal Crown', 'Mahkota Logam', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('26', 'poc', 'Porcelain Crown', 'Mahkota Porcelain', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('27', 'mpc', 'Metal Porcelain Crown', '-', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('28', 'gmc', 'Gold Metal Crown', '-', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('29', 'rct', 'Root Canal Treatment / Perawatan Saluran Akar', 'Biasanya diikuti kondisi tumpatan / restorasi, misal : O amf-rct, poc-rct', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('30', 'ipx', 'Implan', '-', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('31', 'meb', 'Metal Bridge', 'Jembatan Logan', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('32', 'pob', 'Porcelain Bridge', 'Jembatan Porcelain', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('33', 'pon', 'Pontic', 'Bisa di tambah konidisi missing Misal : mis-pon', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('34', 'abu', 'Gigi abutment', 'Gigi Penyangga', '4');
INSERT INTO `master_daftar_odontogram` VALUES ('35', 'prd', 'Partial Denture', 'Gigi Tiruan Sebagian', '5');
INSERT INTO `master_daftar_odontogram` VALUES ('36', 'fld', 'Full Denture', 'Gigi Tiruan Lengkap', '5');
INSERT INTO `master_daftar_odontogram` VALUES ('37', 'acr', 'Acrilic', 'Misal : prd-acr ( Gigi tiruan sebagian akrilik)', '5');

-- ----------------------------
-- Table structure for `master_dasar_odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `master_dasar_odontogram`;
CREATE TABLE `master_dasar_odontogram` (
  `id` int(11) NOT NULL,
  `nama` varchar(3) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `taring` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_dasar_odontogram
-- ----------------------------
INSERT INTO `master_dasar_odontogram` VALUES ('1', '11', '400', '20', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('2', '12', '355', '20', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('3', '13', '310', '20', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('4', '14', '265', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('5', '15', '220', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('6', '16', '175', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('7', '17', '130', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('8', '18', '85', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('9', '21', '460', '20', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('10', '22', '505', '20', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('11', '23', '550', '20', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('12', '24', '595', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('13', '25', '640', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('14', '26', '685', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('15', '27', '730', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('16', '28', '775', '20', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('17', '41', '400', '320', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('18', '42', '355', '320', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('19', '43', '310', '320', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('20', '44', '265', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('21', '45', '220', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('22', '46', '175', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('23', '47', '130', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('24', '48', '85', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('25', '31', '460', '320', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('26', '32', '505', '320', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('27', '33', '550', '320', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('28', '34', '595', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('29', '35', '640', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('30', '36', '685', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('31', '37', '730', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('32', '38', '775', '320', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('33', '51', '400', '120', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('34', '52', '355', '120', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('35', '53', '310', '120', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('36', '54', '265', '120', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('37', '55', '220', '120', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('38', '61', '460', '120', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('39', '62', '505', '120', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('40', '63', '550', '120', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('41', '64', '595', '120', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('42', '65', '640', '120', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('43', '81', '400', '220', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('44', '82', '355', '220', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('45', '83', '310', '220', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('46', '84', '265', '220', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('47', '85', '220', '220', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('48', '71', '460', '220', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('49', '72', '505', '220', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('50', '73', '550', '220', '1');
INSERT INTO `master_dasar_odontogram` VALUES ('51', '74', '595', '220', '0');
INSERT INTO `master_dasar_odontogram` VALUES ('52', '75', '640', '220', '0');

-- ----------------------------
-- Table structure for `master_isi_odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `master_isi_odontogram`;
CREATE TABLE `master_isi_odontogram` (
  `id` int(11) NOT NULL,
  `id_daftar_odontogram` int(11) NOT NULL,
  `ketebalan` float DEFAULT NULL,
  `warna` bit(1) DEFAULT NULL,
  `isi` bit(1) DEFAULT NULL,
  `arsiran` bit(1) DEFAULT NULL,
  `tulisan` varchar(100) DEFAULT NULL,
  `garis` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_isi_odontogram
-- ----------------------------
INSERT INTO `master_isi_odontogram` VALUES ('1', '20', '0', '', '', '', null, '');
INSERT INTO `master_isi_odontogram` VALUES ('2', '7', '0', '', '', '', 'NON', '');
INSERT INTO `master_isi_odontogram` VALUES ('3', '8', '0', '', '', '', 'UNE', '');
INSERT INTO `master_isi_odontogram` VALUES ('4', '9', '0', '', '', '', 'PRE', '');
INSERT INTO `master_isi_odontogram` VALUES ('5', '11', '0', '', '', '', 'ANO', '');
INSERT INTO `master_isi_odontogram` VALUES ('6', '17', '0', '', '', '', '', '');
INSERT INTO `master_isi_odontogram` VALUES ('7', '29', '0', '', '', '', '', '');
INSERT INTO `master_isi_odontogram` VALUES ('8', '25', '5', '', '', '', null, '');
INSERT INTO `master_isi_odontogram` VALUES ('9', '16', '3', '', '', '', null, '');
INSERT INTO `master_isi_odontogram` VALUES ('10', '18', '3', '', '', '', null, '');
INSERT INTO `master_isi_odontogram` VALUES ('11', '19', '3', '', '', '', null, '');

-- ----------------------------
-- Table structure for `master_jenis_odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `master_jenis_odontogram`;
CREATE TABLE `master_jenis_odontogram` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_jenis_odontogram
-- ----------------------------
INSERT INTO `master_jenis_odontogram` VALUES ('1', 'Permukaan Gigi');
INSERT INTO `master_jenis_odontogram` VALUES ('2', 'Keadaan Gigi');
INSERT INTO `master_jenis_odontogram` VALUES ('3', 'Bahan Restorasi');
INSERT INTO `master_jenis_odontogram` VALUES ('4', 'Restorasi');
INSERT INTO `master_jenis_odontogram` VALUES ('5', 'Protesa');

-- ----------------------------
-- Table structure for `master_posisi_odontogram`
-- ----------------------------
DROP TABLE IF EXISTS `master_posisi_odontogram`;
CREATE TABLE `master_posisi_odontogram` (
  `id` int(11) NOT NULL,
  `id_daftar_odontogram` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `gariske` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of master_posisi_odontogram
-- ----------------------------
INSERT INTO `master_posisi_odontogram` VALUES ('1', '20', '-10', '25', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('2', '20', '-30', '25', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('3', '20', '-30', '45', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('4', '20', '-10', '45', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('5', '20', '-10', '25', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('6', '7', '-33', '0', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('7', '8', '-33', '0', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('8', '9', '-33', '0', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('9', '11', '-33', '0', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('10', '17', '-10', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('11', '17', '-30', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('12', '17', '-20', '75', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('13', '17', '-10', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('14', '29', '-10', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('15', '29', '-30', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('16', '29', '-20', '75', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('17', '29', '-10', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('18', '25', '0', '15', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('19', '25', '0', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('20', '25', '-40', '55', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('21', '25', '-40', '15', '0');
INSERT INTO `master_posisi_odontogram` VALUES ('22', '16', '-10', '20', '1');
INSERT INTO `master_posisi_odontogram` VALUES ('23', '16', '-15', '50', '1');
INSERT INTO `master_posisi_odontogram` VALUES ('24', '16', '-25', '20', '2');
INSERT INTO `master_posisi_odontogram` VALUES ('25', '16', '-30', '50', '2');
INSERT INTO `master_posisi_odontogram` VALUES ('26', '16', '-5', '30', '3');
INSERT INTO `master_posisi_odontogram` VALUES ('27', '16', '-35', '30', '3');
INSERT INTO `master_posisi_odontogram` VALUES ('28', '16', '-5', '40', '4');
INSERT INTO `master_posisi_odontogram` VALUES ('29', '16', '-35', '40', '4');
INSERT INTO `master_posisi_odontogram` VALUES ('30', '18', '-30', '10', '1');
INSERT INTO `master_posisi_odontogram` VALUES ('31', '18', '-20', '60', '1');
INSERT INTO `master_posisi_odontogram` VALUES ('32', '18', '-10', '10', '2');
INSERT INTO `master_posisi_odontogram` VALUES ('33', '18', '-20', '60', '2');
INSERT INTO `master_posisi_odontogram` VALUES ('34', '19', '-30', '5', '1');
INSERT INTO `master_posisi_odontogram` VALUES ('35', '19', '-10', '65', '1');
INSERT INTO `master_posisi_odontogram` VALUES ('36', '19', '-10', '5', '2');
INSERT INTO `master_posisi_odontogram` VALUES ('37', '19', '-30', '65', '2');
