-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 26, 2023 at 08:10 AM
-- Server version: 8.0.30
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simklinik`
--

-- --------------------------------------------------------

--
-- Table structure for table `dokter_dokumen`
--

CREATE TABLE `dokter_dokumen` (
  `dokumen_ids` int NOT NULL,
  `dokter_id` varchar(15) NOT NULL DEFAULT '',
  `dokumen_no` varchar(250) NOT NULL DEFAULT 'Tidak Tersedia',
  `dokumen_nama` text NOT NULL,
  `dokumen_jenis` text NOT NULL,
  `dokumen_link` mediumtext NOT NULL,
  `dokumen_tanggal_berlaku` date NOT NULL,
  `dokumen_tanggal_expired` date NOT NULL DEFAULT '1999-01-01',
  `dokumen_tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `dokter_dokumen`
--

INSERT INTO `dokter_dokumen` (`dokumen_ids`, `dokter_id`, `dokumen_no`, `dokumen_nama`, `dokumen_jenis`, `dokumen_link`, `dokumen_tanggal_berlaku`, `dokumen_tanggal_expired`, `dokumen_tanggal`) VALUES
(10, 'D555', '2345345234', 'Perpanjangan SIP 2023', 'Surat Ijin Praktek (SIP)', '/dokumen/doc/DK20230326025342.png', '2023-03-02', '2023-03-26', '2023-03-26 07:53:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dokter_dokumen`
--
ALTER TABLE `dokter_dokumen`
  ADD PRIMARY KEY (`dokumen_ids`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dokter_dokumen`
--
ALTER TABLE `dokter_dokumen`
  MODIFY `dokumen_ids` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
