-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2023 at 04:28 AM
-- Server version: 8.0.30
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simklinik`
--

-- --------------------------------------------------------

--
-- Table structure for table `karyawan_dokumen`
--

CREATE TABLE `karyawan_dokumen` (
  `dokumen_ids` int NOT NULL,
  `karyawan_id` varchar(15) NOT NULL DEFAULT '',
  `dokumen_no` varchar(250) NOT NULL DEFAULT 'Tidak Tersedia',
  `dokumen_nama` text NOT NULL,
  `dokumen_jenis` text NOT NULL,
  `dokumen_link` mediumtext NOT NULL,
  `dokumen_tanggal_berlaku` date NOT NULL,
  `dokumen_tanggal_expired` date NOT NULL DEFAULT '1999-01-01',
  `dokumen_tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan_dokumen`
--
ALTER TABLE `karyawan_dokumen`
  ADD PRIMARY KEY (`dokumen_ids`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `karyawan_dokumen`
--
ALTER TABLE `karyawan_dokumen`
  MODIFY `dokumen_ids` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
