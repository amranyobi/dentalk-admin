<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->level=='7'){
            return redirect(route('LaboratPasien'));
        }if(\Auth::user()->level=='3'){
            $dokter = \Auth::user()->username;
            $datadk = DB::table('master_dokter')->where('kode_dokter', $dokter)->first();
            $todaydatestr = date("Y-m-d");
            $expire = $datadk->dokter_sip_expired;
            $expirestr = $datadk->dokter_str_expire;

            $Y1 = (int)(substr($todaydatestr, 0, 4));
            $Y2 = (int)(substr($expire, 0, 4));
            $Y3 = (int)(substr($expirestr, 0, 4));
            $M1 = (int)(substr($todaydatestr, 6, 2));
            $M2 = (int)(substr($expire, 6, 2));
            $M3 = (int)(substr($expirestr, 6, 2));

            $statusall = "STR dan SIP Masih Berlaku";
            $notif = 0;
            $statussip = "SIP Masih Berlaku";
            $statusstr = "STR Masih Berlaku";
            if(($Y1 - $Y2) > 1){
                $notif = 2;
                $statussip = "SIP Sudah Kadaluarsa";
                $statusall = $statussip;
            } else if (($Y1 - $Y2) == 1) {
                if($M2 == 3) {
                    if($M1 > 11){
                        $notif = 1;
                        $statussip = "SIP akan kadaluarsa, segera lakukan perbaruan";
                        $statusall = $statussip;
                    }
                } if ($M2 < 3) {
                    if ($M1 > 9) {
                        $MM = $M1 + (3 - $M2);
                        if($MM > 11){
                            $notif = 1;
                            $statussip = "SIP akan kadaluarsa, segera lakukan perbaruan";
                            $statusall = $statussip;
                        }
                    }
                }
            } else if (($Y1 - $Y2) == 0) {
                if($M1 > $M2){
                    $notif = 2;
                    $statussip = "SIP Sudah Kadaluarsa";
                    $statusall = $statussip;
                } else if (($M2 - $M1) < 5){
                    $notif = 1;
                    $statussip = "SIP akan kadaluarsa, segera lakukan perbaruan";
                    $statusall = $statussip;
                }
            }

            if(($Y1 - $Y3) > 1) {
                $statusstr = "STR Sudah Kadaluarsa";
                if($notif == 0) {
                    $statusall = $statusstr;
                } else {
                    $statusall = $statusstr . " dan " . $statussip;
                }
                $notif = 2;
            } else if (($Y1 - $Y3) == 1) {
                if($M3 == 3) {
                    if($M1 > 11){
                        $statusstr = "STR akan kadaluarsa, segera lakukan perbaruan";
                        if($notif == 0) {
                            $statusall = $statusstr;
                        } else {
                            $statusall = $statusstr . " dan " . $statussip;
                        }
                        $notif = 1;
                    }
                } if ($M3 < 3) {
                    if ($M1 > 9) {
                        $MM = $M1 + (3 - $M3);
                        if($MM > 11){
                            $statusstr = "STR akan kadaluarsa, segera lakukan perbaruan";
                            if($notif == 0) {
                                $statusall = $statusstr;
                            } else {
                                $statusall = $statusstr . " dan " . $statussip;
                            }
                            $notif = 1;
                        }
                    }
                }
            } else if (($Y1 - $Y3) == 0) {
                if($M1 > $M3){
                    $statusstr = "STR Sudah Kadaluarsa";
                    if($notif == 0) {
                        $statusall = $statusstr;
                    } else {
                        $statusall = $statusstr . " dan " . $statussip;
                    }
                    $notif = 2;
                } else if (($M3 - $M1) < 5){
                    $statusstr = "STR akan kadaluarsa, segera lakukan perbaruan";
                    if($notif == 0) {
                        $statusall = $statusstr;
                    } else {
                        $statusall = $statusstr . " dan " . $statussip;
                    }
                    $notif = 1;
                }
            }

            return view('Dokter.Home', compact('expirestr', 'expire', 'notif', 'statusstr', 'statussip', 'statusall'));
        }else{
            return view('home');
        }
    }
}
