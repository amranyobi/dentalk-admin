<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Session;
use Excel;
use App\Exports\ExportRekap;
use App\Exports\ExportRekapBlokUjian;
/**
 * 
 */
class MasterController extends Controller
{
	
	public function __construct()
	{
		$this->middleware(['auth','level:1']);
	}

	// Menampilkan halaman daftar poli
	public function DaftarPenguji()
	{
		$penguji = DB::table('fk_penguji')
		->select('*')
		->get();

		$no = 1;

		return view('Master.DaftarPenguji',compact('penguji','no'));
	}

	public function DaftarMahasiswa()
	{
		$mahasiswa = DB::table('msmhs')
		->select('*')
		->orderBy('thmasuk','DESC')
		->orderBy('nim','ASC')
		->get();

		$no = 1;

		return view('Master.DaftarMahasiswa',compact('mahasiswa','no'));
	}

	public function DaftarRubrik()
	{
		$rubrik = DB::table('master_rubrik')
		->select('*')
		->orderBy('rubrik','ASC')
		->get();

		$no = 1;

		return view('Master.DaftarRubrik',compact('rubrik','no'));
	}

	public function DaftarGejala()
	{
		$gejala = DB::table('master_gejala')
		->select('*')
		->orderBy('gejala','ASC')
		->get();

		$no = 1;

		return view('Master.DaftarGejala',compact('gejala','no'));
	}

	public function DaftarDiagnosa()
	{
		$diagnosa = DB::table('master_diagnosa')
		->select('*')
		->orderBy('diagnosa','ASC')
		->get();

		$no = 1;

		return view('Master.DaftarDiagnosa',compact('diagnosa','no'));
	}

	public function DaftarUjian()
	{
		$ujian = DB::table('master_ujian')
		->select('master_ujian.*','blok_ujian.ta','mk.nama_mk1','blok_ujian.blok')
		->join('blok_ujian','master_ujian.blokujian','=','blok_ujian.id')
		->join('mk','blok_ujian.blok','=','mk.kdmk')
		->orderBy('master_ujian.tgl','DESC')
		->get();

		$no = 1;

		return view('Master.DaftarUjian',compact('ujian','no'));
	}

	public function TambahRubrik()
    {
      return view('Master.FormTambahRubrik');
    }

  public function TambahGejala()
    {
      return view('Master.FormTambahGejala');
    }

  public function TambahDiagnosa()
    {
      return view('Master.FormTambahDiagnosa');
    }

	public function DetailDiagnosa($id)
    {
		$data_diagnosa = DB::table('master_diagnosa')
		->select('*')
		->where('id',$id)
		->first();

      	return view('Master.DetailDiagnosa',compact('data_diagnosa'));
    }

	public function UbahDiagnosa($id)
    {
		$data_diagnosa = DB::table('master_diagnosa')
		->select('*')
		->where('id',$id)
		->first();

      	return view('Master.FormUbahDiagnosa',compact('data_diagnosa','id'));
    }

    public function TambahUjian()
    {
      $blok = DB::table('mk')
		->select('*')
		->orderBy('kdmk','ASC')
		->get();

			$blokujian = DB::table('blok_ujian')
			->join('mk','blok_ujian.blok','=','mk.kdmk')
			->select('blok_ujian.*', 'mk.nama_mk1')
			->orderBy('blok_ujian.ta','DESC')
			->orderBy('blok_ujian.blok','ASC')
			->get();

      return view('Master.FormTambahUjian',compact('blok','blokujian'));
    }
	

    public function SimpanTambahUjian(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'ujian'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'ujian.required'=>'Nama Ujian Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
        return redirect(route('TambahUjian'))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        $id_rubrik = DB::table('master_ujian')
          ->InsertGetId([
            'ujian'=>$data->ujian,
            'tgl'=>$data->tgl,
            'status'=>1,
            'kdprodi'=>12201,
            'tipe'=>$data->tipe,
            'blok'=>$data->blok,
            'blokujian'=>$data->blokujian,
          ]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Simpan Ujian Sukses');
        return redirect(route('DaftarUjian'));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Simpan Obat Error, Silahkan Hubungi TIK');
        return redirect(route('DaftarUjian'))->withInput();
      }

    }

    public function SimpanTambahRubrik(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'materi'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'materi.required'=>'Nama Rubrik Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
        return redirect(route('TambahRubrik'))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        $id_rubrik = DB::table('master_rubrik')
          ->InsertGetId([
            'rubrik'=>$data->materi,
            'kdprodi'=>'12201',
            'kode_blok'=>'',
            'tipe'=>$data->tipe,
          ]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Simpan Rubrik Sukses');
        return redirect(route('SettingRubrik',['id'=>$id_rubrik]));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Simpan Obat Error, Silahkan Hubungi TIK');
        return redirect(route('TambahObat'))->withInput();
      }

    }

    public function SimpanTambahGejala(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'gejala'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'materi.gejala'=>'Gejala Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
        return redirect(route('TambahGejala'))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        $id_rubrik = DB::table('master_gejala')
          ->InsertGetId([
            'gejala'=>$data->gejala
          ]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Simpan Gejala Sukses');
        return redirect(route('DaftarGejala',['id'=>$id_rubrik]));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Simpan Gejala Error');
        return redirect(route('TambahGejala'))->withInput();
      }

    }

    public function SimpanTambahDiagnosa(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'diagnosa'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'materi.diagnosa'=>'Diagnosa Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
        return redirect(route('TambahDaignosa'))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        $id_rubrik = DB::table('master_diagnosa')
          ->InsertGetId([
            'diagnosa'=>$data->diagnosa,
			'detail'=>$data->detail,
			'penanganan'=>$data->penanganan,
			'sumber'=>$data->sumber,
          ]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Simpan Diagnosa Sukses');
        return redirect(route('DaftarDiagnosa'));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Simpan Diagnosa Error');
        return redirect(route('TambahDiagnosa'))->withInput();
      }

    }

	public function SimpanUbahDiagnosa(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'diagnosa'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'materi.diagnosa'=>'Diagnosa Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
		return redirect(route('UbahDignosa',['id'=>$data->id]))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        DB::table('master_diagnosa')
					->where('id',$data->id)
					->update(['diagnosa' => $data->diagnosa, 'detail' => $data->detail, 'penanganan' => $data->penanganan, 'sumber' => $data->sumber]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Ubah Diagnosa Sukses');
        return redirect(route('DetailDiagnosa',['id'=>$data->id]));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Ubah Diagnosa Error');
        return redirect(route('DetailDiagnosa',['id'=>$data->id]))->withInput();
      }

    }

    public function SimpanMateri(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'materi'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'materi.required'=>'Materi Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
        return redirect(route('SettingRubrik',['id'=>$data->id_rubrik]))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
      	if($data->ks0=='')
      		$ks0 = '';
      	else
      		$ks0 = $data->ks0;

      	if($data->ks1=='')
      		$ks1 = '';
      	else
      		$ks1 = $data->ks1;

      	if($data->ks2=='')
      		$ks2 = '';
      	else
      		$ks2 = $data->ks2;
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        $id_rubrik = DB::table('master_materi')
          ->InsertGetId([
            'rubrik'=>$data->id_rubrik,
            'materi'=>$data->materi,
            'ks0'=>$ks0,
            'ks1'=>$ks1,
            'ks2'=>$ks2
          ]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Simpan Materi Sukses');
        return redirect(route('SettingRubrik',['id'=>$data->id_rubrik]));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Simpan Obat Error, Silahkan Hubungi TIK');
        return redirect(route('SettingRubrik',['id'=>$data->id_rubrik]));
      }

    }

    public function SettingRubrik($id, $id_materi = null)
	{
		$rubrik = DB::table('master_rubrik')
		->select('*')
		->where('id',$id)
		->first();

		$materi = DB::table('master_materi')
		->select('*')
		->where('id',$id_materi)
		->first();		

		$no = 1;

		return view('Master.SettingRubrik',compact('rubrik','no','id','materi'));
	}

	public function StaseUjian($id, $id_stase = null)
	{
		$ujian = DB::table('master_ujian')
		->select('*')
		->where('id',$id)
		->first();

		$stase = DB::table('master_stase')
		->select('*')
		->where('id',$id_stase)
		->first();

		$penguji = DB::table('fk_penguji')
		->select('*')
		->orderBy('nama','ASC')
		->get();

		$rubrik = DB::table('master_rubrik')
		->select('*')
		->orderBy('rubrik','ASC')
		->get();
		

		$no = 1;

		return view('Master.StaseUjian',compact('ujian','no','id','stase','penguji','rubrik'));
	}

	public function DataGejalaDiag($id)
	{
		$gejala_diagnosa = DB::table('gejala_diagnosa')
		->join('master_gejala','gejala_diagnosa.id_gejala','=','master_gejala.id')
		->select('master_gejala.gejala')
		->where('gejala_diagnosa.id_diagnosa',$id)
		->orderBy('master_gejala.gejala','ASC')
		->get();

		$gejala = DB::table('master_gejala')
		->select('*')
		->orderBy('gejala','ASC')
		->get();

		$diagnosa = DB::table('master_diagnosa')
		->select('*')
		->where('id',$id)
		->first();

		$no = 1;

		return view('Master.DataGejalaDiag',compact('gejala_diagnosa','gejala','diagnosa','id'));
	}

	public function RekapNilai($id)
	{
		$kode = \Auth::user()->username;
		$ujian = DB::table('master_ujian')
		->join('master_stase','master_ujian.id','=','master_stase.id_ujian')
		->join('fk_penguji','master_stase.penguji','=','fk_penguji.id')
		->select('master_ujian.*', 'master_stase.stase')
		// ->where('fk_penguji.id',$kode)
		->where('master_ujian.id',$id)
		->first();

		$mahasiswa = DB::table('rotasi_mhs')
		->select('rotasi_mhs.nim', 'msmhs.nama')
		->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
		->where('rotasi_mhs.id_ujian',$id)
		->orderBy('rotasi_mhs.nim','ASC')
		->get();

		$d_materi = DB::table('master_materi')
		->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
		->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
		->select('master_materi.*')
		// ->where('master_stase.stase',$stase)
		->where('master_stase.id_ujian',$id)
		->get();

		$stase = DB::table('master_rubrik')
		// ->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
		->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
		->select('master_rubrik.*','master_stase.stase')
		// ->where('master_stase.stase',$stase)
		->where('master_stase.id_ujian',$id)
		->orderBy('master_stase.stase','ASC')
		->get();


		// dd($stase);

		// dd($mahasiswa);

		// dd($d_materi);

		$get_materi = [];
		foreach ($d_materi as $data_materi) {
			$data_nilai = DB::table('penilaian')
			->select('nilai')
			->where('id_ujian',$id)
			->where('kompetensi',$data_materi->id)
			->first();

			$get_materi[$data_materi->id]['id_materi']=$data_materi->id;
			$get_materi[$data_materi->id]['materi']=$data_materi->materi;
			$get_materi[$data_materi->id]['ks0']=$data_materi->ks0;
			$get_materi[$data_materi->id]['ks1']=$data_materi->ks1;
			$get_materi[$data_materi->id]['ks2']=$data_materi->ks2;
			if(isset($data_nilai))
				$get_materi[$data_materi->id]['nilai']=$data_nilai->nilai;
			else
				$get_materi[$data_materi->id]['nilai']='';
		}

		// dd($get_materi);

		
		
		$no = 1;

		return view('Master.RekapNilai',compact('ujian','no','id','mahasiswa','d_materi','get_materi','stase'));
	}

	public function ExportRekap($id)
	{
		$kode = \Auth::user()->username;
		$ujian = DB::table('master_ujian')
		->join('master_stase','master_ujian.id','=','master_stase.id_ujian')
		->join('fk_penguji','master_stase.penguji','=','fk_penguji.id')
		->select('master_ujian.*', 'master_stase.stase')
		// ->where('fk_penguji.id',$kode)
		->where('master_ujian.id',$id)
		->first();

		foreach ($ujian as $dujian) {
			$nama_ujian = $ujian->ujian;
		}

		$mahasiswa = DB::table('rotasi_mhs')
		->select('rotasi_mhs.nim', 'msmhs.nama')
		->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
		->where('rotasi_mhs.id_ujian',$id)
		->orderBy('rotasi_mhs.nim','ASC')
		->get();

		$d_materi = DB::table('master_materi')
		->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
		->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
		->select('master_materi.*')
		// ->where('master_stase.stase',$stase)
		->where('master_stase.id_ujian',$id)
		->get();

		$stase = DB::table('master_rubrik')
		// ->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
		->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
		->select('master_rubrik.*','master_stase.stase')
		// ->where('master_stase.stase',$stase)
		->where('master_stase.id_ujian',$id)
		->orderBy('master_stase.stase','ASC')
		->get();


		// dd($stase);

		// dd($mahasiswa);

		// dd($d_materi);

		$get_materi = [];
		foreach ($d_materi as $data_materi) {
			$data_nilai = DB::table('penilaian')
			->select('nilai')
			->where('id_ujian',$id)
			->where('kompetensi',$data_materi->id)
			->first();

			$get_materi[$data_materi->id]['id_materi']=$data_materi->id;
			$get_materi[$data_materi->id]['materi']=$data_materi->materi;
			$get_materi[$data_materi->id]['ks0']=$data_materi->ks0;
			$get_materi[$data_materi->id]['ks1']=$data_materi->ks1;
			$get_materi[$data_materi->id]['ks2']=$data_materi->ks2;
			if(isset($data_nilai))
				$get_materi[$data_materi->id]['nilai']=$data_nilai->nilai;
			else
				$get_materi[$data_materi->id]['nilai']='';
		}

		// dd($get_materi);

		
		
		$no = 1;

		$data = [
        'no'=>$no,
        'ujian'=>$ujian,
        'id'=>$id,
        'mahasiswa'=>$mahasiswa,
        'd_materi'=>$d_materi,
        'get_materi'=>$get_materi,
        'stase'=>$stase,
      ];
     $nama_file = 'Rekapitulasi_Nilai_'.$nama_ujian.'.xlsx';
     return Excel::download(new ExportRekap($data), $nama_file);

		// return view('Master.RekapNilai',compact('ujian','no','id','mahasiswa','d_materi','get_materi','stase'));
	}

	public static function NilaiSatu($id_ujian,$nim,$stase)
	{
		$total_nilai = DB::table('penilaian')
		// ->select('nilai')
		->where('id_ujian',$id_ujian)
		->where('stase',$stase)
		->where('nim',$nim)
		->sum('nilai');

		// dd($total_nilai);
		$d_materi = DB::table('master_materi')
		->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
		->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
		->select('master_materi.*')
		->where('master_stase.stase',$stase)
		->where('master_stase.id_ujian',$id_ujian)
		->get();

		$total_poin = 0;
		foreach ($d_materi as $mat) {
			if($mat->ks2=='' || $mat->ks2=='-')
			  $nmax = 1;
			else
			  $nmax = 2;

			$total_poin = $total_poin + $nmax;
		}

		$nilai_satuan = $total_nilai/$total_poin*100;


		return $nilai_satuan;
	}

	public static function NilaiSatuBLokUjian($id_blok,$nim,$stase)
	{

		$d_uji = DB::table('master_ujian')
		->join('rotasi_mhs','master_ujian.id','=','rotasi_mhs.id_ujian')
		->select('master_ujian.id')
		->where('master_ujian.blokujian',$id_blok)
		->where('rotasi_mhs.nim',$nim)
		->first();

		$total_nilai = DB::table('penilaian')
		// ->select('nilai')
		->where('id_ujian',$d_uji->id)
		->where('stase',$stase)
		->where('nim',$nim)
		->sum('nilai');

		// dd($total_nilai);
		$d_materi = DB::table('master_materi')
		->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
		->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
		->select('master_materi.*')
		->where('master_stase.stase',$stase)
		->where('master_stase.id_ujian',$d_uji->id)
		->get();

		$total_poin = 0;
		foreach ($d_materi as $mat) {
			if($mat->ks2=='' || $mat->ks2=='-')
			  $nmax = 1;
			else
			  $nmax = 2;

			$total_poin = $total_poin + $nmax;
		}

		$nilai_satuan = $total_nilai/$total_poin*100;


		return $nilai_satuan;
	}

	public static function CekPembimbing($id_blok,$nim,$stase)
	{

		$d_uji = DB::table('master_ujian')
		->join('rotasi_mhs','master_ujian.id','=','rotasi_mhs.id_ujian')
		->select('master_ujian.id')
		->where('master_ujian.blokujian',$id_blok)
		->where('rotasi_mhs.nim',$nim)
		->first();
		

		$penguji = DB::table('master_stase')
		->leftjoin('fk_penguji','master_stase.penguji','=','fk_penguji.id')
		->select('fk_penguji.nama','fk_penguji.gelar')
		->where('id_ujian',$d_uji->id)
		->where('stase',$stase)
		->first();

		$final_penguji = $penguji->nama.", ".$penguji->gelar;

		return $final_penguji;
	}

	public function PlotMahasiswa($id, $id_stase = null)
	{
		$ujian = DB::table('master_ujian')
		->select('*')
		->where('id',$id)
		->first();

		$mahasiswa = DB::table('rotasi_mhs')
		->select('rotasi_mhs.nim', 'msmhs.nama')
		->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
		->where('rotasi_mhs.id_ujian',$id)
		->orderBy('rotasi_mhs.nim','ASC')
		->get();
		

		$no = 1;

		return view('Master.PlotMahasiswa',compact('ujian','no','id','mahasiswa'));
	}

	public function AjaxMateri($id_rubrik,$materi = null,$ks0 = null,$ks1 = null,$ks2 = null)
	{
		if($materi!=null)
		{
			DB::BeginTransaction();
			try{
				/*$cektindakan = DB::table('tindakan_pemeriksaan')
				->where('id_periksa',$id_periksa)
				->where('tindakan',$tindakan)
				->get();
				Supaya bisa tambah tindakan sampe banyak.
				*/

				/*if($cektindakan->count()==0){*/
				if($ks0=='-')
					$ks0='';
				if($ks1=='-')
					$ks1='';
				if($ks2=='-')
					$ks2='';
				DB::table('master_materi')->insert(
				['rubrik' => $id_rubrik, 'materi' => $materi, 'ks0' => $ks0, 'ks1' => $ks1, 'ks2' => $ks2 ]
				);
				DB::Commit();
				/*}else{
					DB::table('tindakan_pemeriksaan')
					->where('id_periksa',$id_periksa)
					->where('tindakan',$tindakan)
					->update(['persen_biaya' => $persen]);
				DB::Commit();
			}*/
					}
					catch(\Exception $e)
					{
				/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
				DB::rollBack();
				/*nulis exception di log*/
				report($e);
				/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
					}
		}
		$hasil_materi = DB::table('master_materi')
		->select('*')
		->where('rubrik',$id_rubrik)
		->orderBy('id','ASC')
		->get();
		return response()->json($hasil_materi);


	}

	public function AjaxStase($id_ujian,$stase = null,$penguji = null,$rubrik = null)
	{
		if($stase!=null)
		{
			DB::BeginTransaction();
			try{
				DB::table('master_stase')->insert(
				['id_ujian' => $id_ujian, 'stase' => $stase, 'penguji' => $penguji, 'rubrik' => $rubrik]
				);
				DB::Commit();

					}
					catch(\Exception $e)
					{
				/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
				DB::rollBack();
				/*nulis exception di log*/
				report($e);
				/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
					}
		}
		$hasil_stase = DB::table('master_stase')
		->select('master_stase.*', 'fk_penguji.nama', 'fk_penguji.gelar', 'master_rubrik.rubrik')
		->join('fk_penguji','master_stase.penguji','=','fk_penguji.id')
		->join('master_rubrik','master_stase.rubrik','=','master_rubrik.id')
		->where('master_stase.id_ujian',$id_ujian)
		->orderBy('master_stase.stase','ASC')
		->get();
		return response()->json($hasil_stase);
	}

	public function AjaxGejalaDiag($id_diagnosa,$id_gejala = null)
	{
		if($id_gejala!=null)
		{
			$ceksama = DB::table('gejala_diagnosa')
	    ->where('id_diagnosa',$id_diagnosa)
	    ->where('id_gejala',$id_gejala)
	    ->count();

	    if($ceksama==0)
	    {
	      DB::BeginTransaction();
	      try{
	      	DB::table('gejala_diagnosa')->insert(
	      	['id_diagnosa' => $id_diagnosa, 'id_gejala' => $id_gejala]
	      	);
	      	DB::Commit();

	      		}
	      		catch(\Exception $e)
	      		{
	      	/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
	      	DB::rollBack();
	      	/*nulis exception di log*/
	      	report($e);
	      	/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
	      		}
	    }			
		}
		$gejala_diagnosa = DB::table('gejala_diagnosa')
		->join('master_gejala','gejala_diagnosa.id_gejala','=','master_gejala.id')
		->select('master_gejala.gejala','gejala_diagnosa.id','master_gejala.id AS id_gejala')
		->where('gejala_diagnosa.id_diagnosa',$id_diagnosa)
		->orderBy('master_gejala.gejala','ASC')
		->get();
		return response()->json($gejala_diagnosa);
	}

	public function AjaxAngkatan($angkatan)
	{
		$angkatan = DB::table('msmhs')
		->where('thmasuk',$angkatan)
		->orderBy('nim','ASC')
		->get();
		return response()->json($angkatan);
	}

	public function AjaxHapusMateri($id_rubrik,$id_materi=null)
	{
		if($id_materi!=null)
		{
			DB::BeginTransaction();
			try{
				$cekmateri = DB::table('master_materi')
				->where('id',$id_materi)
				->delete();
				DB::Commit();
				}
				catch(\Exception $e)
				{
				/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
				DB::rollBack();
				/*nulis exception di log*/
				report($e);
				/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
				}
		}
		$hasil_materi = DB::table('master_materi')
		->select('*')
		->where('rubrik',$id_rubrik)
		->get();
		return response()->json($hasil_materi);
	}

	public function AjaxHapusStase($id_ujian,$id_stase=null)
	{
		if($id_stase!=null)
		{
			DB::BeginTransaction();
			try{
				$cekmateri = DB::table('master_stase')
				->where('id',$id_stase)
				->delete();
				DB::Commit();
				}
				catch(\Exception $e)
				{
				/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
				DB::rollBack();
				/*nulis exception di log*/
				report($e);
				/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
				}
		}
		$hasil_stase = DB::table('master_stase')
		->select('master_stase.*', 'fk_penguji.nama', 'fk_penguji.gelar', 'master_rubrik.rubrik')
		->join('fk_penguji','master_stase.penguji','=','fk_penguji.id')
		->join('master_rubrik','master_stase.rubrik','=','master_rubrik.id')
		->where('master_stase.id_ujian',$id_ujian)
		->orderBy('master_stase.stase','ASC')
		->get();
		return response()->json($hasil_stase);
	}

	public function AjaxHapusGejala($id_diagnosa,$id_gejala=null)
	{
		if($id_gejala!=null)
		{
			DB::BeginTransaction();
			try{
				$cekmateri = DB::table('master_stase')
				->where('id',$id_stase)
				->delete();
				DB::Commit();
				}
				catch(\Exception $e)
				{
				/*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
				DB::rollBack();
				/*nulis exception di log*/
				report($e);
				/*Bikin alert error, dan kembali ke halaman tambah spesialis*/
				}
		}
		$hasil_stase = DB::table('master_stase')
		->select('master_stase.*', 'fk_penguji.nama', 'fk_penguji.gelar', 'master_rubrik.rubrik')
		->join('fk_penguji','master_stase.penguji','=','fk_penguji.id')
		->join('master_rubrik','master_stase.rubrik','=','master_rubrik.id')
		->where('master_stase.id_ujian',$id_ujian)
		->orderBy('master_stase.stase','ASC')
		->get();
		return response()->json($hasil_stase);
	}

	// Menampilkan halaman tambah poli baru
	public function TambahPoli()
	{
		return view('Poli.FormTambahPoli');
	}

	public function SimpanTambahPoli(request $data)
	{
		$validasi = Validator::make($data->all(),[
			'kode_poli'=>'required|max:15',
			'nama_poli'=>'required|max:100',
		],[
			'kode_poli.required'=>'Kode Poliklinik Harus Di Isi',
			'nama_poli.required'=>'Nama Poliklinik Harus Di Isi',
			'kode_poli.max'=>'Kode Poliklinik Tidak Boleh Lebih Dari 15 Karakter',
			'nama_poli.max'=>'Nama Poliklinik Tidak Boleh Lebih Dari 100 Karakter',
		]);

		if($validasi->fails())
		{ /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama */
		    return redirect(route('TambahPoli'))
		    ->withErrors($validasi)
		    ->withInput();
		}

		//dd($validasi);

		$cekpoli = DB::table('master_poli')
	    ->where('kode_poli',$data->kode_poli)
	    ->count();

	    if($cekpoli!=0)
	    {
	      return redirect(route('TambahPoli'))
	      ->withErrors(['kode_poli'=>'Kode Poliklinik Tidak Boleh Sama'])
	      ->withInput();
	    }

		DB::BeginTransaction();
		try
		{ 	/* Insert ke tabel master_poli, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
		    DB::table('master_poli')->insert($data->except('_token'));
		    DB::Commit();
		    /* Bikin Alert Sukses */
		    Session::flash('simpan_sukses','Simpan Poliklinik Sukses');

		    return redirect(route('DaftarPoli'));
		}
		catch(\Exception $e)
		{
		    /* Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
		    DB::rollBack();
		    /* Menulis exception di log */
		    report($e);
		    //dd($e);
		    /* Bikin alert error, dan kembali ke halaman tambah poli */
		    Session::flash('simpan_error','Simpan Poliklinik Error, Silahkan Hubungi TIK');

		    return redirect(route('TambahPoli'))->withInput();
		}
	}

	public function SimpanMahasiswa(request $data)
	{
		$no = 1;
		foreach ($data->nim as $dnim) {
			DB::BeginTransaction();
			  DB::table('rotasi_mhs')
			    ->Insert([
			      'id_ujian'=>$data->id_ujian,
			      'nim'=>$dnim,
			      'no_urut'=>$no
			    ]);
			  DB::Commit();
			$no++;
		}

		return redirect(route('PlotMahasiswa',['id'=>$data->id_ujian]))->withInput();
	}

	public function HapusRubrik(request $data)
    {
      	DB::table('master_rubrik')->where('id',$data->id)->delete();
      	Session::flash('simpan_sukses','Rubrik Berhasil Dihapus');

      	return redirect(route('DaftarRubrik'));
    }
  public function HapusGejala(request $data)
    {
      	DB::table('master_gejala')->where('id',$data->id)->delete();
      	Session::flash('simpan_sukses','Gejala Berhasil Dihapus');

      	return redirect(route('DaftarGejala'));
    }

  public function HapusDiagnosa(request $data)
    {
      	DB::table('master_diagnosa')->where('id',$data->id)->delete();
      	Session::flash('simpan_sukses','Diagnosa Berhasil Dihapus');

      	return redirect(route('DaftarDiagnosa'));
    }

    public function HapusUjian(request $data)
    {
      	DB::table('master_ujian')->where('id',$data->id)->delete();
      	Session::flash('simpan_sukses','Ujian Berhasil Dihapus');

      	return redirect(route('DaftarUjian'));
    }

    public function UbahPoli($id)
    {
      	$pl = DB::table('master_poli')->where('id',$id);
      	if($pl->count()==0)
      	{
        	return redirect(route('DaftarPoli'));
      	}
      	$poliklinik = $pl->first();

      	return view('Poli.FormUbahPoli',compact('poliklinik'));
    }

    public function SimpanUbahPoli(request $data)
    {
    	$validasi = Validator::make($data->all(),[
    		'kode_poli'=>'required|max:15',
			'nama_poli'=>'required|max:100',
		],[
			'kode_poli.required'=>'Kode Poliklinik Harus Di Isi',
			'nama_poli.required'=>'Nama Poliklinik Harus Di Isi',
			'kode_poli.max'=>'Kode Poliklinik Tidak Boleh Lebih Dari 15 Karakter',
			'nama_poli.max'=>'Nama Poliklinik Tidak Boleh Lebih Dari 100 Karakter',
    	]);

    	if($validasi->fails())
    	{
    		return redirect(route('UbahPoli',['id'=>$data->id]))
        	->withErrors($validasi)
        	->withInput();
    	}

    	DB::BeginTransaction();
    	try
    	{
    		DB::table('master_poli')->where('id',$data->id)
	        ->update($data->except(['_token','id']));
	        DB::Commit();
	        
	        Session::flash('simpan_sukses','Poliklinik Berhasil di Ubah');
	        return redirect(route('DaftarPoli'));
    	}

    	catch(\Exception $e)
      	{
	        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
	        DB::rollBack();
	        /*nulis exception di log*/
	        report($e);
	        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
	        Session::flash('simpan_error','Ubah Poliklinik Error, Silahkan Hubungi TIK');
	        return redirect(route('UbahPoli',['id'=>$data->id]))->withInput();
      	}
    }

    public function DaftarBlokUjian()
    {
    	$blokujian = DB::table('blok_ujian')
    	->select('blok_ujian.*','mk.nama_mk1')
    	->join('mk','blok_ujian.blok','=','mk.kdmk')
    	->orderBy('blok_ujian.ta','DESC')
    	->orderBy('blok_ujian.blok','ASC')
    	->get();

    	$no = 1;

    	return view('Master.DaftarBlokUjian',compact('blokujian','no'));
    }

    public function TambahBlokUjian()
    {
      $blok = DB::table('mk')
			->select('*')
			->orderBy('kdmk','ASC')
			->get();

			$ta = DB::table('ta')
			->select('*')
			->orderBy('id','DESC')
			->get();

      return view('Master.FormTambahBlokUjian',compact('blok','ta'));
    }

    public function SimpanTambahBlokUjian(request $data)
    {
      /* Validasi inputan, */
      $validasi = Validator::make($data->all(),[
        /*syarat Validasi , Tidak Boleh Kosong, dan Maksimal karakter*/
        'blok'=>'required',
        'ta'=>'required',
      ],[
        /*Error Saat Validasi Gagal*/
        'blok.required'=>'Blok Harus Di Isi',
        'ta.required'=>'TA Harus Di Isi',
      ]);

      if($validasi->fails())
      { /* Jika Validasi Gagal, with error dengan mengirim error, with input dengan membawa inputan lama*/
        return redirect(route('TambahBlokUjian'))
        ->withErrors($validasi)
        ->withInput();
      }

      DB::BeginTransaction();
      try{
        /* Insert ke tabel master_obat, dengan inputan dari data input kecuali _token, _token itu dari @csrf */
        $id_rubrik = DB::table('blok_ujian')
          ->InsertGetId([
            'blok'=>$data->blok,
            'ta'=>$data->ta,
          ]);
        DB::Commit();
        /* Bikin Alert Sukses */
        Session::flash('simpan_sukses','Simpan Blok Ujian Sukses');
        return redirect(route('DaftarBlokUjian'));
      }
      catch(\Exception $e)
      {
        /*Jika eksepsion atau gagal, maka dia akan rollback yang sudah di insert */
        DB::rollBack();
        /*nulis exception di log*/
        dd($e);
        //report($e);
        /*Bikin alert error, dan kembali ke halaman tambah spesialis*/
        Session::flash('simpan_error','Simpan Blok Ujian Error');
        return redirect(route('DaftarBlokUjian'))->withInput();
      }

    }

    public function HapusBlokUjian(request $data)
    {
      	DB::table('blok_ujian')->where('id',$data->id)->delete();
      	Session::flash('simpan_sukses','Blok Ujian Berhasil Dihapus');

      	return redirect(route('DaftarBlokUjian'));
    }

    public function RekapNilaiBlokUjian($id)
    {
    	$kode = \Auth::user()->username;
    	$blokujian = DB::table('blok_ujian')
    	->join('mk','blok_ujian.blok','=','mk.kdmk')
    	->select('blok_ujian.*', 'mk.nama_mk1')
    	->where('blok_ujian.id',$id)
    	->first();

    	$ujian = DB::table('master_ujian')
    	->select('master_ujian.*', 'rotasi_mhs.nim','msmhs.nama')
    	->join('rotasi_mhs','master_ujian.id','=','rotasi_mhs.id_ujian')
    	->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
    	->where('master_ujian.blokujian',$id)
    	->get();

    	$get_data = [];
    	foreach ($ujian as $uji) {
    		$get_data[$uji->id]['id']=$uji->id;
    		$get_data[$uji->id]['mahasiswa'][] = [
         'nim'=>$uji->nim,
         'nama'=>$uji->nama,
       	];
    	}

    	// foreach ($get_data as $gd) {
    	// 	dd($get_data)
    	// }

    	// foreach($ujian as $uji)
    	// {
    	// 	$mahasiswa = DB::table('rotasi_mhs')
    	// 	->select('rotasi_mhs.nim', 'msmhs.nama')
    	// 	->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
    	// 	->where('rotasi_mhs.id_ujian',$uji->id)
    	// 	->orderBy('rotasi_mhs.nim','ASC')
    	// 	->get();
    	// 	foreach ($mahasiswa as $mhs) {
    	// 		$datamahasiswa[$uji->id]['nim'] = $mhs->nim;
    	// 	}
    	// }

    	// dd($datamahasiswa);

    	$mahasiswa = DB::table('rotasi_mhs')
    	->select('rotasi_mhs.nim', 'msmhs.nama')
    	->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
    	->join('master_ujian','rotasi_mhs.id_ujian','=','master_ujian.id')
    	->where('master_ujian.blokujian',$id)
    	->groupBy('rotasi_mhs.nim', 'msmhs.nama')
    	->orderBy('rotasi_mhs.nim','ASC')
    	->get();

    	$d_materi = DB::table('master_materi')
    	->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
    	->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
    	->select('master_materi.*')
    	// ->where('master_stase.stase',$stase)
    	->where('master_stase.id_ujian',$id)
    	->get();

    	$stase = DB::table('master_rubrik')
    	// ->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
    	->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
    	->join('master_ujian','master_stase.id_ujian','=','master_ujian.id')
    	->select('master_stase.stase')
    	// ->where('master_stase.stase',$stase)
    	->where('master_ujian.blokujian',$id)
    	->orderBy('master_stase.stase','ASC')
    	->groupBy('master_stase.stase')
    	->get();

    	// dd($stase);


    	// dd($stase);

    	// dd($mahasiswa);

    	// dd($d_materi);

    	$get_materi = [];
    	foreach ($d_materi as $data_materi) {
    		$data_nilai = DB::table('penilaian')
    		->select('nilai')
    		->where('id_ujian',$id)
    		->where('kompetensi',$data_materi->id)
    		->first();

    		$get_materi[$data_materi->id]['id_materi']=$data_materi->id;
    		$get_materi[$data_materi->id]['materi']=$data_materi->materi;
    		$get_materi[$data_materi->id]['ks0']=$data_materi->ks0;
    		$get_materi[$data_materi->id]['ks1']=$data_materi->ks1;
    		$get_materi[$data_materi->id]['ks2']=$data_materi->ks2;
    		if(isset($data_nilai))
    			$get_materi[$data_materi->id]['nilai']=$data_nilai->nilai;
    		else
    			$get_materi[$data_materi->id]['nilai']='';
    	}

    	// dd($get_materi);

    	
    	
    	$no = 1;

    	return view('Master.RekapNilaiBlokUjian',compact('get_data','blokujian','ujian','no','id','d_materi','get_materi','stase','mahasiswa'));
    }

    public function ExportRekapBlokUjian($id)
    {
    	$kode = \Auth::user()->username;
    	$blokujian = DB::table('blok_ujian')
    	->join('mk','blok_ujian.blok','=','mk.kdmk')
    	->select('blok_ujian.*', 'mk.nama_mk1')
    	->where('blok_ujian.id',$id)
    	->first();

    	$ujian = DB::table('master_ujian')
    	->select('master_ujian.*', 'rotasi_mhs.nim','msmhs.nama')
    	->join('rotasi_mhs','master_ujian.id','=','rotasi_mhs.id_ujian')
    	->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
    	->where('master_ujian.blokujian',$id)
    	->get();

    	$get_data = [];
    	foreach ($ujian as $uji) {
    		$get_data[$uji->id]['id']=$uji->id;
    		$get_data[$uji->id]['mahasiswa'][] = [
         'nim'=>$uji->nim,
         'nama'=>$uji->nama,
       	];
    	}

    	// foreach ($get_data as $gd) {
    	// 	dd($get_data)
    	// }

    	// foreach($ujian as $uji)
    	// {
    	// 	$mahasiswa = DB::table('rotasi_mhs')
    	// 	->select('rotasi_mhs.nim', 'msmhs.nama')
    	// 	->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
    	// 	->where('rotasi_mhs.id_ujian',$uji->id)
    	// 	->orderBy('rotasi_mhs.nim','ASC')
    	// 	->get();
    	// 	foreach ($mahasiswa as $mhs) {
    	// 		$datamahasiswa[$uji->id]['nim'] = $mhs->nim;
    	// 	}
    	// }

    	// dd($datamahasiswa);

    	$mahasiswa = DB::table('rotasi_mhs')
    	->select('rotasi_mhs.nim', 'msmhs.nama')
    	->join('msmhs','rotasi_mhs.nim','=','msmhs.nim')
    	->join('master_ujian','rotasi_mhs.id_ujian','=','master_ujian.id')
    	->where('master_ujian.blokujian',$id)
    	->groupBy('rotasi_mhs.nim', 'msmhs.nama')
    	->orderBy('rotasi_mhs.nim','ASC')
    	->get();

    	$d_materi = DB::table('master_materi')
    	->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
    	->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
    	->select('master_materi.*')
    	// ->where('master_stase.stase',$stase)
    	->where('master_stase.id_ujian',$id)
    	->get();

    	$stase = DB::table('master_rubrik')
    	// ->join('master_rubrik','master_materi.rubrik','=','master_rubrik.id')
    	->join('master_stase','master_rubrik.id','=','master_stase.rubrik')
    	->join('master_ujian','master_stase.id_ujian','=','master_ujian.id')
    	->select('master_stase.stase')
    	// ->where('master_stase.stase',$stase)
    	->where('master_ujian.blokujian',$id)
    	->orderBy('master_stase.stase','ASC')
    	->groupBy('master_stase.stase')
    	->get();

    	// dd($stase);


    	// dd($stase);

    	// dd($mahasiswa);

    	// dd($d_materi);

    	// $get_materi = [];
    	// foreach ($d_materi as $data_materi) {
    	// 	$data_nilai = DB::table('penilaian')
    	// 	->select('nilai')
    	// 	->where('id_ujian',$id)
    	// 	->where('kompetensi',$data_materi->id)
    	// 	->first();

    	// 	$get_materi[$data_materi->id]['id_materi']=$data_materi->id;
    	// 	$get_materi[$data_materi->id]['materi']=$data_materi->materi;
    	// 	$get_materi[$data_materi->id]['ks0']=$data_materi->ks0;
    	// 	$get_materi[$data_materi->id]['ks1']=$data_materi->ks1;
    	// 	$get_materi[$data_materi->id]['ks2']=$data_materi->ks2;
    	// 	if(isset($data_nilai))
    	// 		$get_materi[$data_materi->id]['nilai']=$data_nilai->nilai;
    	// 	else
    	// 		$get_materi[$data_materi->id]['nilai']='';
    	// }

    	// dd($get_materi);

    	
    	
    	$no = 1;

    	$data = [
        'no'=>$no,
        'ujian'=>$ujian,
        'id'=>$id,
        'mahasiswa'=>$mahasiswa,
        'd_materi'=>$d_materi,
        // 'get_materi'=>$get_materi,
        'stase'=>$stase,
        'blokujian'=>$blokujian,
      ];
     $nama_file = 'Rekapitulasi_Nilai_'.$blokujian->blok.'.xlsx';
     return Excel::download(new ExportRekapBlokUjian($data), $nama_file);

    	// return view('Master.RekapNilaiBlokUjian',compact('get_data','blokujian','ujian','no','id','d_materi','get_materi','stase','mahasiswa'));
    }

}