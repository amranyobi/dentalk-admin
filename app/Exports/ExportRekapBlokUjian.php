<?php

namespace App\Exports;

#use App\Pasien;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExportRekapBlokUjian implements FromView
{
    public function __construct(array $data)
    {
    $this->data = $data;
    }

    public function view(): View
    {
        return view('Master/ExportRekapBlokUjian', [
            'data' => $this->data,
        ]);
    }
}